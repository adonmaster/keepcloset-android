package br.com.adonio.keepcloset.frags.main.look

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.adonio.keepcloset.repos.LookRepo

class FragMainLookEditVM(private val initLoading: Boolean): ViewModel() {

    val loading = MutableLiveData<Boolean>().also { it.value = initLoading }
    var bknd = 0
    val category = MutableLiveData<String?>()
    val items = arrayListOf<Pair<LookRepo.SuItem, View>>()
}