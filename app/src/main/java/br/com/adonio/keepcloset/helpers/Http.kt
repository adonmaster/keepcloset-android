package br.com.adonio.keepcloset.helpers

import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONArrayRequestListener
import com.androidnetworking.interfaces.JSONObjectRequestListener
import org.json.JSONArray
import org.json.JSONObject

object Http {

    fun url(uri: String): String {
        if (uri.startsWith("http")) return uri
        return "https://keepcloset.com.br/api/$uri"
    }

    fun get(uri: String, params: List<Pair<String, Any>> = listOf()) = Promise<JSONObject> { resolve, reject ->
        val u = url(uri)
        AndroidNetworking.get(u)
            .apply { params.forEach { addQueryParameter(mapOf(it)) } }
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    resolve(response!!)
                }
                override fun onError(anError: ANError?) {
                    resolveError(anError, reject)
                }
            })
    }

    fun getArray(uri: String) = Promise<JSONArray> { resolve, reject ->
        val u = url(uri)
        AndroidNetworking.get(u)
            .build()
            .getAsJSONArray(object : JSONArrayRequestListener {
                override fun onResponse(response: JSONArray?) {
                    resolve(response!!)
                }
                override fun onError(anError: ANError?) {
                    resolveError(anError, reject)
                }
            })
    }


    fun post(uri: String, params: HttpFormData?) = Promise<JSONObject> { resolve, reject ->
        val u = url(uri)
        AndroidNetworking.post(u)
            .addJSONObjectBody(params?.get())
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject?) {
                    resolve(response!!)
                }
                override fun onError(anError: ANError?) {
                    resolveError(anError, reject)
                }
            })
    }

    private fun resolveError(error: ANError?, reject: (String) -> Unit)
    {
        var message: String? = null

        // json error
        JEx(error?.errorBody) {
            message = str("message")
            o("errors") {
                arrStr(keys[0]) {
                    message = it
                    false
                }
            }
        }

        // couldn't catch it
        reject(Str.coalesce(message, error?.toString(), "Error 61"))
    }
}

open class HttpFormData {
    private val jsonObject = JSONObject()
    fun put(key: String, value: String): HttpFormData {
        jsonObject.put(key, value)
        return this
    }
    fun put(key: String, value: Int): HttpFormData {
        jsonObject.put(key, value)
        return this
    }
    fun get(): JSONObject {
        return jsonObject
    }
}