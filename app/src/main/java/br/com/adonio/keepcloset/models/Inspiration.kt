package br.com.adonio.keepcloset.models

import androidx.room.Entity

@Entity(tableName = "inspirations")
class Inspiration: BaseModel() {

    var user_id: Long = 0
    var category: String? = null
    var color1: String? = null
    var color2: String? = null
    var desc: String? = null
    lateinit var path: String
    lateinit var path_thumb: String

}