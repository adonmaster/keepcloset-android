package br.com.adonio.keepcloset.models

import androidx.room.*

@Dao
abstract class LuggageDao: BaseDao<Luggage>() {

    @Query("select * from luggages where id = :id")
    abstract override fun find(id: Long): Luggage?

    @Query("select * from luggages where user_id = :userId order by date desc")
    abstract fun getAll(userId: Long): List<Luggage>

    @Query("select * from luggage_item where luggage_id = :luggageId order by id")
    abstract fun getItemsFor(luggageId: Long): List<LuggageItem>

    @Query("update luggage_item set is_packed = :packed where id = :itemId")
    abstract fun packItem(itemId: Long, packed: Boolean)

    // items

    @Query("select * from luggage_item where id = :id")
    abstract fun findItem(id: Long): LuggageItem?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertItem(model: LuggageItem): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract fun updateItem(model: LuggageItem)

    @Delete
    abstract fun deleteItem(model: LuggageItem)

}