package br.com.adonio.keepcloset.helpers

class CategorizedContainer<T>(items: List<T>?, private val extractor: (T)->String) {

    private lateinit var data: List<Item<T>>
    private var mItems: List<T>

    init {
        mItems = items ?: listOf()
        update(mItems)
    }

    fun update(items: List<T>)
    {
        mItems = items

        val pre = ArrayList<Pair<String, ArrayList<T>>>()
        for (item in items) {
            val currentCategory = extractor(item)
            val pair = pre.find { it.first.trim().equals(currentCategory.trim(), true) }
            if (pair != null) {
                pair.second.add(item)
            } else {
                pre.add(Pair(currentCategory, arrayListOf(item)))
            }
        }

        // now put the damn thing together
        val r = ArrayList<Item<T>>()
        for (pair in pre) {
            r.add(ItemCategory(pair.first))
            for (item in pair.second) {
                r.add(ItemData(item))
            }
        }

        data = r
    }

    fun getItems() = mItems

    fun count() = data.count()

    operator fun get(position: Int): Item<T>? {
        return data[position]
    }

    fun isTypeData(type: Int) = type == 1

    fun isTypeCategory(type: Int) = type == 2

    // item

    interface Item<T> {
        fun getType(): Int
        fun isTypeData(): Boolean
        fun isTypeCategory(): Boolean
        fun whenCategory(cb: (String)->Unit): Item<T>
        fun whenData(cb: (T)->Unit): Item<T>
    }

    class ItemData<T>(private val content: T): Item<T> {
        override fun getType() = 1
        override fun isTypeData() = true
        override fun isTypeCategory() = false
        override fun whenCategory(cb: (String) -> Unit) = this
        override fun whenData(cb: (T)->Unit): Item<T> {
            cb(content)
            return this
        }
    }

    class ItemCategory<T>(private val content: String): Item<T> {
        override fun getType() = 2
        override fun isTypeData() = false
        override fun isTypeCategory() = true
        override fun whenData(cb: (T) -> Unit): Item<T> = this
        override fun whenCategory(cb: (String) -> Unit): Item<T> {
            cb(content)
            return this
        }
    }
}
