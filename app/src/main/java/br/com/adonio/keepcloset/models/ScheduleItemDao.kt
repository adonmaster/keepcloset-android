package br.com.adonio.keepcloset.models

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import br.com.adonio.keepcloset.rmodels.ScheduleGroupPathRModel
import org.threeten.bp.LocalDate

@Dao
abstract class ScheduleItemDao: BaseDao<ScheduleItem>() {

    @Query("select * from schedule_item where id = :id LIMIT 1")
    abstract override fun find(id: Long): ScheduleItem?

    @Query("select * from schedule_item where user_id = :userId AND dt >= :ini AND dt <= :end")
    abstract fun getBetween(userId: Long, ini: LocalDate, end: LocalDate): List<ScheduleItem>

    @Query("delete from schedule_item where id = :id")
    abstract fun deleteById(id: Long)

    @Transaction
    @Query("select item_id, look_id, count(*) as count from schedule_item where user_id = :userId group by item_id, look_id order by count desc limit 20")
    abstract fun getGroupedPaths(userId: Long): List<ScheduleGroupPathRModel>

}