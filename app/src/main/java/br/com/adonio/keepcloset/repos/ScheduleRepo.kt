package br.com.adonio.keepcloset.repos

import br.com.adonio.keepcloset.db.DB
import br.com.adonio.keepcloset.helpers.*
import br.com.adonio.keepcloset.models.ScheduleItem
import br.com.adonio.keepcloset.models.ScheduleItemDao
import br.com.adonio.keepcloset.rmodels.ScheduleGroupPathRModel
import org.threeten.bp.LocalDate
import java.io.File

class ScheduleRepo: BaseRepo<ScheduleItem, ScheduleItemDao>(DB.i.schedule) {

    fun create(
        userId: Long, dt: LocalDate, item_thumb: String, item_full: String, item_id: Long, look_id: Long
    ): ScheduleItem
    {
        val filename = Str.random(32)
        val full = F.copyInternal(File(item_full), "schedule", filename)
        val thumb = F.copyInternal(File(item_thumb), "schedule", "${filename}_thumb")

        val model = ScheduleItem.f(dt, full.path, thumb.path)
        model.user_id = userId
        model.item_id = item_id
        model.look_id = look_id

        return save(model)
    }

    fun getBetweenPromise(userId: Long, ini: LocalDate, end: LocalDate) = Promise<List<ScheduleItem>> { resolve, _ ->
        Task.main {
            resolve(dao.getBetween(userId, ini, end))
        }
    }

    fun destroy(id: Long?) {
        val i = guard(id) { return }
        val model = guard(dao.find(i)) { return }

        File(model.path).delete()
        File(model.path_thumb).delete()

        dao.delete(model)
    }

    fun updateColor(
        id: Long,
        color1: Long,
        color1Desc: String?,
        color2: Long,
        color2Desc: String?
    ): ScheduleItem?
    {
        val model = guard(dao.find(id)) { return null }
        model.color1 = color1
        model.color1_desc = color1Desc
        model.color2 = color2
        model.color2_desc = color2Desc

        return save(model, true)
    }

    fun getGroupedPaths(userId: Long): List<ScheduleGroupPathRModel> {
        return dao.getGroupedPaths(userId)
    }

}