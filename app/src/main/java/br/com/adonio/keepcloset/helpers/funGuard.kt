package br.com.adonio.keepcloset.helpers

import java.lang.IllegalArgumentException

inline fun <T> guard(obj: T?, bye: ()->Unit): T {
    if (obj==null) {
        bye()
        throw IllegalArgumentException("Use return with guard, damn it!")
    }
    return obj
}

