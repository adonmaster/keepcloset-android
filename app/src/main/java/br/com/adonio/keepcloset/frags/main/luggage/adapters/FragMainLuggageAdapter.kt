package br.com.adonio.keepcloset.frags.main.luggage.adapters

import android.annotation.SuppressLint
import android.os.Build
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Act
import br.com.adonio.keepcloset.helpers.Dt
import br.com.adonio.keepcloset.helpers.Res
import br.com.adonio.keepcloset.helpers.Str
import br.com.adonio.keepcloset.models.Luggage
import br.com.adonio.keepcloset.repos.Repo
import kotlinx.android.synthetic.main.rv_frag_main_luggage.view.*

class FragMainLuggageAdapter(private val listener: Listener):
    RecyclerView.Adapter<FragMainLuggageAdapter.VH>()
{

    // state
    private val mData by lazy { Repo.luggage.getAll(App.modelUser?.id ?: 0).toMutableList() }

    // methods
    override fun getItemCount(): Int {
        return mData.count().also {
            listener.onEmpty(it == 0)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(R.layout.rv_frag_main_luggage, parent, false)!!
        return VH(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = mData[position]
        val itemCount = m.items.count()
        val itemCountPacked = m.items.filter { it.isPacked }.count()

        //
        holder.uiTitle.text = m.location

        //
        holder.uiDesc1.text = listOfNotNull(
            Dt.format(m.date, Res.str(R.string.dd_MM_yyyy)),
            Dt.fromNow(m.date)?.let { "($it)" }
        ).joinToString(" ")

        //
        var desc2 = Str.plural(
            m.duration,
            Res.str(R.string.sem_dias),
            Res.str(R.string.duracao_de_um_dia)
        ) {
            Res.str(R.string.duracao_de_ss) + it + Res.str(R.string.ss_dias)
        }
        desc2 += "\n" + when {
            itemCount==0 -> Res.str(R.string.p_sem_item_cadastrado_p)
            itemCountPacked==0 -> Res.str(R.string.nenhum_item_empacotado)
            itemCountPacked<itemCount ->
                Str.plural(
                    itemCountPacked,
                    "",
                    Res.str(R.string.um_item_empacotado)
                ) {
                    "$it "+Res.str(R.string.itens_empacotados)
                }
            else -> Res.str(R.string.todos_itens_empacotados)
        }
        holder.uiDesc2.text = desc2

        // items
        val idBknd = if (itemCount > 0 && itemCount == itemCountPacked)
            R.drawable.sel_oval_green else R.drawable.sel_oval_black
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.uiWrapperLeft.background = App.shared.resources.getDrawable(idBknd, null)
        } else {
            holder.uiWrapperLeft.background = App.shared.resources.getDrawable(idBknd)
        }

        //
        holder.uiLeft.text = "$itemCountPacked/$itemCount"

        // transparency by date
        holder.itemView.alpha = if (m.p.isDatePast()) 0.4f else 1.0f
    }

    fun removeModel(model: Luggage, position: Int) {
        Repo.luggage.delete(model)
        mData.removeAt(position)
        notifyItemRemoved(position)
    }

    // view
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiWrapperLeft = itemView.ui_wrapper_left
        val uiLeft = itemView.ui_left
        val uiTitle = itemView.ui_title
        val uiDesc1 = itemView.ui_desc1
        val uiDesc2 = itemView.ui_desc2

        init {
            itemView.setOnClickListener {
                listener.onSelect(mData[adapterPosition], adapterPosition)
            }
            itemView.setOnLongClickListener {
                listener.onLongClick(mData[adapterPosition], adapterPosition)
                true
            }
        }
    }

    // listener
    interface Listener {
        fun onSelect(model: Luggage, position: Int)
        fun onLongClick(model: Luggage, position: Int)
        fun onEmpty(option: Boolean)
    }

}