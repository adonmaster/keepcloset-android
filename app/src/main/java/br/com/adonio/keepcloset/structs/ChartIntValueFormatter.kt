package br.com.adonio.keepcloset.structs

import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter

class ChartIntValueFormatter: ValueFormatter() {

    override fun getPieLabel(value: Float, pieEntry: PieEntry?): String {
        return "${value.toInt()}"
    }

}