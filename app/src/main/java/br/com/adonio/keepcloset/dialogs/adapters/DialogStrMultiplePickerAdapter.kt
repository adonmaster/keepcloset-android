package br.com.adonio.keepcloset.dialogs.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatCheckedTextView
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.DialogStrMultiplePicker
import br.com.adonio.keepcloset.helpers.Act
import br.com.adonio.keepcloset.helpers.Cl
import kotlinx.android.synthetic.main.rv_dialog_str_multiple_picker.view.*

class DialogStrMultiplePickerAdapter(private val listener: Listener): RecyclerView.Adapter<DialogStrMultiplePickerAdapter.VH>() {

    // state
    private var mData = listOf<DialogStrMultiplePicker.Item>()

    // methods
    fun refresh(data: List<DialogStrMultiplePicker.Item>) {
        mData = data
        notifyDataSetChanged()
    }

    // adapter

    override fun getItemCount(): Int {
        return mData.count().also {
            listener.onEmpty(it == 0)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(Act.inflate(R.layout.rv_dialog_str_multiple_picker, parent, false)!!)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = mData[position]

        val context = holder.v.context
        val isSelected = listener.isItemSelected(m.uid)
        val hi = if (isSelected) R.color.md_blue_grey_800 else R.color.md_blue_grey_300

        holder.uiLbl?.text = m.text
        holder.uiLbl?.setTextColor(Cl.res(context, hi))
        holder.uiLbl?.isChecked = isSelected
    }


    // vh
    inner class VH(val v: View) : RecyclerView.ViewHolder(v) {
        val uiLbl: AppCompatCheckedTextView? = itemView.ui_lbl
        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                val m = mData[position]
                listener.onSelect(m, position)
            }
        }
    }

    // listener
    interface Listener {
        fun onSelect(selected: DialogStrMultiplePicker.Item, position: Int)
        fun isItemSelected(uid: String): Boolean
        fun onEmpty(option: Boolean)
    }

}