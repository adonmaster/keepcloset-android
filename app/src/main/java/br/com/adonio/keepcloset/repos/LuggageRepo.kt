package br.com.adonio.keepcloset.repos

import br.com.adonio.keepcloset.db.DB
import br.com.adonio.keepcloset.helpers.F
import br.com.adonio.keepcloset.helpers.Str
import br.com.adonio.keepcloset.models.Luggage
import br.com.adonio.keepcloset.models.LuggageDao
import br.com.adonio.keepcloset.models.LuggageItem
import org.threeten.bp.LocalDate
import java.io.File

class LuggageRepo: BaseRepo<Luggage, LuggageDao>(DB.i.luggage) {

    fun getAll(userId: Long): List<Luggage> {
        return dao.getAll(userId)
    }

    fun find(id: Long): Luggage? {
        return dao.find(id)
    }

    fun save(luggageId: Long, userId: Long, location: String, date: LocalDate, duration: Int): Luggage {
        val model = find(luggageId) ?: Luggage()
        model.userId = userId
        model.location = location
        model.date = date
        model.duration = duration

        return save(model, true)
    }

    fun delete(model: Luggage) {
        model.items.forEach{ removeItem(it) }
        dao.delete(model)
    }

    fun getItemsFor(luggageId: Long): List<LuggageItem> {
        return dao.getItemsFor(luggageId)
    }

    fun packItem(itemId: Long, isPacked: Boolean) {
        if (itemId<=0) return
        dao.packItem(itemId, isPacked)
    }

    fun saveItems(parent: Luggage, items: List<LuggageItem>) {
        if (parent.id==0L) return

        val r = arrayListOf<LuggageItem>()

        // save models
        items.forEach { item ->
            val isNew = item.isNew()
            item.luggage_id = parent.id

            // save prior to copy files, in order to get the 'id'
            val itemSaved = saveItem(item)

            if (isNew && item.desc != "Text") {
                val newFile = F.copyInternal(File(item.path), "luggages", "${itemSaved.id}.png")
                itemSaved.path = newFile.path
                saveItem(itemSaved)
            }

            r.add(itemSaved)
        }

        // sync
        val all = Repo.luggage.getItemsFor(parent.id)
        val deleteList = all.filter { o -> r.find { ri -> ri.id == o.id } == null }
        deleteList.forEach { d ->
            removeItem(d)
        }
    }

    private fun removeItem(item: LuggageItem) {
        dao.deleteItem(item)
        File(item.path).delete()
    }

    private fun saveItem(model: LuggageItem): LuggageItem {
        return if (model.id > 0) {
            dao.updateItem(model)
            dao.findItem(model.id)!!
        } else {
            val id = dao.insertItem(model)
            dao.findItem(id)!!
        }
    }

    fun clone(model: Luggage?): Luggage? {
        if (model == null || model.id == 0L) return null

        val cloned = save(0L, model.userId, model.location, model.date, model.duration)
        saveItems(cloned, model.items.map { it.cloneUnlinked() })

        return find(cloned.id)
    }


}