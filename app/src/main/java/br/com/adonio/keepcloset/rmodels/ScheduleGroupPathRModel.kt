package br.com.adonio.keepcloset.rmodels

import androidx.room.Embedded
import androidx.room.Ignore
import androidx.room.Relation
import br.com.adonio.keepcloset.db.DB
import br.com.adonio.keepcloset.helpers.LightCache
import br.com.adonio.keepcloset.models.Item
import br.com.adonio.keepcloset.models.Look
import br.com.adonio.keepcloset.repos.Repo

class ScheduleGroupPathRModel {

    var item_id = 0L
    var look_id = 0L
    var count = 0

    @Ignore
    private val cache = LightCache()

    // eager loading
    val item: Item? get() = cache.get("item", { DB.i.item.find(this.item_id) })
    val look: Look? get() = cache.get("look", { DB.i.look.find(this.look_id) })

}