package br.com.adonio.keepcloset.frags.about

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import br.com.adonio.keepcloset.MainActivity
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Act
import kotlinx.android.synthetic.main.frag_about.*

class FragAbout: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.frag_about, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // setup top navigation
        (activity as MainActivity).setupChildNavController(null)

        //
        ui_ver.text = getString(R.string.version) + " ${Act.versionName()}"

        ui_btn_fb.setOnClickListener { Act.openLink(context, "https://web.facebook.com/keepclosetapp") }
        ui_btn_insta.setOnClickListener { Act.openInstagram(context!!, "keepclosetapp") }
    }

}