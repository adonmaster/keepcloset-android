package br.com.adonio.keepcloset.helpers

import android.content.Context
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.*
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.NonNull
import android.text.Spannable
import android.text.style.TypefaceSpan
import android.text.TextPaint
import android.text.style.MetricAffectingSpan
import androidx.annotation.FontRes
import androidx.core.content.res.ResourcesCompat


class Span(s: String) {

    private var mInit: Int = 0
    private var mEnd: Int = 0
    private val result: SpannableStringBuilder = SpannableStringBuilder(s)

    companion object {
        fun f(s: String) = Span(s)
        fun f() = Span("")
        fun f(number: Long) = f(number.toString())
    }

    init {
        mEnd = s.length
    }

    fun concat(@NonNull s: String): Span {
        result.append(s)
        mInit = mEnd
        mEnd = result.length
        return this
    }

    private fun setSpan(span: Any): Span {
        result.setSpan(span, mInit, mEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        return this
    }

    fun color(context: Context, @ColorRes id: Int): Span {
        return color(Cl.res(context, id))
    }

    fun color(color: Int): Span {
        return setSpan(ForegroundColorSpan(color))
    }

    fun bold(): Span {
        return setSpan(StyleSpan(Typeface.BOLD))
    }

    fun italic(): Span {
        return setSpan(StyleSpan(Typeface.ITALIC))
    }

    fun boldItalic(): Span {
        return setSpan(StyleSpan(Typeface.BOLD_ITALIC))
    }

    fun img(drawable: Drawable): Span {
        val span = ImageSpan(drawable, DynamicDrawableSpan.ALIGN_BOTTOM)
        return concat("(img)").setSpan(span)
    }

    fun build(): SpannableStringBuilder {
        return result
    }

    fun size(context: Context, @DimenRes textSizeId: Int): Span {
        val size = context.resources.getDimensionPixelSize(textSizeId)
        return setSpan(AbsoluteSizeSpan(size))
    }

    fun font(context: Context, @FontRes fontId: Int): Span {
        val font = ResourcesCompat.getFont(context, fontId)!!
        return setSpan(CustomTypeFaceSpan(font))
    }

    // custom type face span
    inner class CustomTypeFaceSpan(private val typeface: Typeface) : MetricAffectingSpan() {
        override fun updateDrawState(ds: TextPaint) {
            applyCustomTypeFace(ds, typeface)
        }
        override fun updateMeasureState(paint: TextPaint) {
            applyCustomTypeFace(paint, typeface)
        }
        private fun applyCustomTypeFace(paint: Paint, tf: Typeface) {
            paint.typeface = tf
        }
    }
}