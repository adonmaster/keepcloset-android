package br.com.adonio.keepcloset.frags.main.schedule

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Dt
import kotlinx.android.synthetic.main.frag_main_schedule.*

class FragMainSchedule: Fragment() {

    // state
    private lateinit var vm: FragMainScheduleVM

    // methods
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_main_schedule, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(activity!!).get(FragMainScheduleVM::class.java)

        ui_pager.adapter = object: FragmentPagerAdapter(childFragmentManager) {
            override fun getCount(): Int {
                return vm.positionCount
            }
            override fun getItem(position: Int): Fragment {
                return FragScheduleItem.f(position)
            }
        }
        ui_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                vm.position.value = position

                updateDesktop()
            }
        })
        ui_pager.currentItem = vm.position.value!!

        updateDesktop()
    }

    @SuppressLint("DefaultLocale")
    private fun updateDesktop() {
        ui_lbl_month.text = Dt.format(vm.cDate, "MMMM")?.toUpperCase()
        ui_lbl_year.text = Dt.format(vm.cDate, "yyyy")
    }

}