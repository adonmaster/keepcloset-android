package br.com.adonio.keepcloset.frags.main.inspiration.adapter

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Act
import br.com.adonio.keepcloset.helpers.CategorizedContainer
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.helpers.Res
import br.com.adonio.keepcloset.models.Inspiration
import kotlinx.android.synthetic.main.rv_main_inspiration.view.*
import kotlinx.android.synthetic.main.rv_main_inspiration_header.view.*
import java.io.File

class FragMainInspirationAdapter(private val listener: Listener): RecyclerView.Adapter<FragMainInspirationAdapter.VH>() {

    @SuppressLint("DefaultLocale")
    private val mData = CategorizedContainer<Inspiration>(null) {
        it.category ?: Res.str(R.string.sem_categoria)
    }

    fun updateData(list: List<Inspiration>) {
        mData.update(list)
        notifyDataSetChanged()
    }

    fun isItemCategory(position: Int): Boolean {
        return mData[position]?.isTypeCategory() != false
    }

    override fun getItemViewType(position: Int): Int {
        return mData[position]?.getType() ?: -1
    }

    override fun getItemCount(): Int {
        return mData.count()
            .also { listener.onEmpty(it==0) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val id = if (mData.isTypeData(viewType)) R.layout.rv_main_inspiration
            else R.layout.rv_main_inspiration_header
        val v = Act.inflate(id, parent, false)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        mData[position]
            ?.whenCategory {
                holder.uiHeaderTitle?.text = it
            }
            ?.whenData {
                Img.load(File(it.path_thumb), holder.uiImg)
            }
    }

    // vh
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiHeaderTitle: TextView? = itemView.ui_header_title
        val uiImg: ImageView? = itemView.ui_img

        init {
            itemView.setOnClickListener {
                mData[adapterPosition]?.whenData { m ->
                    mData.getItems().indexOf(m).takeIf { it >= 0 }?.let { rawPosition ->
                        listener.onSelect(mData.getItems(), rawPosition)
                    }
                }
            }
        }
    }

    // interface
    interface Listener {
        fun onEmpty(option: Boolean)
        fun onSelect(rawList: List<Inspiration>, rawPosition: Int)
    }
}