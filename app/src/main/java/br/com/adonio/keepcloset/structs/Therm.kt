package br.com.adonio.keepcloset.structs

import android.content.Context
import android.text.Spannable
import androidx.annotation.ColorRes
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Span

enum class Therm {

    VERY_COLD {
        override fun v() = 1
        override fun toString() = "Muito frio"
        override fun faIcon() = "\uF2CB"
        override fun colorLightTheme() = R.color.md_indigo_500
    },
    COLD {
        override fun v() = 2
        override fun toString() = "Frio"
        override fun faIcon() = "\uF2CA"
        override fun colorLightTheme() = R.color.md_blue_500
    },
    FINE {
        override fun v() = 3
        override fun toString() = "Ameno"
        override fun faIcon() = "\uF2C9"
        override fun colorLightTheme() = R.color.md_grey_800
    },
    WARM {
        override fun v() = 4
        override fun toString() = "Quente"
        override fun faIcon() = "\uF2C8"
        override fun colorLightTheme() = R.color.md_deep_orange_500
    },
    VERY_WARM {
        override fun v() = 5
        override fun toString() = "Muito quente"
        override fun faIcon() = "\uF2C7"
        override fun colorLightTheme() = R.color.md_red_500
    };

    companion object {
        fun list() = listOf(VERY_COLD, COLD, FINE, WARM, VERY_WARM)
        fun f(v: Int) = list().find { it.v()==v }
    }

    abstract fun v(): Int
    abstract override fun toString(): String
    abstract fun faIcon(): String
    abstract fun colorLightTheme(): Int

    fun faIconColored(context: Context): Spannable {
        return Span(faIcon()).color(context, colorLightTheme()).build()
    }

}