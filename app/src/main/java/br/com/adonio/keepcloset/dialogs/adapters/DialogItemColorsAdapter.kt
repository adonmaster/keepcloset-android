package br.com.adonio.keepcloset.dialogs.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Act
import br.com.adonio.keepcloset.structs.Palette
import kotlinx.android.synthetic.main.rv_dialog_item_colors.view.*

class DialogItemColorsAdapter(val listener: Listener): RecyclerView.Adapter<DialogItemColorsAdapter.VH>() {

    private val mData = Palette.shared.list

    override fun getItemCount(): Int {
        return mData.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(R.layout.rv_dialog_item_colors, parent, false)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = mData[position]
        holder.uiView?.setBackgroundColor(m.first.toInt())
    }

    // view holder
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiView: View? = itemView.ui_view
        init {
            itemView.setOnClickListener {
                listener.onSelect(mData[adapterPosition])
            }
        }
    }

    // listener
    interface Listener {
        fun onSelect(color: Pair<Long, String>)
    }

}