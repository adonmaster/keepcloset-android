package br.com.adonio.keepcloset.frags.main.luggage

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.DatePicker
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.DialogOk
import br.com.adonio.keepcloset.dialogs.DialogPicPath
import br.com.adonio.keepcloset.dialogs.DialogQuery
import br.com.adonio.keepcloset.dialogs.DialogStrPicker
import br.com.adonio.keepcloset.dialogs.adapters.DialogPicPathAdapter
import br.com.adonio.keepcloset.extensions.setVisible
import br.com.adonio.keepcloset.frags.main.luggage.adapters.FragLuggageEditAdapter
import br.com.adonio.keepcloset.helpers.*
import br.com.adonio.keepcloset.models.LuggageItem
import br.com.adonio.keepcloset.repos.Repo
import kotlinx.android.synthetic.main.frag_luggage_edit.*
import org.threeten.bp.LocalDate
import java.io.File

class FragLuggageEdit: Fragment(), DialogStrPicker.Listener, DatePickerDialog.OnDateSetListener,
    DialogPicPath.Listener, DialogQuery.Listener {

    // static
    companion object {
        fun navigate(from: Fragment, @IdRes id: Int, luggageId: Long) {
            val bundle = Bundle()
            bundle.putLong("luggageId", luggageId)
            from.findNavController().navigate(id, bundle)
        }
    }

    // computed
    private val cModel get() = Repo.luggage.find(arguments?.getLong("luggageId") ?: 0)

    // state
    private lateinit var vm: FragLuggageEditVM
    private lateinit var mAdapter: FragLuggageEditAdapter
    private var mChanged = MutableLiveData<Int>().also { it.value = 0 }

    // methods
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.frag_luggage_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(this).get(FragLuggageEditVM::class.java)

        vm.luggageId = cModel?.id ?: 0

        mChanged.observe(this, Observer {
            ui_wrapper_changed.setVisible(it > 0)
        })
        mChanged.value = 0

        vm.location.observe(this, Observer {
            val span = Span(it ?: getString(R.string.escolha_lugar_dots))
            if (it==null) span.color(context!!, R.color.md_grey_500)
            ui_location.text = span.build()
        })
        vm.location.value = cModel?.location

        vm.date.observe(this, Observer {
            var msg = getString(R.string.escolha_data_dots)
            it?.let { d -> msg = Dt.format(d, getString(R.string.dd_MM_yyyy)) + " (${Dt.fromNow(d)})" }
            val span = Span(msg)
            if (it==null) span.color(context!!, R.color.md_grey_500)
            ui_date.text = span.build()
        })
        vm.date.value = cModel?.date

        vm.duration.observe(this, Observer {
            val span = Span(it?.let { duration -> "$duration "+getString(R.string.dias_p_s_p) } ?: getString(R.string.escolha_duracao_da_viagem_dots))
            if (it==null) span.color(context!!, R.color.md_grey_500)
            ui_duration.text = span.build()
        })
        vm.duration.value = cModel?.duration

        // items
        vm.items.clear()
        cModel?.items?.let { l -> vm.items.addAll(l) }

        //--

        ui_wrapper_location.setOnClickListener {
            DialogStrPicker.show(fragmentManager, getString(R.string.escolha_lugar), "location")
        }

        ui_wrapper_date.setOnClickListener {
            val dt = vm.date.value ?: LocalDate.now()
            DatePickerDialog(context!!, this, dt.year, dt.monthValue-1, dt.dayOfMonth).show()
        }

        ui_wrapper_duration.setOnClickListener {
            DialogStrPicker.show(fragmentManager, getString(R.string.escolha_duracao), "duration")
        }

        ui_btn_add_item.setOnClickListener {
            DialogPicPath.show(fragmentManager)
        }

        //
        mAdapter = FragLuggageEditAdapter(vm.items, object : FragLuggageEditAdapter.Listener {
            override fun onEmpty(option: Boolean) {
                ui_empty.setVisible(option)
            }
            override fun onSelect(model: LuggageItem, position: Int) {
                packItem(model, position)
            }
            override fun onLongClick(model: LuggageItem, position: Int) {
                removeItem(position)
            }
        })
        ui_rv.adapter = mAdapter
        ui_rv.layoutManager = GridLayoutManager(context, 3)
        ui_rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_slide_right)

        //
        updatePackedInfo()
    }

    private fun packItem(model: LuggageItem, position: Int) {
        model.isPacked = !model.isPacked

        Repo.luggage.packItem(model.id, model.isPacked)

        mAdapter.notifyItemChanged(position)

        updatePackedInfo()

        mChanged.value = 1
    }

    private fun removeItem(position: Int) {
        vm.items.removeAt(position)
        mAdapter.notifyItemRemoved(position)

        updatePackedInfo()

        mChanged.value = 1
    }

    private fun updatePackedInfo() {
        val itemCount = vm.items.count()
        val itemCountPacked = vm.items.filter { it.isPacked }.count()
        ui_packed.text = when {
            itemCount==0 -> getString(R.string.p_sem_item_cadastrado_p)
            itemCountPacked==0 -> getString(R.string.nenhum_item_empacotado)
            itemCountPacked<itemCount ->
                Str.plural(
                    itemCountPacked, "",
                    getString(R.string.um_item_empacotado)
                ) {
                    "$it "+getString(R.string.itens_empacotados)
                }
            else -> getString(R.string.todos_itens_empacotados)
        }
    }

    private fun copy() {
        DialogOk.show(
            context,
            getString(R.string.tem_certeza_que_deseja_copiar_essa_bagagem),
            getString(R.string.bagagem), getString(R.string.cancelar)
        ) {
            reallyCopy()
        }
    }

    private fun reallyCopy() {
        Repo.luggage.clone(cModel)
        findNavController().popBackStack()
    }


    private fun save() {
        validate()
            .catch { T.error(activity, it) }
            .then {
                // parent
                val parent = Repo.luggage.save(
                    vm.luggageId, App.modelUser!!.id, vm.location.value!!,
                    vm.date.value!!, vm.duration.value!!
                )

                // items
                Repo.luggage.saveItems(parent, vm.items)

                mChanged.value = 0

                findNavController().popBackStack()
            }
    }

    private fun validate() = Promise<Boolean> { resolve, reject ->
        when {
            App.modelUser==null -> reject(getString(R.string.usuario_desconhecido))
            vm.location.value?.isNotBlank()!=true -> reject(getString(R.string.preencha_o_destino_da_viagem))
            vm.date.value==null -> reject(getString(R.string.preencha_a_data_da_viagem))
            vm.duration.value==null -> reject(getString(R.string.escolha_a_duracao_da_viagem_em_dias))
            else -> resolve(true)
        }
    }

    private fun reallyAdd(f: File, desc: String, item_id: Long, inspiration_id: Long, look_id: Long) {
        val model = LuggageItem()
        model.desc = desc
        model.item_id = item_id
        model.inspiration_id = inspiration_id
        model.look_id = look_id
        model.path = f.path

        vm.items.add(model)
        mAdapter.notifyItemInserted(vm.items.count()-1)

        updatePackedInfo()

        mChanged.value = 1
    }

    private fun reallyAddText(s: String) {
        val model = LuggageItem()
        model.desc = "Text"
        model.item_id = 0
        model.inspiration_id = 0
        model.look_id = 0
        model.path = s

        vm.items.add(model)
        mAdapter.notifyItemInserted(vm.items.count()-1)

        updatePackedInfo()

        mChanged.value = 1
    }

    override fun dialogStrPickerGetData(identifier: String): Promise<List<String>>? {
        return when(identifier) {
            "location" -> {
                promiseIt(Res.locations())
            }
            "duration" -> {
                val list = (1..100).toList().map { ii -> "$ii" }
                promiseIt(list)
            }
            else -> null
        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val dt = LocalDate.of(year, month + 1, dayOfMonth)
        vm.date.value = dt

        mChanged.value = 1
    }

    override fun dialogStrPickerOnSelected(identifier: String, s: String) {
        when(identifier) {
            "location" -> {
                mChanged.value = 1

                vm.location.value = s
            }
            "duration" -> {
                mChanged.value = 1

                vm.duration.value = s.toIntOrNull()
            }
        }
    }

    override fun dialogPicPathItem(item: DialogPicPathAdapter.Item) {
        reallyAdd(File(item.full), "Item", item.item_id, item.inspiration_id, item.look_id)
    }

    override fun dialogPicPathPickAlbum() {
        ImgProvider.pick(this, 1)
    }

    override fun dialogPicPathPickCamera() {
        ImgProvider.pick(this, 1)
    }

    override fun dialogPicPathText() {
        DialogQuery.show(childFragmentManager, getString(R.string.insira_texto), "")
    }

    override fun dialogQueryOnSubmit(identifier: String, v: String) {
        reallyAddText(v)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_frag_luggage_edit, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId) {
            R.id.menu_action_copy -> copy()
            R.id.menu_action_done -> save()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()

        DialogStrPicker.attach(this)
        DialogPicPath.attach(this)
        DialogQuery.attach(this)
    }

    override fun onStop() {
        super.onStop()

        DialogStrPicker.detach()
        DialogPicPath.detach()
        DialogQuery.detach()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val f = ImgProvider.onActivityResultFirst(requestCode, resultCode, data) ?: return
        reallyAdd(f, "Album", 0, 0, 0)
    }

}