package br.com.adonio.keepcloset.dialogs

import android.app.Dialog
import android.graphics.Point
import android.os.Bundle
import android.view.Gravity
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import br.com.adonio.keepcloset.R

abstract class DialogBase: DialogFragment() {

    override fun onResume() {
        val window = dialog?.window
        val size = Point()
        val display = window!!.windowManager.defaultDisplay
        display.getSize(size)
        window.setLayout((size.x * .95).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
        window.setGravity(Gravity.CENTER)

        super.onResume()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let { restoreUI(it) }
    }

    protected open fun restoreUI(savedInstanceState: Bundle) {}

    override fun onSaveInstanceState(outState: Bundle) {
        saveUI(outState)
        super.onSaveInstanceState(outState)
    }

    protected open fun saveUI(outState: Bundle) {}

}