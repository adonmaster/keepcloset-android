package br.com.adonio.keepcloset.frags.main.closet

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.adonio.keepcloset.models.Item

class FragMainClosetVM: ViewModel() {

    val modelEditing = MutableLiveData<Item>()

    var items = listOf<Item>()

}