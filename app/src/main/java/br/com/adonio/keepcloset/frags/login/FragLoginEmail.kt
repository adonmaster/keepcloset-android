package br.com.adonio.keepcloset.frags.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.extensions.setEnabledEx
import br.com.adonio.keepcloset.extensions.snackError
import br.com.adonio.keepcloset.helpers.Http
import br.com.adonio.keepcloset.helpers.HttpFormData
import br.com.adonio.keepcloset.repos.Repo
import kotlinx.android.synthetic.main.frag_login_email.*

class FragLoginEmail: Fragment() {

    // computed

    // state
    private lateinit var vm: FragLoginEmailVM

    // methods

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.frag_login_email, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(this).get(FragLoginEmailVM::class.java)

        vm.loading.observe(this, Observer {
            ui_submit.setEnabledEx(!it)
            ui_progress.isVisible = it
        })

        vm.error.observe(this, Observer { reason ->
            reason?.let { ui_submit.snackError(it) }
        })

        ui_submit.setOnClickListener { submit2() }
    }

    private fun submit() {
        vm.loading.value = true
        vm.error.value = null

        val email = ui_email.text.toString()
        val params = HttpFormData().put("email", email)

        // google play account
        // uid: google_play_acc3487@keepcloset.com.br
        if (email == "google_play_acc3487@keepcloset.com.br") {
            afterSubmit(email)
            return
        }

        Http.post("register/code", params)
            .then {
                afterSubmit(email)
            }
            .catch { reason ->
                vm.error.value = reason
            }
            .finally {
                vm.loading.value = false
            }
    }

    private fun submit2()
    {
        vm.error.value = null;
        val email = ui_email.text.toString();

        if (email.trim().isEmpty()) {
            vm.error.value = "Email vazio";
            return
        }

        App.shared.model.user = Repo.user.registerAndActivate(email)
        activity?.finish()
    }

    private fun afterSubmit(email: String) {
        findNavController().navigate(
            R.id.action_to_login_check,
            Bundle().apply { putString("email", email) }
        )
    }

}
