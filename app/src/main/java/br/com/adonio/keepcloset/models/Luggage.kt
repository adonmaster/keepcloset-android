package br.com.adonio.keepcloset.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import br.com.adonio.keepcloset.models.presenters.LuggagePresenter
import br.com.adonio.keepcloset.repos.Repo
import org.threeten.bp.LocalDate

@Entity(tableName = "luggages")
class Luggage: BaseModel() {

    @ColumnInfo(name = "user_id")
    var userId: Long = 0

    lateinit var date: LocalDate
    var duration = 0
    lateinit var location: String

    // presenter
    val p get() = cache.getGranted("p", { LuggagePresenter(this) })

    // relationships
    val items get() = cache.getGranted("rel@items", {
        Repo.luggage.getItemsFor(id)
    })

}