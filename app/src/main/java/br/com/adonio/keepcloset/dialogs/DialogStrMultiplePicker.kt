package br.com.adonio.keepcloset.dialogs

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.adapters.DialogStrMultiplePickerAdapter
import br.com.adonio.keepcloset.extensions.setVisible
import br.com.adonio.keepcloset.helpers.ListWatcher
import br.com.adonio.keepcloset.helpers.Promise
import br.com.adonio.keepcloset.helpers.Task
import kotlinx.android.synthetic.main.dialog_str_multiple_picker.*


class DialogStrMultiplePicker: DialogBase() {

    // static
    companion object {
        fun show(fragmentManager: FragmentManager?, title: String, identifier: String="", alreadySelected: List<String>?=null, maxCount: Int=-1) {
            if (fragmentManager==null) return

            val dialog = DialogStrMultiplePicker()
            val bundle = Bundle()
            bundle.putString("identifier", identifier)
            bundle.putStringArrayList("alreadySelected", alreadySelected?.let { ArrayList(it) } ?: arrayListOf())
            bundle.putString("title", title)
            bundle.putInt("maxCount", maxCount)
            dialog.arguments = bundle

            dialog.show(fragmentManager, "DialogStrMultiplePicker")
        }

        private var listener: Listener? = null

        fun attach(listener: Listener) {
            this.listener = listener
        }
        fun detach() {
            this.listener = null
        }
    }

    // state
    private lateinit var mAdapter: DialogStrMultiplePickerAdapter

    data class Item(val uid: String, val text: String, val payload: Any?)
    private var mData = listOf<Item>()
    private val mSelected = ListWatcher<String>().observe { selList, _, _ ->
        // title
        val countPage = if (cMaxCount > 0) "${selList.count()}/$cMaxCount" else "${selList.count()}"
        val suffix = if (selList.isEmpty()) "" else " ($countPage)"
        dialog?.setTitle(cTitle + suffix)
    }

    // computed
    private val cIdentifier
        get() = arguments!!.getString("identifier")!!
    private val cTitle
        get() = arguments!!.getString("title")!!
    private val cMaxCount
        get() = arguments!!.getInt("maxCount")
    private val cAlreadySelected
        get() = arguments!!.getStringArrayList("alreadySelected")!!

    // methods

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.setTitle(cTitle)
        return inflater.inflate(R.layout.dialog_str_multiple_picker, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAdapter = DialogStrMultiplePickerAdapter(object : DialogStrMultiplePickerAdapter.Listener {
            override fun onSelect(selected: Item, position: Int) {
                addSelected(selected)
            }
            override fun isItemSelected(uid: String): Boolean {
                return mSelected.contains(uid)
            }
            override fun onEmpty(option: Boolean) {
                ui_empty.setVisible(option)
            }
        })

        // initial data
        ui_progress.setVisible(true)
        listener?.dialogStrMultiplePickerGetData(cIdentifier)
            ?.then { data ->
                mData = data
                mSelected.put(
                    mData
                        .filter { cAlreadySelected.contains(it.uid) }
                        .map { it.uid }
                )

                mAdapter.refresh(data)
            }
            ?.finally {
                ui_progress.setVisible(false)
            }

        ui_rv.adapter = mAdapter
        ui_rv.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        ui_rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
        ui_rv.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        ui_ed_query.setText("")
        ui_ed_query.hint = getString(R.string.digite_aqui)
        ui_ed_query.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                filterByText(s?.toString())
            }
        })

        ui_btn_clear_all.setOnClickListener {
            mSelected.clear()
            mAdapter.notifyDataSetChanged()
        }

        ui_btn_cancel.setOnClickListener { dismissAllowingStateLoss() }

        ui_btn_submit.setOnClickListener {
            val selected = mData.filter { mSelected.contains(it.uid) }
            listener?.dialogStrMultiplePickerOnSelected(cIdentifier, selected)
            dismissAllowingStateLoss()
        }
    }

    private fun mDataIndex(uid: String): Int? {
        return mData
            .indexOfFirst { it.uid == uid }
            .takeIf { it > -1 }
    }

    private fun addSelected(item: Item) {
        val index = mSelected.indexOf(item.uid)
        if (index == -1) {
            mSelected.add(item.uid)
            mDataIndex(item.uid)?.let { mAdapter.notifyItemChanged(it) }

            if (mSelected.count() > cMaxCount && cMaxCount > 0) {
                val removedUid = mSelected.removeAt(0)
                mDataIndex(removedUid)?.let { mAdapter.notifyItemChanged(it) }
            }
        } else {
            val removedUId = mSelected.removeAt(index)
            mDataIndex(removedUId)?.let { mAdapter.notifyItemChanged(it) }
        }
    }

    private fun filterByText(q: String?) {
        Task.debounce("DialogStrMultiplePicker@filter", 200) {
            val data = if (q?.trim()?.isEmpty() != false) mData
            else mData.filter { it.text.contains(q, true) }

            mAdapter.refresh(data)
        }
    }

    // listener
    interface Listener {
        fun dialogStrMultiplePickerGetData(identifier: String): Promise<List<Item>>?
        fun dialogStrMultiplePickerOnSelected(identifier: String, selected: List<Item>)
    }

}