package br.com.adonio.keepcloset.frags.main.closet.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Act
import br.com.adonio.keepcloset.helpers.CategorizedContainer
import br.com.adonio.keepcloset.models.Item
import kotlinx.android.synthetic.main.rv_main_closet.view.*
import kotlinx.android.synthetic.main.rv_main_closet_header.view.*

class FragMainClosetAdapter(private val mData: CategorizedContainer<Item>, private val listener: Listener?=null): RecyclerView.Adapter<FragMainClosetAdapter.VH>() {

    override fun getItemCount(): Int {
        return mData.count().also {
            listener?.onEmpty(it <= 0)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return mData[position]?.getType() ?: -1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val resId = if (mData.isTypeCategory(viewType)) R.layout.rv_main_closet_header
            else R.layout.rv_main_closet

        val view = Act.inflate(resId, parent,false)!!

        return VH(view)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        mData[position]
            ?.whenData {
                it.p().loadThumb(holder.uiImg, true)
            }
            ?.whenCategory {
                holder.uiLbl?.text = it
            }
    }

    // vh
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiImg: ImageView? = itemView.ui_img
        val uiLbl: TextView? = itemView.ui_lbl

        init {
            itemView.setOnClickListener {
                mData[adapterPosition]?.let { item ->
                    listener?.onSelected(item)
                }
            }
        }
    }

    // listener
    interface Listener {
        fun onSelected(item: CategorizedContainer.Item<Item>)
        fun onEmpty(option: Boolean)
    }

}