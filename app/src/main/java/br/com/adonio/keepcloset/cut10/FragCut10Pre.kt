package br.com.adonio.keepcloset.cut10

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.adonio.keepcloset.Cut10VM
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.ImgCorrector
import kotlinx.android.synthetic.main.frag_cut10_pre.*

class FragCut10Pre: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_cut10_pre, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val vm = ViewModelProviders.of(requireActivity()).get(Cut10VM::class.java)

        vm.loading.observe(viewLifecycleOwner, Observer {
            ui_loading?.isVisible = it
        })

        ui_btn_clear.setOnClickListener {
            ui_pre_draw.clear()
        }

        ui_btn_next.setOnClickListener {
            vm.loading.value = true
            ui_pre_draw.requestMaskedBmp { bmp, mask ->
                vm.loading.value = false

                vm.bmp = bmp
                vm.bmpMasked = mask
                findNavController().navigate(R.id.to_second, null)
            }
        }

        ui_pre_draw.post {
//            val img = BitmapFactory.decodeFile(vm.path)
            ImgCorrector.handleSamplingAndRotationBitmap(vm.path)
                ?.let {
                    ui_pre_draw.setBitmap(it)
                }
        }
    }

}