package br.com.adonio.keepcloset.rmodels

import br.com.adonio.keepcloset.helpers.JParser
import org.json.JSONObject

class SupportRModel(
    val title: String?,
    val desc: String?,
    val url: String?
) {

    companion object {
        fun parse(j: JSONObject) = JParser(j) {
            arr("pack") {
                store("title")
                store("desc")
                store("url")
            }
        }.parse {
            arr("pack") {
                SupportRModel(getNexto(), getNexto(), getNexto())
            }
        }
        val custom = """
            {
              "message": "Maravilha",
              "code": 200,
              "pack": [
                {
                  "title": "SEJA BEM VINDO",
                  "desc": "Mensagem de boas vindas...",
                  "url": "http://keepcloset.com.br/mobile/support/welcome"
                }
              ]
            }
        """.trimIndent()
    }

}