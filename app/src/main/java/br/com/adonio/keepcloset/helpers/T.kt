package br.com.adonio.keepcloset.helpers

import android.app.Activity
import com.tfb.fbtoast.FBToast

object T {

    enum class Duration {
        LONG {
            override fun translateToFb() = FBToast.LENGTH_LONG
        },
        SHORT {
            override fun translateToFb() = FBToast.LENGTH_SHORT
        };

        abstract fun translateToFb(): Int
    }

    fun error(act: Activity?, reason: String, duration: Duration=Duration.LONG) {
        act?.let {
            FBToast.errorToast(it,reason, duration.translateToFb())
        }
    }

}