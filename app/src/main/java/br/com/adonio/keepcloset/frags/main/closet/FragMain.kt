package br.com.adonio.keepcloset.frags.main.closet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import br.com.adonio.keepcloset.MainActivity
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.services.AdService
import kotlinx.android.synthetic.main.frag_main.*

class FragMain: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.frag_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // setup top navigation
        val mainActivity = activity as MainActivity
        val navController = Navigation.findNavController(mainActivity, R.id.ui_nav_main)
        val topLevelDestinationIds = setOf(
            R.id.nav_main_frag_closet,
            R.id.nav_main_frag_inspiration,
            R.id.nav_main_frag_look,
            R.id.nav_main_frag_schedule,
            R.id.nav_main_frag_luggage
        )
        mainActivity.setupChildNavController(navController, topLevelDestinationIds)
        NavigationUI.setupWithNavController(ui_bot_nav, navController)
    }

    override fun onStart() {
        super.onStart()

        //
        AdService.loadAds()
    }

}