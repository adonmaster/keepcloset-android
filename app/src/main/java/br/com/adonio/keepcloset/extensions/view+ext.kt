@file:Suppress("unused")

package br.com.adonio.keepcloset.extensions

import android.graphics.Bitmap
import android.graphics.Point
import android.graphics.PointF
import android.view.View
import androidx.annotation.ColorRes
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Cl
import com.google.android.material.snackbar.Snackbar


inline fun View.snack(message: String, length: Int = Snackbar.LENGTH_LONG, f: Snackbar.()->Unit) {
    val snack = Snackbar.make(this, message, length)
    snack.f()
    snack.show()
}

fun View.snack(message: String, length: Int) {
    snack(message, length) {}
}

fun View.snack(message: String, cb: ((View)->Unit)?=null) {
    snack(message, Snackbar.LENGTH_INDEFINITE) { cb?.let { setAction("OK", it) } }
}

fun View.snackOk(message: String, cb: ((View)->Unit)?=null) {
    snack(message, Snackbar.LENGTH_INDEFINITE) { setAction("OK") { cb?.invoke(it) } }
}

fun View.snackError(message: String, cb: ((View)->Unit)?=null) {
    snackAction(message, context.getString(R.string.ok_entao), R.color.md_red_500, cb)
}

fun View.snackSuccess(message: String, cb: ((View)->Unit)?=null) {
    snackAction(message, context.getString(R.string.maravilha), R.color.md_green_500, cb)
}

fun View.snackAction(message: String, actionMessage: String, @ColorRes actionColor: Int, cb: ((View)->Unit)?=null) {
    snack(message, Snackbar.LENGTH_INDEFINITE) {
        setActionTextColor(Cl.res(context, actionColor))
        setAction(actionMessage) { cb?.invoke(it) }
    }
}

fun View.setVisible(visible: Boolean=false) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.isVisible() = visibility == View.VISIBLE

fun View.toggleVisibility(): Boolean {
    val isVisible = visibility == View.VISIBLE
    setVisible(!isVisible)
    return !isVisible
}

fun View.getLocationInScreen(): Point {
    val arr = IntArray(2)
    this.getLocationOnScreen(arr)
    return Point(arr[0], arr[1])
}


fun View.show()
{
    alpha = 0f
    visibility = View.VISIBLE

    clearAnimation()
    animate()
        .setDuration(300)
        .alpha(1f)
        .start()
}

fun View.hide()
{
    clearAnimation()
    animate()
        .setDuration(300)
        .alpha(0f)
        .withEndAction {
            visibility = View.GONE
        }
        .start()
}

fun View.setEnabledEx(option: Boolean) {
    isEnabled = option
    alpha = if (option) 1.0f else 0.27f
}