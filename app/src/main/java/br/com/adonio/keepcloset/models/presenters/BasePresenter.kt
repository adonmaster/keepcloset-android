package br.com.adonio.keepcloset.models.presenters

abstract class BasePresenter<M>(protected val model: M)