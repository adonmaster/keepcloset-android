package br.com.adonio.keepcloset.frags.main.inspiration

import androidx.lifecycle.ViewModel
import br.com.adonio.keepcloset.models.Inspiration

class FragMainInspirationVM: ViewModel() {

    var selectedRawPosition: Int = 0
    var selectedRawList: List<Inspiration> = listOf()

}