package br.com.adonio.keepcloset.frags.main.look

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class FragMainLookEditFVM: ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FragMainLookEditVM(false) as T
    }

}
