package br.com.adonio.keepcloset.helpers

object Num {

    fun between(n: Float, min: Float, max: Float): Float {
        if (n > max) return max
        if (n < min) return min
        return n
    }

}