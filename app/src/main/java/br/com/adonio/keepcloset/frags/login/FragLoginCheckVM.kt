package br.com.adonio.keepcloset.frags.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class FragLoginCheckVM: ViewModel() {

    val loading = MutableLiveData<Boolean>().apply { value = false }
    val error = MutableLiveData<String?>().apply { value = null }

}