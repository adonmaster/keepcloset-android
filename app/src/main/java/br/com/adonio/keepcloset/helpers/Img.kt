package br.com.adonio.keepcloset.helpers

import android.graphics.*
import android.net.Network
import android.view.View
import android.widget.ImageView
import androidx.annotation.DrawableRes
import br.com.adonio.keepcloset.R
import com.squareup.picasso.*
import java.io.File
import java.lang.Exception

private abstract class ImgResource {
    abstract fun buildRequest(): RequestCreator
}

private class StrResource(val s: String?): ImgResource() {
    override fun buildRequest(): RequestCreator {
        return Picasso.get().load(s)
    }
}

private class FileResource(val file: File): ImgResource() {
    override fun buildRequest(): RequestCreator {
        return Picasso.get().load(file)
    }
}

object Img {

    private fun load(resource: ImgResource, imgView: ImageView?, refresh: Boolean=false, onDone: ((ImageView)->Unit)?=null) {
        imgView?.let {
            resource.buildRequest()
                .also {  builder ->
                    if (refresh) {
                        builder.networkPolicy(NetworkPolicy.NO_CACHE)
                        builder.memoryPolicy(MemoryPolicy.NO_CACHE)
                    }
                }
                .placeholder(R.drawable.img_ph)
                .error(R.drawable.img_404)
                .into(it, object : Callback {
                    override fun onSuccess() {
                        onDone?.invoke(it)
                    }
                    override fun onError(e: Exception?) { }
                })
        }
    }

    fun load(path: String?, imgView: ImageView?, onDone: ((ImageView) -> Unit)?=null) {
        load(StrResource(path), imgView, false, onDone)
    }

    fun load(file: File?, imgView: ImageView?, refresh: Boolean=false, onDone: ((ImageView) -> Unit)?=null) {
        val f = guard(file) { return }
        load(FileResource(f), imgView, refresh, onDone)
    }

    fun loadAvatar(path: String?, imgView: ImageView?) {
        val u = path?.trim()
        if (u==null) {
            val ctx = imgView?.context ?: return
            imgView.setImageDrawable(ctx.getDrawable(R.drawable.img_guy))
        } else {
            load(u, imgView) {}
        }
    }

    fun thumbFrom(targetBmp: Bitmap, reqHeightInPixels: Int=200, reqWidthInPixels: Int=200): Bitmap {
        val matrix = Matrix()
            .apply {
                setRectToRect(
                    RectF(0f, 0f, targetBmp.width.toFloat(), targetBmp.height.toFloat()),
                    RectF(0f, 0f, reqWidthInPixels.toFloat(), reqHeightInPixels.toFloat()),
                    Matrix.ScaleToFit.CENTER
                )
            }
        return Bitmap.createBitmap(targetBmp, 0, 0, targetBmp.width, targetBmp.height, matrix, true)
    }


    fun bitmapFromView(view: View): Bitmap {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        val bgDrawable = view.background
        if (bgDrawable != null) {
            bgDrawable.draw(canvas)
        } else {
            canvas.drawColor(Color.WHITE)
        }
        view.draw(canvas)
        return bitmap!!
    }

    fun load(@DrawableRes id: Int, imageView: ImageView?) {
        if (imageView==null) return
        Picasso.get().load(id)
            .error(R.drawable.img_404)
            .into(imageView)
    }
}