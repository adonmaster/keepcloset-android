package br.com.adonio.keepcloset

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class Cut10VM: ViewModel() {

    val loading = MutableLiveData<Boolean>().also { it.value = false }
    var path = ""

    lateinit var bmp: Bitmap
    lateinit var bmpMasked: Bitmap
    lateinit var bmpResult: Bitmap

}