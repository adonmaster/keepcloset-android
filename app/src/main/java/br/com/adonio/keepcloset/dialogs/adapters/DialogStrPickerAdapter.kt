package br.com.adonio.keepcloset.dialogs.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Act
import kotlinx.android.synthetic.main.rv_dialog_str_picker.view.*

class DialogStrPickerAdapter(private val listener: Listener): RecyclerView.Adapter<DialogStrPickerAdapter.VH>() {

    // state
    private var mData = listOf<String>()

    // methods
    fun refresh(data: List<String>) {
        mData = data
        notifyDataSetChanged()
    }

    // adapter

    override fun getItemCount(): Int {
         return mData.count().also {
             listener.onEmpty(it == 0)
         }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(Act.inflate(R.layout.rv_dialog_str_picker, parent, false)!!)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.uiLbl?.setText(mData[position])
    }


    // vh
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiLbl: TextView? = itemView.ui_lbl
        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                listener.onSelect(mData[position], position)
            }
        }
    }

    // listener
    interface Listener {
        fun onSelect(s: String, position: Int)
        fun onEmpty(option: Boolean)
    }

}