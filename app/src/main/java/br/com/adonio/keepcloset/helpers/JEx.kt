package br.com.adonio.keepcloset.helpers

import br.com.adonio.keepcloset.extensions.isNotNull
import org.json.JSONObject

fun JEx(json: String?, block: JExDsl.()->Unit): JExResult {
    if (json!=null) {
        try {
            val jsonObject = JSONObject(json)
            return JEx(jsonObject, block)
        } catch (e: Throwable) {
            JExResult(JExDsl(null), e)
        }
    }
    return JExResult(JExDsl(null), Throwable("Vazio"))
}

fun JEx(json: JSONObject?, block: JExDsl.()->Unit): JExResult {
    val jExInstance = JExDsl(json)
    try {
        block(jExInstance)
    } catch (e: Throwable) {
        return JExResult(jExInstance, e)
    }
    return JExResult(jExInstance)
}

class JExDsl(private val json: JSONObject?) {

    val keys: List<String>
        get() = json?.keys()?.asSequence()?.toList() ?: listOf()

    fun o(field: String, block: JExDsl.()->Unit) {
        val j = if (json!!.isNull(field)) null else json.getJSONObject(field)
        JEx(j, block)
    }

    fun oGuarded(field: String, block: JExDsl.()->Unit) {
        val j = if (json!!.isNull(field)) null else json.getJSONObject(field)
        if (j!=null) JEx(j, block)
    }

    fun str(field: String, default: String?=null): String? {
        if (json!!.isNotNull(field)) return json.getString(field)
        return default
    }

    fun gStr(field: String, default: String=""): String {
        return str(field, default)!!
    }

    fun arr(field: String, block: JExDsl.()->Unit) {
        val array = json!!.getJSONArray(field)
        for (index in 0..array.length()) {
            JEx(array.getJSONObject(index), block)
        }
    }

    fun arrStr(field: String, block: (String)->Boolean) {
        val array = json!!.getJSONArray(field)
        for (index in 0..array.length()) {
            if ( ! block(array.getString(index))) break
        }
    }

    fun arrStrs(field: String, block: (String)->Unit) {
        arrStr(field) {
            block(it)
            true
        }
    }

        fun long(field: String, default: Long?=null): Long? {
        if (json!!.isNotNull(field)) return json.getLong(field)
        return default
    }

    fun i(field: String, default: Int?): Int? {
        if (json!!.isNotNull(field)) return json.getInt(field)
        return default
    }

    fun gi(field: String): Int {
        return json!!.getInt(field)
    }

    fun gLong(field: String, default: Long=0): Long {
        return long(field, default)!!
    }

    fun b(field: String): Boolean {
        return json!!.getBoolean(field)
    }

    fun d(field: String, default: Double?=null): Double? {
        if (json!!.isNotNull(field)) return json.getDouble(field)
        return default
    }

    fun gd(field: String, default: Double=0.0): Double {
        return d(field, default)!!
    }

    fun getObject() = json
}

class JExResult(private val j: JExDsl, private val error: Throwable?=null) {

    fun onError(cb: (String)->Unit): JExResult {
        if (error!=null) cb(error.localizedMessage)
        return this
    }

}