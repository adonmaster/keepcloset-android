package br.com.adonio.keepcloset.models

data class ItemSuPaths(
    val id: Long,
    val path: String,
    val path_thumb: String
)