package br.com.adonio.keepcloset.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore


@Entity(tableName = "luggage_item")
class LuggageItem: BaseModel() {

    var luggage_id = 0L

    var item_id = 0L
    var inspiration_id = 0L
    var look_id = 0L

    lateinit var path: String
    var desc = ""

    @ColumnInfo(name = "is_packed")
    var isPacked = false

    @Ignore
    fun cloneUnlinked(): LuggageItem {
        val new = LuggageItem()

        new.luggage_id = 0L

        new.item_id = this.item_id
        new.inspiration_id = this.inspiration_id
        new.look_id = this.look_id
        new.path = this.path
        new.desc = this.desc
        new.isPacked = this.isPacked

        return new
    }

}