package br.com.adonio.keepcloset.helpers

fun <T> ter(obj: Any?, yep: T, nop: T): T {
    obj!=null && return yep
    return nop
}

fun <T> ter(b: Boolean, yep: T, nop: T): T {
    b && return yep
    return nop
}

inline fun <reified T> coalesce(first: T?, vararg rest: T?): T {
    val list: Array<T?> = arrayOf(first) + rest
    for (a in list) if (a != null) return a
    return list.last()!!
}