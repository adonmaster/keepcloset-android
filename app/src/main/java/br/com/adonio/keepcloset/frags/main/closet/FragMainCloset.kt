package br.com.adonio.keepcloset.frags.main.closet

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.DialogAd
import br.com.adonio.keepcloset.extensions.setVisible
import br.com.adonio.keepcloset.frags.main.closet.adapters.FragMainClosetAdapter
import br.com.adonio.keepcloset.helpers.*
import br.com.adonio.keepcloset.models.Item
import br.com.adonio.keepcloset.repos.Repo
import br.com.adonio.keepcloset.services.AdService
import br.com.adonio.keepcloset.structs.SimpleTextWatcher
import kotlinx.android.synthetic.main.frag_main_closet.*

class FragMainCloset: Fragment(), DialogAd.Listener {

    // state
    private lateinit var vm: FragMainClosetVM
    private lateinit var mAdapter: FragMainClosetAdapter

    private var mAdapterData = CategorizedContainer<Item>(null) { it.category ?: getString(R.string.sem_categoria) }

    // override
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.frag_main_closet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.vm = ViewModelProviders.of(activity!!).get(FragMainClosetVM::class.java)

        // adapter
        mAdapter = FragMainClosetAdapter(
            mAdapterData,
            object : FragMainClosetAdapter.Listener {
                override fun onSelected(item: CategorizedContainer.Item<Item>) {
                    item.whenData { i ->
                        val position = vm.items.indexOf(i)
                        onSelected(i, position)
                    }
                }
                override fun onEmpty(option: Boolean) {
                    ui_empty.setVisible(option)
                }
            })

        // rv
        ui_rv.adapter = mAdapter
        ui_rv.layoutManager = GridLayoutManager(context, 3).also {
            it.spanSizeLookup = object: GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (mAdapterData[position]?.isTypeCategory() == true) 3 else 1
                }
            }
        }
        ui_rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_slide_right)

        //
        ui_btn_add.setOnClickListener {
            if (AdService.shouldShowAd()) {
                AdService.showAdDialog(childFragmentManager)
            } else {
                add()
            }
        }

        //
        ui_search.addTextChangedListener(object: SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Task.debounce("FragMainCloset@query", 900) {
                    updateData()
                }
            }
        })
        ui_search.setOnEditorActionListener { _, actionId, _ ->
            when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    Act.hideKeyboard(this@FragMainCloset)
                    true
                }
                else -> false
            }
        }

        //
        updateData()
    }

    private fun updateData() {
        synced("FragMainCloset@updateData") {
            Repo.item.queryAll(App.modelUser, ui_search.text?.toString())
                .then { list ->
                    vm.items = list

                    mAdapterData.update(list)
                    mAdapter.notifyDataSetChanged()

                    syncRelease()
                }
        }
    }

    private fun add() {
        Act.hideKeyboard(this)

        ImgProvider.pick(this, 1)
    }

    private fun addingNewModel(path: String) {
        Act.hideKeyboard(this)

        val userId = guard(App.modelUser?.id) { return }

        vm.modelEditing.value = Item.f(userId, path)
        findNavController().navigate(R.id.action_to_frag_main_closet_edit)
    }

    private fun onSelected(item: Item, position: Int) {
        Act.hideKeyboard(this)

        vm.modelEditing.value = item
        FragClosetSlider.navigate(this, R.id.action_to_frag_closet_slider, position)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // image picker
        val f = ImgProvider.onActivityResultFirst(requestCode, resultCode, data)
        if (f!=null) {
            addingNewModel(f.path)
        }
    }

    override fun onStart() {
        super.onStart()

        DialogAd.attach(this)
    }

    override fun onStop() {
        super.onStop()

        DialogAd.detach(this)
    }

    //
    override fun dialogAdOnCancel() {
        add()
    }
}