package br.com.adonio.keepcloset.models

import androidx.room.Entity

@Entity(tableName = "look_item")
class LookItem: BaseModel() {

    var look_id: Long = 0

    lateinit var source_type: String
    var source_id: Long = 0

    lateinit var path: String

    var position: Int = 0
    var pos_x: Float = 0f
    var pos_y: Float = 0f
    var scale: Float = 1f
    var rotation: Float = 0f


}