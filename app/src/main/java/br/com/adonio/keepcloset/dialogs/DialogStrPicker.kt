package br.com.adonio.keepcloset.dialogs

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.adapters.DialogStrPickerAdapter
import br.com.adonio.keepcloset.extensions.setEnabledEx
import br.com.adonio.keepcloset.extensions.setVisible
import br.com.adonio.keepcloset.helpers.Promise
import br.com.adonio.keepcloset.helpers.Task
import kotlinx.android.synthetic.main.dialog_str_picker.*

class DialogStrPicker: DialogBase() {

    // static
    companion object {
        fun show(fragmentManager: FragmentManager?, title: String, identifier: String="", hideSearch:Boolean=false) {
            if (fragmentManager==null) return

            val dialog = DialogStrPicker()
            val bundle = Bundle()
            bundle.putString("identifier", identifier)
            bundle.putString("title", title)
            bundle.putBoolean("hideSearch", hideSearch)
            dialog.arguments = bundle

            dialog.show(fragmentManager, "DialogStrPicker")
        }

        private var listener: Listener? = null

        fun attach(listener: Listener) {
            this.listener = listener
        }
        fun detach() {
            this.listener = null
        }
    }

    // state
    private lateinit var mAdapter: DialogStrPickerAdapter
    private var mData = listOf<String>()

    // computed
    private val cIdentifier
        get() = arguments!!.getString("identifier")!!

    private val cTitle
        get() = arguments!!.getString("title")!!

    private val cHideSearch
        get() = arguments!!.getBoolean("hideSearch")

    // methods

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.setTitle(cTitle)
        return inflater.inflate(R.layout.dialog_str_picker, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAdapter = DialogStrPickerAdapter(object : DialogStrPickerAdapter.Listener {
            override fun onSelect(s: String, position: Int) {
                listener?.dialogStrPickerOnSelected(cIdentifier, s)
                dismissAllowingStateLoss()
            }
            override fun onEmpty(option: Boolean) {
                ui_empty.setVisible(option)
            }
        })

        // initial data
        ui_progress.setVisible(true)
        listener?.dialogStrPickerGetData(cIdentifier)
            ?.then {
                mData = it
                mAdapter.refresh(it)
            }
            ?.finally {
                ui_progress.setVisible(false)
            }

        ui_rv.adapter = mAdapter
        ui_rv.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        ui_rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
        ui_rv.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))


        ui_ed_query.setVisible(! cHideSearch)
        ui_ed_query.setText("")
        ui_ed_query.hint = getString(R.string.digite_aqui)
        ui_ed_query.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                filter(s?.toString())
                ui_btn_submit.setEnabledEx(s?.toString()?.trim()?.isNotEmpty() != false)
            }
        })

        ui_btn_cancel.setOnClickListener { dismissAllowingStateLoss() }

        ui_btn_submit.setVisible(! cHideSearch)
        ui_btn_submit.setEnabledEx(false)
        ui_btn_submit.setOnClickListener {
            listener?.dialogStrPickerOnSelected(cIdentifier, ui_ed_query.text.toString())
            dismissAllowingStateLoss()
        }
    }

    private fun filter(q: String?) {
        Task.debounce("DialogStrPicker@filter", 200) {
            val data = if (q?.trim()?.isEmpty() != false) mData
                else mData.filter { it.contains(q, true) }

            mAdapter.refresh(data)
        }
    }


    // listener
    interface Listener {
        fun dialogStrPickerGetData(identifier: String): Promise<List<String>>?
        fun dialogStrPickerOnSelected(identifier: String, s: String)
    }

}