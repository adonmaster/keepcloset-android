package br.com.adonio.keepcloset.helpers

import android.app.Activity
import android.content.Intent
import android.os.Parcelable
import androidx.fragment.app.Fragment
import com.nguyenhoanglam.imagepicker.model.Config
import com.nguyenhoanglam.imagepicker.model.Image
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker
import java.io.File

object ImgProvider {

    fun pick(fragment: Fragment, maxCount: Int=1) {
        ImagePicker.with(fragment)              //  Initialize ImagePicker with activity or fragment context
            .setToolbarColor("#212121")         //  Toolbar color
            .setStatusBarColor("#000000")       //  StatusBar color (works with SDK >= 21  )
            .setToolbarTextColor("#FFFFFF")     //  Toolbar text color (Title and Done button)
            .setToolbarIconColor("#FFFFFF")     //  Toolbar icon color (Back and Camera button)
            .setProgressBarColor("#4CAF50")     //  ProgressBar color
            .setBackgroundColor("#212121")      //  Background color
            .setCameraOnly(false)               //  Camera mode
            .setMultipleMode(maxCount > 1)              //  Select multiple images or single image
            .setFolderMode(true)                //  Folder mode
            .setShowCamera(true)                //  Show camera button
            .setFolderTitle("Album")           //  Folder title (works with FolderMode = true)
            .setImageTitle("Galeria")         //  Image title (works with FolderMode = false)
            .setDoneTitle("OK")               //  Done button title
            .setLimitMessage("Você atingiu o limite de seleção")    // Selection limit message
            .setMaxSize(maxCount)                     //  Max images can be selected
//            .setSavePath("ImagePicker")         //  Image capture folder name
//            .setSelectedImages(images)          //  Selected images
            .setAlwaysShowDoneButton(false)      //  Set always show done button in multiple mode
            .setRequestCode(100)                //  Set request code, default Config.RC_PICK_IMAGES
            // .setKeepScreenOn(true)              //  Keep screen on when selecting images
            .start()
    }

    private fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): List<String>?
    {
        return if (requestCode == Config.RC_PICK_IMAGES && resultCode == Activity.RESULT_OK && data != null) {
            @Suppress("UNCHECKED_CAST")
            val images = data.getParcelableArrayListExtra<Parcelable>(Config.EXTRA_IMAGES) as List<Image>

            images.map { it.path }.takeIf { it.count() > 0 }
        } else {
            null
        }
    }

    fun onActivityResultFirst(requestCode: Int, resultCode: Int, data: Intent?): File?
    {
        val path = onActivityResult(requestCode, resultCode, data)?.firstOrNull()
            ?: return null

        val bmp = ImgCorrector.handleSamplingAndRotationBitmap(path) ?: return null

        //
        return F.saveExternalSynced("img-provider", "last.dat", bmp)
    }

    fun onActivityResultAll(requestCode: Int, resultCode: Int, data: Intent?): List<String>? {
        return onActivityResult(requestCode, resultCode, data)
    }

}
