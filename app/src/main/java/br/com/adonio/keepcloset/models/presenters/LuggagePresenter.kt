package br.com.adonio.keepcloset.models.presenters

import br.com.adonio.keepcloset.models.Luggage
import org.threeten.bp.LocalDate

class LuggagePresenter(model: Luggage) : BasePresenter<Luggage>(model) {

    fun isDatePast(): Boolean {
        return model.date < LocalDate.now()
    }

}