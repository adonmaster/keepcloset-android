package br.com.adonio.keepcloset.repos

import android.content.Context
import br.com.adonio.keepcloset.App

class PrefRepo {

    private val pref
        get() = App.shared.getSharedPreferences("main.${App.modelUser?.id ?: 0}.pref", Context.MODE_PRIVATE)

    fun add(key: String, v: String, position: Int?=null) {
        val r = (getList(key) ?: listOf()).toMutableList()

        r.remove(v)
        if (position!=null) {
            r.add(position, v)
        } else {
            r.add(v)
        }
        with(pref.edit()) {
            putStringSet(key, r.toSet())
            apply()
        }
    }

    fun getList(key: String): List<String>? {
        return pref.getStringSet(key, null)?.toList()
    }

    fun setList(key: String, list: List<String>) {
        pref
            .edit()
            .putStringSet(key, list.toSet())
            .apply()
    }

    fun get(key: String): String? {
        return pref.getString(key, null)
    }

    fun put(key: String, s: String) {
        pref.edit().putString(key, s).apply()
    }

}