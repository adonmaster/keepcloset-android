package br.com.adonio.keepcloset.frags.main.closet

import android.os.Bundle
import android.view.*
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.DialogOk
import br.com.adonio.keepcloset.models.Item
import br.com.adonio.keepcloset.repos.Repo
import kotlinx.android.synthetic.main.frag_closet_slider.*

class FragClosetSlider: Fragment() {

    //
    companion object {
        fun navigate(frag: Fragment, @IdRes id: Int, fragMainClosetPosition: Int) {
            val bundle = Bundle()
            bundle.putInt("fragMainClosetPosition", fragMainClosetPosition)
            frag.findNavController().navigate(id, bundle)
        }
    }

    // state
    private lateinit var vmParent: FragMainClosetVM

    // computed
    private var mSelectedIndex = -1

    // methods
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.frag_closet_slider, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        vmParent = ViewModelProviders.of(activity!!).get(FragMainClosetVM::class.java)

        //
        mSelectedIndex = arguments!!.getInt("fragMainClosetPosition")

        //
        ui_pager.adapter = object: FragmentStatePagerAdapter(childFragmentManager) {
            override fun getCount(): Int {
                return vmParent.items.count()
            }
            override fun getItem(position: Int): Fragment {
                return FragClosetSliderItem.f(position)
            }
        }
        ui_pager.currentItem = mSelectedIndex
        ui_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                mSelectedIndex = position
            }
        })
    }

    private fun destroy(item: Item?) {
        val itemId = item?.id ?: return
        DialogOk.show(context,
            getString(R.string.quer_mesmo_apagar_esse_registro),
            getString(R.string.atencao),
            getString(R.string.cancelar))
        {
            this.reallyDestroy(itemId)
        }
    }

    private fun reallyDestroy(modelId: Long) {
        Repo.item.destroy(modelId)
        findNavController().popBackStack()
    }

    //
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater?.inflate(R.menu.menu_frag_closet_slider, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item?.itemId) {
            R.id.menu_action_delete -> {
                destroy(vmParent.items.getOrNull(mSelectedIndex))
                return true
            }
            R.id.menu_action_edit -> {
                vmParent.modelEditing.value = vmParent.items[mSelectedIndex]
                findNavController().navigate(R.id.action_to_frag_main_closet_edit)
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}