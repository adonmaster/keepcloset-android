package br.com.adonio.keepcloset.frags.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.extensions.setEnabledEx
import br.com.adonio.keepcloset.extensions.snackError
import br.com.adonio.keepcloset.helpers.Http
import br.com.adonio.keepcloset.helpers.HttpFormData
import br.com.adonio.keepcloset.helpers.Span
import br.com.adonio.keepcloset.repos.Repo
import kotlinx.android.synthetic.main.frag_login_check.*

class FragLoginCheck: Fragment() {

    // computed
    private val cEmail: String?
        get() = arguments?.getString("email")

    // ui
    private lateinit var vm: FragLoginCheckVM

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.frag_login_check, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(this).get(FragLoginCheckVM::class.java)

        vm.loading.observe(this, Observer {
            ui_submit.setEnabledEx(!it)
            ui_cancel.setEnabledEx(!it)
            ui_progress.isVisible = it
        })

        vm.error.observe(this, Observer { reason ->
            reason?.let { ui_submit.snackError(it) }
        })

        ui_cancel.setOnClickListener { findNavController().popBackStack() }

        ui_submit.setOnClickListener { submit() }

        ui_desc_email.text = Span(getString(R.string.foi_enviado_um_codigo_para))
            .concat("\"${cEmail ?: ""}\".").bold()
            .concat("\n\n"+getString(R.string.por_favor_insira_o_embaixo))
            .build()
    }

    private fun submit()
    {
        vm.loading.value = true
        vm.error.value = null

        val sCode = ui_code.text.toString()
        val params = HttpFormData()
            .put("email", cEmail!!)
            .put("code", sCode)

        // google play account
        // uid: google_play_acc3487@keepcloset.com.br
        // code: 111111
        if (cEmail == "google_play_acc3487@keepcloset.com.br" && sCode == "111111") {
            afterSubmit()
            return
        }


        Http.post("register/check", params)
            .then {
                afterSubmit()
            }
            .catch {
                vm.error.value = it
            }
            .finally {
                vm.loading.value = false
            }
    }

    private fun afterSubmit() {
        App.shared.model.user = Repo.user.registerAndActivate(cEmail!!)
        activity?.finish()
    }

}