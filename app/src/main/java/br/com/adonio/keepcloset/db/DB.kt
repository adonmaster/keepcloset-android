package br.com.adonio.keepcloset.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import br.com.adonio.keepcloset.models.*
import br.com.adonio.keepcloset.repos.Repo

object DB {

    @Database(entities = [
        User::class, Item::class, Look::class, LookItem::class, Inspiration::class,
        ScheduleItem::class, Luggage::class, LuggageItem::class
    ], version = 14)

    @TypeConverters(DBConverters::class)
    abstract class AppDatabase: RoomDatabase() {
        abstract val user: UserDao
        abstract val item: ItemDao
        abstract val look: LookDao
        abstract val inspiration: InspirationDao
        abstract val schedule: ScheduleItemDao
        abstract val luggage: LuggageDao
    }

    lateinit var i: AppDatabase

    fun config(context: Context)
    {
        i = Room.databaseBuilder(context, AppDatabase::class.java, "main.room")
            .fallbackToDestructiveMigration()
            .apply { migrate(this) }
            .allowMainThreadQueries()
            .build()
    }

    private fun migrate(db: RoomDatabase.Builder<AppDatabase>) {
        db.apply {
            addMigrations(migrationMaker(0, 1) {
                it.execSQL("")
            })
        }
    }

    private fun migrationMaker(from: Int, to: Int, cb: (SupportSQLiteDatabase)->Unit): Migration {
        return object: Migration(from, to) {
            override fun migrate(database: SupportSQLiteDatabase) {
                cb(database)
            }
        }
    }

}