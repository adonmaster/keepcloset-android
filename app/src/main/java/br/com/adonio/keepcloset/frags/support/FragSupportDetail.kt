package br.com.adonio.keepcloset.frags.support

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.T
import kotlinx.android.synthetic.main.frag_support_detail.*

class FragSupportDetail: Fragment() {

    //
    private val cUrl get() = arguments!!.getString("url")!!

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_support_detail, container, false)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        ui_progress?.isVisible = false

        //
        ui_btn_back.setOnClickListener {
            findNavController().popBackStack()
        }

        //
        ui_web?.settings?.javaScriptEnabled = true
        ui_web?.settings?.loadWithOverviewMode = true
        ui_web?.settings?.useWideViewPort = true
        ui_web?.settings?.domStorageEnabled = true
        ui_web?.webViewClient = object: WebViewClient() {
            override fun onReceivedError(
                view: WebView?,
                request: WebResourceRequest?,
                error: WebResourceError?
            ) {
                T.error(activity, error?.toString() ?: getString(R.string.erro_de_navegacao))
            }
        }
        ui_web?.webChromeClient = object: WebChromeClient() {
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                ui_progress.isVisible = 100 < newProgress
            }
        }
    }

    override fun onStart() {
        super.onStart()

        ui_progress?.isVisible = true
        ui_web?.loadUrl(cUrl)
    }

}