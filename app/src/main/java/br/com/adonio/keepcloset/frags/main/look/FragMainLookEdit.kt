package br.com.adonio.keepcloset.frags.main.look

import android.animation.Animator
import android.graphics.Bitmap
import android.graphics.PointF
import android.os.Bundle
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.CheckBox
import android.widget.SeekBar
import android.widget.Switch
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.DialogOk
import br.com.adonio.keepcloset.dialogs.DialogStrMultiplePicker
import br.com.adonio.keepcloset.dialogs.DialogStrPicker
import br.com.adonio.keepcloset.extensions.isVisible
import br.com.adonio.keepcloset.extensions.setVisible
import br.com.adonio.keepcloset.frags.main.look.adapter.FragMainLookEditItemsAdapter
import br.com.adonio.keepcloset.helpers.*
import br.com.adonio.keepcloset.models.Inspiration
import br.com.adonio.keepcloset.models.Item
import br.com.adonio.keepcloset.models.Look
import br.com.adonio.keepcloset.repos.LookRepo
import br.com.adonio.keepcloset.repos.Repo
import br.com.adonio.keepcloset.structs.SimpleAnimatorListener
import br.com.adonio.keepcloset.structs.SimpleOnSeekBarChangeListener
import br.com.adonio.keepcloset.widgets.AnchorImageView
import kotlinx.android.synthetic.main.frag_main_look_edit.*
import java.io.File
import kotlin.properties.Delegates


class FragMainLookEdit: Fragment(), DialogStrPicker.Listener, DialogStrMultiplePicker.Listener {

    // state
    private lateinit var vm: FragMainLookEditVM
    private lateinit var parentVm: FragMainLookVM
    private var mFilter by Delegates.observable<List<DialogStrMultiplePicker.Item>>(listOf()) { _, _, _ ->
        updateDesktopFilterAndAdapter()
    }
    private lateinit var mItemAdapter: FragMainLookEditItemsAdapter
    private var mAnchorSelection = arrayListOf<AnchorImageView>()

    // computed
    private val cModel: Look
        get() = parentVm.modelEditing
    private val cItems: ArrayList<Pair<LookRepo.SuItem, View>>
        get() = vm.items

    // methods
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.frag_main_look_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders
            .of(this, FragMainLookEditFVM())
            .get(FragMainLookEditVM::class.java)
        parentVm = ViewModelProviders.of(activity!!).get(FragMainLookVM::class.java)

        // loading
        vm.loading.observe(this, Observer {
            ui_progress.setVisible(it)
        })

        // category
        vm.category.observe(this, Observer {
            ui_category.text = Str.colorEmpty(context!!, it, getString(R.string.escolha_dots))
        })
        vm.category.value = cModel.category
        ui_wrapper_category.setOnClickListener {
            DialogStrPicker.show(fragmentManager, getString(R.string.ccategoria), "category")
        }

        //..
        ui_container_collapse_top.visibility = View.GONE
        ui_toggle_top.isChecked = false
        ui_toggle_top.setOnCheckedChangeListener { _, b ->
            ui_container_collapse_top.visibility = if (b) View.VISIBLE else View.GONE
        }

        ui_container_collapse_bottom.visibility = View.GONE
        ui_toggle_bottom.isChecked = false
        ui_toggle_bottom.setOnCheckedChangeListener { _, b ->
            ui_container_collapse_bottom.visibility = if (b) View.VISIBLE else View.GONE
        }

        // radio buttons
        val radioButtonsBgList = listOf(
            Pair(ui_rb_bg_mockup_white, R.drawable.img_bknd_mockup_white),
            Pair(ui_rb_bg_mockup_black, R.drawable.img_bknd_mockup_black),
            Pair(ui_rb_bg_mockup_black_empty, R.drawable.img_bknd_mockup_empty_black),
            Pair(ui_rb_bg_mockup_white_empty, R.drawable.img_bknd_mockup_empty_white)
        )
        val rbListener: (View)->Unit = { curView ->
            // check myself and others
            radioButtonsBgList.forEach { it.first.isChecked = curView === it.first }

            // set current background
            val curImgId = radioButtonsBgList.find { it.first === curView }!!.second
            ui_background.setImageResource(curImgId)

            // save model
            val index = radioButtonsBgList.indexOfFirst { it.first == curView }
            vm.bknd = index.takeIf { it >= 0 } ?: 0
        }
        radioButtonsBgList.forEach { it.first.setOnClickListener(rbListener) }

        // bknd
        vm.bknd = cModel.bknd_index
        radioButtonsBgList.forEachIndexed { index, pair ->
            val isChecked = index == vm.bknd
            pair.first.isChecked = isChecked
            if (isChecked) rbListener(pair.first)
        }

        // ui rv
        mItemAdapter = FragMainLookEditItemsAdapter(object : FragMainLookEditItemsAdapter.Listener {
            override fun onSelect(item: FragMainLookEditItemsAdapter.SuItem) {
                item.whenItem {
                    onSelectItem(it)
                }
                item.whenInspiration {
                    onSelectInspiration(it)
                }
            }
            override fun onEmpty(option: Boolean) {
                ui_empty.setVisible(option)
                ui_rv_items.setVisible( ! option)
            }
        })
        ui_rv_items.adapter = mItemAdapter
        ui_rv_items.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        ui_rv_items.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_slide_right)

        // filter
        ui_wrapper_filter.setOnClickListener {
            DialogStrMultiplePicker.show(fragmentManager, getString(R.string.cfiltro), "filter", mFilter.map { it.uid })
        }
        updateDesktopFilterAndAdapter()

        //
        updateDesktopItems()

        //
        ui_wrapper_manipulate_anchor.isVisible = false
     }

    private fun updateDesktopFilterAndAdapter() {
        // label
        val s = mFilter.map { it.text }.takeIf { it.count() > 0 }?.joinToString()
        ui_filter.text = Str.colorEmpty(context!!, s, getString(R.string.escolha_dots))

        // adapter
        mItemAdapter.filter(mFilter)
    }

    private fun updateDesktopItems() {
        cItems.clear()
        val model = guard(cModel.takeIf { !it.isNew() }) { return }

        for (item in Repo.look.getItemsFor(model.id)) {
            addToCanvas(
                LookRepo.SuItem(item.id, item.source_type, item.source_id, item.path),
                PointF(item.pos_x, item.pos_y),
                item.scale,
                item.rotation
            )
        }
    }

    override fun dialogStrPickerGetData(identifier: String): Promise<List<String>>? {
        return promiseIt(Repo.look.getCategories(App.modelUser))
    }

    override fun dialogStrPickerOnSelected(identifier: String, s: String) {
        vm.category.value = s
    }

    override fun dialogStrMultiplePickerGetData(identifier: String): Promise<List<DialogStrMultiplePicker.Item>>? {
        val list = arrayListOf<DialogStrMultiplePicker.Item>()

        // items
        list.add(DialogStrMultiplePicker.Item(
            "item@*", "Closet / "+getString(R.string.todas_categorias),null
        ))
        for (c in Repo.item.getCategoriesWithNull(App.modelUser)) {
            val categoryTitle = c ?: getString(R.string.sem_categoria)
            val categoryKey = c ?: "null"
            list.add(DialogStrMultiplePicker.Item(
                "item@$categoryKey", "Closet / $categoryTitle",null
            ))
        }

        // inspirations
        list.add(DialogStrMultiplePicker.Item(
            "inspiration@*", getString(R.string.inspira_es) + " / "+getString(R.string.todas_categorias), null
        ))
        for (c in Repo.inspiration.getCategoriesWithNull(App.modelUser)) {
            val categoryTitle = c ?: getString(R.string.sem_categoria)
            val categoryKey = c ?: "null"
            list.add(DialogStrMultiplePicker.Item(
                "inspiration@$categoryKey", getString(R.string.inspira_es)+ " / $categoryTitle", null
            ))
        }

        // return
        return promiseIt(list)
    }

    override fun dialogStrMultiplePickerOnSelected(identifier: String, selected: List<DialogStrMultiplePicker.Item>) {
        mFilter = selected
    }

    override fun onStart() {
        super.onStart()
        DialogStrPicker.attach(this)
        DialogStrMultiplePicker.attach(this)
    }

    override fun onStop() {
        super.onStop()
        DialogStrPicker.detach()
        DialogStrMultiplePicker.detach()
    }

    private fun onSelectItem(item: Item) {
        addToCanvas(LookRepo.SuItem(0, Item::class.java.simpleName, item.id, item.path), null, null, null)
    }

    private fun onSelectInspiration(model: Inspiration) {
        addToCanvas(LookRepo.SuItem(0, Inspiration::class.java.simpleName, model.id, model.path), null, null, null)
    }

    private fun addToCanvas(suItem: LookRepo.SuItem, pos: PointF?, scale: Float?, rotation: Float?)
    {
        val parent = ui_container_canvas
        val v = AnchorImageView(context)
        Img.load(File(suItem.path), v) { vx ->
            pos?.let { vx.x = it.x; vx.y = it.y } // position
        }

        cItems.add(Pair(suItem, v))
        parent.addView(v)

        v.scaleX = scale ?: 0.5f; v.scaleY = v.scaleX // scale
        v.rotation = rotation ?: 0f // rotation
        pos?.let { v.x = it.x; v.y = it.y } // position

        v.anchorOnSelect = { vv, option ->
            // bring to front
            if (option) {
                parent.bringChildToFront(vv)

                // change order
                val bkupItem = cItems.find { vv == it.second }
                bkupItem?.let {
                    cItems.remove(it)
                    cItems.add(it)
                }
            }

            // only 1 selected
            if (option) {
                mAnchorSelection.forEach {
                    if (it != vv) it.anchorSelected = false
                }
                if (! mAnchorSelection.contains(vv)) {
                    mAnchorSelection.add(vv)
                    setupAnchorSelected(vv)
                }
            } else {
                mAnchorSelection.remove(vv)

                if (mAnchorSelection.count() == 0) {
                     setupAnchorSelected(null)
                }
            }
        }
        v.anchorOnLongClick = { vv ->
            val pair = cItems.find { vv == it.second }
            cItems.remove(pair)

            // remove view
            pair?.second?.let { vlong ->
                vlong.animate()
                    .setDuration(200)
                    .scaleX(0f)
                    .scaleY(0f)
                    .setListener(object: SimpleAnimatorListener() {
                        override fun onAnimationEnd(animation: Animator?) {
                            parent.removeView(vlong)
                        }
                    })
                    .start()
            }
        }
    }

    private fun setupAnchorSelected(a: AnchorImageView?)
    {
        ui_wrapper_manipulate_anchor.isVisible = a != null
        if (a != null) {

            // scale
            ui_seek_scale.max = 250
            ui_seek_scale.progress = (a.scaleX * 100).toInt()
            ui_seek_scale.setOnSeekBarChangeListener(object: SimpleOnSeekBarChangeListener() {
                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    val scale = progress.toFloat() / 100
                    a.scaleX = scale
                    a.scaleY = a.scaleX
                }
            })

            // rotation
            ui_seek_rotation.max = 359
            ui_seek_rotation.progress = a.rotation.toInt()
            ui_seek_rotation.setOnSeekBarChangeListener(object: SimpleOnSeekBarChangeListener() {
                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    a.rotation = progress.toFloat()
                }
            })
        } else {
            ui_seek_rotation.setOnSeekBarChangeListener(null)
            ui_seek_scale.setOnSeekBarChangeListener(null)
        }
    }

    private fun submit() {
        if (vm.loading.value == true) return
        vm.loading.value = true

        Task.main(100) {
            Repo.look.save(cModel, screenshot(), cItems, vm.category.value, vm.bknd)
                .then {
                    findNavController().popBackStack()
                }
                .catch {
                    DialogOk.show(context, it, getString(R.string.atencao))
                }
                .finally {
                    vm.loading.value = false
                }
        }
    }

    private fun screenshot(): Bitmap {
        // deselect all
        cItems.forEach { (it.second as? AnchorImageView)?.anchorSelected = false }

        // take a pic
        return Img.bitmapFromView(ui_container_canvas)
    }

    private fun share() {
        if (vm.loading.value == true) return
        vm.loading.value = true

        Task.main(10) {
            val f = F.saveExternalSynced("", "Keep_Closet_Look_1.png", screenshot())
            activity?.let { act ->
                Act.shareImage(act, f)
                vm.loading.value = false
            }
        }
    }

    private fun tryDestroy() {
        DialogOk.show(
            context,
            getString(R.string.quer_mesmo_apagar_esse_registro),
            getString(R.string.atencao),
            getString(R.string.cancelar)
        ) {
            this.destroy()
        }
    }

    private fun destroy() {
        Repo.look.destroy(cModel.id)

        findNavController().popBackStack()
    }

    // menu

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(if (cModel.isNew()) R.menu.menu_frag_new else R.menu.menu_frag_look_edit, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        item?.itemId?.let { id ->
            when (id) {
                R.id.menu_action_done -> {
                    submit()
                }
                R.id.menu_action_share -> {
                    share()
                }
                R.id.menu_action_delete -> {
                    tryDestroy()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
