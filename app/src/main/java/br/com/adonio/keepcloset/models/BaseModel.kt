package br.com.adonio.keepcloset.models

import androidx.room.Ignore
import androidx.room.PrimaryKey
import br.com.adonio.keepcloset.helpers.LightCache

abstract class BaseModel {

    @Ignore
    protected val cache = LightCache()

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @Ignore
    fun isNew() = id == 0L

}