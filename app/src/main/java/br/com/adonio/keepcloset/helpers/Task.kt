package br.com.adonio.keepcloset.helpers

import android.os.Handler
import android.os.Looper

object Task {

    private val debounces by lazy<MutableList<String>> { mutableListOf() }
    private val indexes by lazy<ArrayList<String>> { arrayListOf() }

    fun main(delay: Long = 0, closure: ()->Unit): Boolean {
        return Handler(Looper.getMainLooper()).postDelayed(closure, delay)
    }

    fun mainIndexed(delay: Long = 0, closure: ()->Unit): String {
        val uid = "Task.MainIndexed." + Str.random(25)
        indexes.add(uid)

        main(delay) {
            if (indexes.contains(uid)) {
                indexes.remove(uid)
                closure()
            }
        }
        return uid
    }

    fun removeMainIndex(uid: String) {
        indexes.remove(uid)
    }

    fun debounce(key: String, interval: Long, cb: ()->Unit) {
        debounces.add(key)
        main(interval) {
            debounces.remove(key)
            if (debounces.indexOf(key) == -1) cb()
        }
    }

}