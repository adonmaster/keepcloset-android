package br.com.adonio.keepcloset.dialogs.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Act
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.models.Inspiration
import br.com.adonio.keepcloset.models.Look
import kotlinx.android.synthetic.main.rv_dialog_pic_path.view.*
import java.io.File

class DialogPicPathAdapter(private val listener: Listener): RecyclerView.Adapter<DialogPicPathAdapter.VH>() {

    data class Item(
        val thumb: String, val full: String,
        val item_id: Long, val look_id: Long, val inspiration_id: Long
    )
    private var mData = listOf<Item>()

    override fun getItemCount(): Int {
        return mData.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(R.layout.rv_dialog_pic_path, parent, false)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = mData[position]
        Img.load(File(m.thumb), holder.uiImg)
    }

    fun updateFromItems(list: List<br.com.adonio.keepcloset.models.Item>) {
        mData = list.map { i -> Item(i.path_thumb, i.path, i.id, 0, 0) }
        notifyDataSetChanged()
    }

    fun updateFromLooks(list: List<Look>) {
        mData = list.map { i -> Item(i.path_thumb!!, i.path!!, 0, i.id, 0) }
        notifyDataSetChanged()
    }

    fun updateFromInspirations(list: List<Inspiration>) {
        mData = list.map { i -> Item(i.path_thumb, i.path, 0, 0, i.id) }
        notifyDataSetChanged()
    }

    // vh
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiImg: ImageView = itemView.ui_img
        init {
            itemView.setOnClickListener {
                listener.onSelect(mData[adapterPosition])
            }
        }
    }

    // listener
    interface Listener {
        fun onSelect(item: Item)
    }
}