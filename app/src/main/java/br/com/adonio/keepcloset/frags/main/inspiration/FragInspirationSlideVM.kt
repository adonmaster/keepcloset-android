package br.com.adonio.keepcloset.frags.main.inspiration

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class FragInspirationSlideVM: ViewModel() {

    val selectedIndex = MutableLiveData<Int>()

}