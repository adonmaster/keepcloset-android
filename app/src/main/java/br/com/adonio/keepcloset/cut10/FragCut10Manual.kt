package br.com.adonio.keepcloset.cut10

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.adonio.keepcloset.Cut10VM
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.F
import kotlinx.android.synthetic.main.frag_cut10_manual.*

class FragCut10Manual: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_cut10_manual, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val vm = ViewModelProviders.of(requireActivity()).get(Cut10VM::class.java)

        vm.loading.observe(viewLifecycleOwner, Observer {
            ui_loading?.isVisible = it
        })

        ui_btn_back.setOnClickListener {
            findNavController().popBackStack()
        }

        //
        val buttons = arrayOf(
            ui_btn_brush to PostDrawView.Mode.Wand,
            ui_btn_eraser to PostDrawView.Mode.Erase,
            ui_btn_zoom to PostDrawView.Mode.Zoom
        )
        buttons.forEach {
            it.first.alpha = 0.2f
            it.first.setOnClickListener {view ->
                if (view.alpha != 1f) {
                    buttons.forEach { pair ->
                        val isb = view == pair.first
                        pair.first.alpha = if (isb) 1f else 0.2f
                        if (isb) ui_draw.setMode(pair.second)
                    }
                }
            }
        }
        ui_btn_eraser.alpha = 1f
        ui_draw.setMode(PostDrawView.Mode.Erase)

        ui_btn_next.setOnClickListener {
            vm.loading.value = true
            ui_draw.requestMaskedBmp { bmp ->
                vm.bmpResult = bmp
                vm.loading.value = false

                findNavController().navigate(R.id.to_third);
            }
        }

        //
        ui_draw.post {
            ui_draw.setImage(vm.bmp, vm.bmpMasked)
        }
    }

}