package br.com.adonio.keepcloset.helpers

import org.json.JSONArray
import org.json.JSONObject

@Suppress("unused")
/**
 * @ver 2.0
 */
class JParser {

    private var json: JSONObject?
    private val cb: JParserDsl.() -> Unit

    constructor(json: JSONObject?, cb: JParserDsl.()->Unit) {
        this.json = json
        this.cb = cb
    }

    constructor(jarray: JSONArray?, fieldArray: String, cb: JParserDsl.()->Unit) {
        val j = JSONObject()
        j.put(fieldArray, jarray)

        this.json = j
        this.cb = cb
    }


    fun <T> parse(cb: JParserResult.()->T): T {
        val dsl = JParserDsl(this.json, this.cb)
        return cb(JParserResult(dsl.getCache(), dsl.getFieldsStored()))
    }

    fun <T> parseo(cb: JParserResult.()->T?): T? {
        val dsl = JParserDsl(this.json, this.cb)
        return cb(JParserResult(dsl.getCache(), dsl.getFieldsStored()))
    }

}

class JParserDsl(val json: JSONObject?, cb: JParserDsl.() -> Unit) {

    private var cache = HashMap<String, Any?>()
    private val fieldsStored = arrayListOf<String>()

    init {
        cb(this)
    }

    fun getCache() = cache
    fun getFieldsStored(): List<String> = fieldsStored

    /** getters */
    private fun <T> jsonGet(field: String): T {
        val j = guard(json) { abort("Json nullo") }

        if ( ! j.has(field)) { abort("campo '$field' existente, mas nulo") }

        @Suppress("UNCHECKED_CAST")
        return guard(j.get(field) as? T) { abort("campo '$field' existente, mas conteudo do tipo errado")}
    }

    private fun <T> jsonGeto(field: String): T? {
        if (json==null) return null

        if (json.has(field)) {
            if (json.isNull(field)) return null

            @Suppress("UNCHECKED_CAST")
            return json.get(field) as? T
        }

        return null
    }
    /** getters */

    fun store(field: String, newFieldName: String?=null): JParserDsl {
        fieldsStored.add(field)
        cache[newFieldName ?: field] = jsonGeto<Any>(field)
        return this
    }

    fun storeSeek(field: String, path: String): JParserDsl {
        fieldsStored.add(field)
        cache[field] = seekData(path.split("."), json)
        return this
    }

    fun arrSeek(field: String, path: String, cb: JParserDsl.()->Unit): JParserDsl {
        val list = seekData(path.split("."), json) as JSONArray
        val instances = ArrayList<JParserDsl>()
        for (index in 0 until list.length()) {
            val curJson = guard(list.get(index) as? JSONObject) {
                abort("Da lista '$field', o item [$index] não é um objeto válido.")
            }
            instances.add(JParserDsl(curJson, cb))
        }
        cache[field] = instances
        return this
    }

    private fun seekData(paths: List<String>, data: Any?): Any? {
        if (data==null) return null
        if (paths.count() <= 0) return data

        val newPath = paths.toMutableList()
        val path = newPath.removeAt(0)

        // array?
        val index = path.toIntOrNull()?.takeIf { it >= 0 }
        if (index != null) {
            val jsonArray = guard(data as? JSONArray) { return null }
            if (index >= jsonArray.length()) return null

            val v = jsonArray.get(index)
            return seekData(newPath, v)
        }

        // has to be an object
        val jsonObject = guard(data as? JSONObject) { return null }
        if (jsonObject.isNull(path)) return null
        val v = jsonObject.get(path)
        return seekData(newPath, v)
    }

    fun obj(field: String, cb: JParserDsl.()->Unit): JParserDsl {
        val j: JSONObject = jsonGet(field)
        cache[field] = JParserDsl(j, cb)
        return this
    }

    fun objo(field: String, cb: JParserDsl.()->Unit): JParserDsl {
        val j = guard(jsonGeto<JSONObject>(field)) { return this }
        cache[field] = JParserDsl(j, cb)
        return this
    }

    fun storeArr(field: String) {
        val list = jsonGet<JSONArray>(field)
        val r = ArrayList<Any>()

        @Suppress("UNCHECKED_CAST")
        for (index in 0 until list.length()) {
            r.add(list.get(index))
        }

        cache[field] = r
    }

    fun storeArro(field: String) {
        val list = guard(jsonGeto<JSONArray>(field)) { return }
        val r = ArrayList<Any>()

        @Suppress("UNCHECKED_CAST")
        for (index in 0 until list.length()) {
            r.add(list.get(index))
        }

        cache[field] = r
    }

    fun arr(field: String, cb: JParserDsl.()->Unit): JParserDsl {
        val list: JSONArray = jsonGet(field)
        val instances = ArrayList<JParserDsl>()
        for (index in 0 until list.length()) {
            val curJson = guard(list.get(index) as? JSONObject) {
                abort("Da lista '$field', o item [$index] não é um objeto válido.")
            }
            instances.add(JParserDsl(curJson, cb))
        }
        cache[field] = instances
        return this
    }

    fun arro(field: String, cb: JParserDsl.()->Unit): JParserDsl {
        val list = guard(jsonGeto<JSONArray>(field)) { return this }
        val instances = ArrayList<JParserDsl>()
        for (index in 0 until list.length()) {
            val curJson = guard(list.get(index) as? JSONObject) {
                abort("Da lista '$field', o item [$index] não é um objeto válido.")
            }
            instances.add(JParserDsl(curJson, cb))
        }
        cache[field] = instances
        return this
    }

}

class JParserResult(private val cache: HashMap<String,Any?>, private val fieldsStored: List<String>) {

    @Suppress("UNCHECKED_CAST")
    fun <T> get(field: String): T {
        return guard(cache[field] as? T) {
            if (cache[field]==null) abort("Campo $field não encontrado.")
            abort("Campo $field com tipo incorreto.")
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> geto(field: String): T? {
        return cache[field] as? T
    }

    private var fieldIndex = 0
    fun resetFieldNext() { fieldIndex = 0 }

    fun <T> getNext(): T {
        val field = fieldsStored[fieldIndex++]
        return get(field)
    }

    fun <T> getNexto(): T? {
        val field = fieldsStored[fieldIndex++]
        return geto(field)
    }

    fun getStrOrJson(field: String): String? {
        val v = cache[field] ?: return null

        if (v is String) return v
        if (cache[field] is JSONObject) return v.toString()
        throw RuntimeException("campo $field não é string ou json")
    }

    fun getBoolean(field: String): Boolean {
        val v = cache[field]
        if (v is Int) return v > 0
        if (v is Boolean) return v
        return false
    }

    fun <T> obj(field: String, cb: JParserResult.()->T): T {
        val instance: JParserDsl = get(field)
        return cb(JParserResult(instance.getCache(), instance.getFieldsStored()))
    }

    fun <T> objo(field: String, cb: JParserResult.()->T): T? {
        val instance = geto<JParserDsl>(field)
        return instance?.getCache()?.let { cb(JParserResult(it, instance.getFieldsStored())) }
    }

    fun <T> getArr(field: String): List<T> {
        return get(field)
    }

    fun <T> getArro(field: String): List<T>? {
        return geto(field)
    }

    fun <T> arr(field: String, cb: JParserResult.()->T): List<T> {
        val instances: List<JParserDsl> = get(field)
        val r = ArrayList<T>()
        instances.forEach {
            r.add(
                cb(JParserResult(it.getCache(), it.getFieldsStored()))
            )
        }
        return r
    }

    fun <T> arro(field: String, cb: JParserResult.()->T): List<T>? {
        val instances = guard(geto<List<JParserDsl>>(field)) { return null }
        val r = ArrayList<T>()
        instances.forEach {
            r.add(
                cb(JParserResult(it.getCache(), it.getFieldsStored()))
            )
        }
        return r
    }

}

private fun abort(msg: String) {
    throw RuntimeException(msg)
}