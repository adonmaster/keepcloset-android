package br.com.adonio.keepcloset.models

import androidx.room.Entity
import androidx.room.Ignore
import br.com.adonio.keepcloset.models.presenters.UserPresenter

@Entity(tableName = "users")
class User: BaseModel() {

    var active: Boolean = false
    lateinit var email: String
    var name: String? = null
    var avatar_thumb: String? = null
    var avatar: String? = null


    // utils

    @Ignore
    fun p() = cache.getGranted("p", { UserPresenter(this) })

}