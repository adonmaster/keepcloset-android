package br.com.adonio.keepcloset.frags.main.look.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Act
import br.com.adonio.keepcloset.helpers.CategorizedContainer
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.helpers.Res
import br.com.adonio.keepcloset.models.Look
import kotlinx.android.synthetic.main.rv_main_look.view.*
import kotlinx.android.synthetic.main.rv_main_look_header.view.*
import java.io.File

class FragMainLookAdapter(private val listener: Listener): RecyclerView.Adapter<FragMainLookAdapter.VH>() {

    //
    private val mData = CategorizedContainer<Look>(null) {
        it.category ?: Res.str(R.string.sem_categoria)
    }

    //
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val resId = if (mData.isTypeCategory(viewType)) R.layout.rv_main_look_header
            else R.layout.rv_main_look

        val view = Act.inflate(resId, parent,false)!!

        return VH(view)
    }

    override fun getItemViewType(position: Int): Int {
        return mData[position]?.getType() ?: -1
    }

    override fun getItemCount(): Int {
        return mData.count().also {
            listener.onEmpty(it <= 0)
        }
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        mData[position]
            ?.whenData {
                Img.load(File(it.path_thumb), holder.uiImg)
            }
            ?.whenCategory {
                holder.uiLbl?.text = it
            }
    }

    fun getModel(position: Int): CategorizedContainer.Item<Look>? {
        return mData[position]
    }

    fun updateData(list: List<Look>) {
        mData.update(list)
        notifyDataSetChanged()
    }

    // vh
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiImg: ImageView? = itemView.ui_img
        val uiLbl: TextView? = itemView.ui_lbl

        init {
            itemView.setOnClickListener {
                mData[adapterPosition]?.whenData {
                    listener.onSelect(it)
                }
            }
        }
    }

    // listener
    interface Listener {
        fun onSelect(item: Look)
        fun onEmpty(option: Boolean)
    }

}