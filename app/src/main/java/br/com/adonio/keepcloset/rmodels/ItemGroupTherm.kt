package br.com.adonio.keepcloset.rmodels

import androidx.annotation.ColorRes

data class ItemGroupTherm(val desc: String, val count: Int, @ColorRes val color: Int) {}