package br.com.adonio.keepcloset.models

import androidx.room.Entity
import androidx.room.Ignore
import br.com.adonio.keepcloset.models.presenters.LookPresenter

@Entity(tableName = "looks")
class Look: BaseModel() {

    var user_id: Long = 0
    var category: String? = null
    var bknd_index: Int = 0
    var path: String? = null
    var path_thumb: String? = null

    @Ignore
    fun p() = cache.getGranted("presenter", { LookPresenter(this) })

}