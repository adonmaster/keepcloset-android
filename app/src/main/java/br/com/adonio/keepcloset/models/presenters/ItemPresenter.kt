package br.com.adonio.keepcloset.models.presenters

import android.content.Context
import android.text.Spannable
import android.widget.ImageView
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.helpers.Span
import br.com.adonio.keepcloset.models.Item
import br.com.adonio.keepcloset.structs.Therm
import java.io.File

class ItemPresenter(model: Item) : BasePresenter<Item>(model) {

    fun loadThumb(imageView: ImageView?, refresh: Boolean=false) {
        Img.load(File(model.path_thumb), imageView, refresh)
    }

    fun loadFull(imageView: ImageView?) {
        Img.load(File(model.path), imageView)
    }

    fun fullTherm(context: Context, ifEmpty: String): Spannable {
        val therm = Therm.f(this.model.therm_id) ?: return Span(ifEmpty)
            .color(context, R.color.md_grey_500)
            .build()

        return Span(therm.faIcon() + "  ")
            .font(context, R.font.fontawesome)
            .color(context, therm.colorLightTheme())
            .concat(therm.toString())
            .color(context, therm.colorLightTheme())
            .build()
    }

    fun fullBrand(context: Context, ifEmpty: String): Spannable {
        val brand = this.model.brand ?: return Span(ifEmpty)
            .color(context, R.color.md_grey_500)
            .build()

        return Span("\uF02B ")
            .font(context, R.font.fontawesome)
            .concat(brand)
            .build()
    }

    fun fullStatus(context: Context, ifEmpty: String): Spannable {
        val status = this.model.status ?: return Span(ifEmpty)
            .color(context, R.color.md_grey_500)
            .build()

        return Span("\uF05A ")
            .font(context, R.font.fontawesome)
            .concat(status)
            .build()
    }

    fun fullColors(context: Context, isEmpty: String): CharSequence {
        val colors = listOf(
                Pair(model.color1, model.color1_desc),
                Pair(model.color2, model.color2_desc)
            )
            .mapNotNull { if (it.second == null) null else Pair(it.first.toInt(), it.second!!) }

        if (colors.count() == 0) return Span(isEmpty).color(context, R.color.md_grey_500).build()

        val span = Span("")
        for (color in colors) {
            span.concat("\uF111")
                .color(color.first)
                .font(context, R.font.fontawesome)
        }
        span.concat(" "+ colors.joinToString(", ") { it.second.toLowerCase().capitalize() })

        return span.build()
    }

}