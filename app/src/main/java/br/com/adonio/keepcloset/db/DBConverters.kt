package br.com.adonio.keepcloset.db

import androidx.room.TypeConverter
import br.com.adonio.keepcloset.helpers.Dt
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset

class DBConverters {

    @TypeConverter
    fun fromTimestamp(value: Long?): LocalDateTime? {
        return if (value==null) null
            else LocalDateTime.ofEpochSecond(value, 0, ZoneOffset.UTC)
    }

    @TypeConverter
    fun dateToTimestamp(date: LocalDateTime?): Long? {
        return date?.toEpochSecond(ZoneOffset.UTC)
    }

    @TypeConverter
    fun fromDate(value: String?): LocalDate? {
        return Dt.fromSql(value)
    }

    @TypeConverter
    fun toDate(date: LocalDate?): String? {
        return Dt.toSql(date)
    }

}