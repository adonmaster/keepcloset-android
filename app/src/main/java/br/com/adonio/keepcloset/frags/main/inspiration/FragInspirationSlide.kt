package br.com.adonio.keepcloset.frags.main.inspiration

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.DialogOk
import br.com.adonio.keepcloset.helpers.Task
import br.com.adonio.keepcloset.repos.Repo
import kotlinx.android.synthetic.main.frag_inspiration_slide.*

class FragInspirationSlide: Fragment() {

    private lateinit var vm: FragMainInspirationVM
    private lateinit var vmSlide: FragInspirationSlideVM

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.frag_inspiration_slide, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(activity!!).get(FragMainInspirationVM::class.java)
        vmSlide = ViewModelProviders.of(activity!!).get(FragInspirationSlideVM::class.java)

        ui_pager.adapter = object: FragmentStatePagerAdapter(childFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
            override fun getItem(position: Int): Fragment {
                return FragInspirationSlideItem.f(position)
            }
            override fun getCount(): Int {
                return vm.selectedRawList.count()
            }
        }
        ui_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                ui_description.text = "${position+1}/${vm.selectedRawList.count()}"
                vmSlide.selectedIndex.value = position
            }
        })
        if (vm.selectedRawList.count() > 0) {
            ui_pager.currentItem = vm.selectedRawPosition
            vmSlide.selectedIndex.value = ui_pager.currentItem

            ui_description.text = "${ui_pager.currentItem+1}/${vm.selectedRawList.count()}"
        }

    }

    private fun tryDelete() {
        DialogOk.show(context,
            getString(R.string.quer_mesmo_apagar_esse_registro),
            getString(R.string.atencao),
            getString(R.string.cancelar)
        ) {
            delete()
        }
    }

    private fun delete() {
        vm.selectedRawList.getOrNull(ui_pager.currentItem)
            ?.let { m ->
                Repo.inspiration.destroy(m)
                findNavController().popBackStack()
            }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_frag_inspiration_slide, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId) {
            R.id.menu_action_delete -> tryDelete()
        }
        return super.onOptionsItemSelected(item)
    }

}