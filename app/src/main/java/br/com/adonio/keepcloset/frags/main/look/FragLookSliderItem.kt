package br.com.adonio.keepcloset.frags.main.look

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import br.com.adonio.keepcloset.R
import kotlinx.android.synthetic.main.frag_look_slider_item.*

class FragLookSliderItem: Fragment() {

    //
    companion object {
        fun f(position: Int): FragLookSliderItem {
            val bundle = Bundle()
            bundle.putInt("position", position)
            val frag = FragLookSliderItem()
            frag.arguments = bundle
            return frag
        }
    }

    //
    private lateinit var vmParent: FragMainLookVM

    //
    private val cPosition
        get() = arguments!!.getInt("position")

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_look_slider_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        vmParent = ViewModelProviders.of(activity!!).get(FragMainLookVM::class.java)

        //
        val m = vmParent.items[cPosition]

        //
        m.p().loadFull(ui_img)
    }

}