package br.com.adonio.keepcloset.cut10

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.adonio.keepcloset.Cut10VM
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.F
import br.com.adonio.keepcloset.helpers.Task
import kotlinx.android.synthetic.main.frag_cut10_preview.*

class FragCut10Preview: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_cut10_preview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val vm = ViewModelProviders.of(requireActivity()).get(Cut10VM::class.java)

        ui_btn_back.setOnClickListener {
            findNavController().popBackStack()
        }

        ui_img.setImageBitmap(vm.bmpResult)

        ui_btn_done.setOnClickListener {
            Task.main {

                val f = F.saveInternalSynced("cut10", "img_masked.png", vm.bmpResult)
                val data = Intent().putExtra("path", f.path)

                activity?.setResult(Activity.RESULT_OK, data)
                activity?.finish()

            }
        }

    }

}