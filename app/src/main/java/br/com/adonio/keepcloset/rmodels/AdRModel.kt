package br.com.adonio.keepcloset.rmodels

import android.os.Parcel
import android.os.Parcelable
import br.com.adonio.keepcloset.helpers.JParser
import org.json.JSONArray

class AdRModel(
    var id: Int,
    var body: Body,
    var attachments: List<Attachment>
): Parcelable {


    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readParcelable(Body::class.java.classLoader)!!,
        parcel.createTypedArrayList(Attachment)!!
    ) {
    }

    class Body(
        var title: String?,
        var desc: String?,
        var link: String
    ): Parcelable {
        constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString()!!
        ) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(title)
            parcel.writeString(desc)
            parcel.writeString(link)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Body> {
            override fun createFromParcel(parcel: Parcel): Body {
                return Body(parcel)
            }

            override fun newArray(size: Int): Array<Body?> {
                return arrayOfNulls(size)
            }
        }
    }

    class Attachment(
        var id: Int,
        var type: String,
        var url: String
    ): Parcelable {
        constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString()!!,
            parcel.readString()!!
        ) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeInt(id)
            parcel.writeString(type)
            parcel.writeString(url)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<Attachment> {
            override fun createFromParcel(parcel: Parcel): Attachment {
                return Attachment(parcel)
            }

            override fun newArray(size: Int): Array<Attachment?> {
                return arrayOfNulls(size)
            }
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeParcelable(body, flags)
        parcel.writeTypedList(attachments)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AdRModel> {

        fun parse(items: JSONArray) = JParser(items, "items") {
            arr("items") {
                store("id")
                obj("body") {
                    store("title")
                    store("desc")
                    store("link")
                }
                arr("attachments") {
                    store("id")
                    store("type")
                    store("url")
                }
            }
        }.parse {
            arr("items") {
                AdRModel(
                    getNext(),
                    obj("body") {
                        Body(getNexto(), getNexto(), getNext())
                    },
                    arr("attachments") {
                        Attachment(getNext(), getNext(), getNext())
                    }
                )
            }
        }

        override fun createFromParcel(parcel: Parcel): AdRModel {
            return AdRModel(parcel)
        }

        override fun newArray(size: Int): Array<AdRModel?> {
            return arrayOfNulls(size)
        }
    }

}
