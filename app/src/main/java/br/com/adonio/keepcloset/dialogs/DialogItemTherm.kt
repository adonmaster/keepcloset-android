package br.com.adonio.keepcloset.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.adapters.DialogItemThermAdapter
import br.com.adonio.keepcloset.structs.Therm
import kotlinx.android.synthetic.main.dialog_item_therm.*

class DialogItemTherm: DialogBase() {

    // static
    companion object {
        fun show(fragmentManager: FragmentManager?) {
            if (fragmentManager==null) return
            DialogItemTherm().show(fragmentManager, "DialogItemTherm")
        }

        private var listener: Listener? = null

        fun attach(listener: Listener) {
            this.listener = listener
        }
        fun detach() {
            this.listener = null
        }
    }

    // methods
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.setTitle(getString(R.string.ctemperatura))
        return inflater.inflate(R.layout.dialog_item_therm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ui_rv.adapter = DialogItemThermAdapter(object : DialogItemThermAdapter.Listener {
            override fun onSelect(therm: Therm) {
                listener?.dialogItemThermOnSelect(therm)
                dismissAllowingStateLoss()
            }
        })
        ui_rv.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        ui_rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_slide_right)

        ui_btn_cancel.setOnClickListener {
            dismissAllowingStateLoss()
        }
    }


    // listener
    interface Listener {
        fun dialogItemThermOnSelect(therm: Therm)
    }

}