package br.com.adonio.keepcloset.frags.main.look.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.DialogStrMultiplePicker
import br.com.adonio.keepcloset.helpers.Act
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.helpers.ListWatcher
import br.com.adonio.keepcloset.models.Inspiration
import br.com.adonio.keepcloset.models.Item
import br.com.adonio.keepcloset.repos.Repo
import kotlinx.android.synthetic.main.rv_main_look_edit_items.view.*
import java.io.File

class FragMainLookEditItemsAdapter(private val listener: Listener): RecyclerView.Adapter<FragMainLookEditItemsAdapter.VH>() {

    // class
    data class SuItem(val path: String, val model: Any) {
        fun whenItem(cb: (Item)->Unit) {
            (model as? Item)?.let(cb)
        }
        fun whenInspiration(cb: (Inspiration)->Unit) {
            (model as? Inspiration)?.let(cb)
        }
    }

    // state
    private val mData by lazy {
        ListWatcher<SuItem>()
            .observe { _, _, _ ->
                notifyDataSetChanged()
            }
    }

    // methods
    override fun getItemCount(): Int {
        return mData.count()
            .also { listener.onEmpty(it <= 0) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(R.layout.rv_main_look_edit_items, parent, false)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = mData[position]
        Img.load(File(m.path), holder.uiImg)
    }

    fun filter(filter: List<DialogStrMultiplePicker.Item>) {
        val items = filterItem(filter)
        val inspirations = filterInspirations(filter)

        mData.put(items + inspirations)
    }

    private fun filterItem(filter: List<DialogStrMultiplePicker.Item>): List<SuItem> {
        val list = filter.map { it.uid }
            .map { it.split("@") }
            .filter { it.size == 2 && it[0] == "item" }
            .map { it[1] }

        // all or count == 0
        if (list.contains("*") || filter.count() == 0) {
            return Repo.item.getAllSynced(App.modelUser)
                .map { SuItem(it.path, it) }
        }

        return Repo.item.getForCategories(App.modelUser?.id, list)
            .map { SuItem(it.path, it) }
    }

    private fun filterInspirations(filter: List<DialogStrMultiplePicker.Item>): List<SuItem> {
        val userId = App.modelUser?.id ?: return listOf()

        //
        val list = filter.map { it.uid }
            .map { it.split("@") }
            .filter { it.size == 2 && it[0] == "inspiration" }
            .map { it[1] }

        // all or count == 0
        if (list.contains("*") || filter.count() == 0) {
            return Repo.inspiration.getAll(userId)
                .map { SuItem(it.path, it) }
        }

        return Repo.inspiration.getForCategories(userId, list)
            .map { SuItem(it.path, it) }
    }


    // view holder
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiImg: ImageView? = itemView.ui_img
        init {
            itemView.setOnClickListener {
                listener.onSelect(mData[adapterPosition])
            }
        }
    }

    // listener
    interface Listener {
        fun onSelect(item: SuItem)
        fun onEmpty(option: Boolean)
    }

}