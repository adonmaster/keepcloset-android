package br.com.adonio.keepcloset.frags.main.luggage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.DialogAd
import br.com.adonio.keepcloset.dialogs.DialogOk
import br.com.adonio.keepcloset.extensions.setVisible
import br.com.adonio.keepcloset.frags.main.luggage.adapters.FragMainLuggageAdapter
import br.com.adonio.keepcloset.models.Luggage
import br.com.adonio.keepcloset.services.AdService
import kotlinx.android.synthetic.main.frag_main_luggage.*

class FragMainLuggage: Fragment(), DialogAd.Listener {

    private lateinit var mAdapter: FragMainLuggageAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_main_luggage, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAdapter = FragMainLuggageAdapter(object: FragMainLuggageAdapter.Listener {
            override fun onEmpty(option: Boolean) {
                ui_empty.setVisible(option)
            }
            override fun onSelect(model: Luggage, position: Int) {
                edit(model)
            }
            override fun onLongClick(model: Luggage, position: Int) {
                tryDelete(model, position)
            }
        })
        ui_rv.adapter = mAdapter
        ui_rv.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        ui_rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)

        ui_btn_add.setOnClickListener {
            if (AdService.shouldShowAd()) {
                AdService.showAdDialog(childFragmentManager)
            } else {
                edit(null)
            }
        }
    }

    private fun tryDelete(model: Luggage, position: Int) {
        DialogOk.show(
            context,
            getString(R.string.quer_mesmo_apagar_esse_registro),
            getString(R.string.bagagem),
            getString(R.string.cancelar)
        ) {
            delete(model, position)
        }
    }

    private fun delete(model: Luggage, position: Int) {
        mAdapter.removeModel(model, position)
    }

    private fun edit(model: Luggage?) {
        FragLuggageEdit.navigate(this, R.id.sg_to_luggage_edit, model?.id ?: 0)
    }

    //

    override fun onStart() {
        super.onStart()

        DialogAd.attach(this)
    }

    override fun onStop() {
        super.onStop()

        DialogAd.detach(this)
    }

    //
    override fun dialogAdOnCancel() {
        edit(null)
    }
}