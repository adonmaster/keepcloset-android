@file:Suppress("unused")

package br.com.adonio.keepcloset.cut10

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import br.com.adonio.keepcloset.helpers.Bmp
import br.com.adonio.keepcloset.helpers.Num
import br.com.adonio.keepcloset.helpers.Task
import br.com.adonio.keepcloset.helpers.ter
import kotlin.math.abs
import kotlin.properties.Delegates

@Suppress("PrivatePropertyName")
class PostDrawView(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    private val TOUCH_TOLERANCE = 4f
    private var mBmp = Bitmap.createBitmap(2, 2, Bitmap.Config.ARGB_8888)

    private var mMode: Mode

    private var mPaint = Paint()
    private var mPath = Path()

    private var mPaintErase = Paint()
    private var mPathErase = Path()

    private val mPaintSemiTransparent: Paint

    private var mCanvasDraw = Canvas()
    private var mBmpDraw = Bitmap.createBitmap(2, 2, Bitmap.Config.ARGB_8888)

    private var mUndo by Delegates.observable<Bitmap?>(null) { _, _, v ->
        listener?.drawViewOnRedoCount(if (v==null) 0 else 1)
    }

    private val cPath: Path
        get() = when (mMode) {
            Mode.Wand -> mPath
            Mode.Erase -> mPathErase
            Mode.Zoom -> mPath
        }

    private val cPaint: Paint
        get() = when (mMode) {
            Mode.Wand -> mPaint
            Mode.Erase -> mPaintErase
            Mode.Zoom -> mPaint
        }

    private var mX = 0f
    private var mY = 0f

    private var mTransform: Transformation? = null
    private var mIsZoomed = false

    var listener: Listener? = null

    //
    init {
        isClickable = true
        setLayerType(LAYER_TYPE_SOFTWARE, null)

        mMode = Mode.Wand

        mPaint = Paint(mPaint)
        mPaint.isDither = true
        mPaint.color = Color.RED
        mPaint.style = Paint.Style.STROKE
        mPaint.strokeJoin = Paint.Join.ROUND
        mPaint.strokeCap = Paint.Cap.ROUND
        mPaint.strokeWidth = 30f

        mPaintErase = Paint(mPaintErase)
        mPaintErase.style = Paint.Style.STROKE
        mPaintErase.strokeJoin = Paint.Join.ROUND
        mPaintErase.strokeCap = Paint.Cap.ROUND
        mPaintErase.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
        mPaintErase.strokeWidth = 40f

        mPaintSemiTransparent = Paint()
        mPaintSemiTransparent.alpha = (255 * .7).toInt()

        mPath = Path()
    }

    private fun toggleZoom(pt: PointF) {
        mTransform = ter(
            mTransform==null,
            Transformation(2.7f, pt.x, pt.y),
            null
        )

        mIsZoomed = mTransform != null

        invalidate()
    }

    fun setImage(bmp: Bitmap, mask: Bitmap?) {

        val w = width
        val h = height

        this.mBmp = Bmp.getResizedBitmap(bmp, w, h)
        this.mBmp.setHasAlpha(true)

        this.mBmpDraw = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        this.mBmpDraw.setHasAlpha(true)

        this.mCanvasDraw = Canvas(this.mBmpDraw)

        // drawing all red
        var paint = Paint()
        paint.style = Paint.Style.FILL
        paint.color = Color.RED
        this.mCanvasDraw.drawRect(0f, 0f, w.toFloat(), h.toFloat(), paint)

        // masking the drawing bmp
        if (mask != null) {
            paint = Paint()
            paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_OUT)
            this.mCanvasDraw.drawBitmap(Bmp.getResizedBitmap(mask, w, h)!!, 0f, 0f, paint)
        }

        mUndo = null
        mTransform = null

        invalidate()
    }

    fun undo() {
        mUndo?.let { bmp ->
            mBmpDraw = bmp
            mCanvasDraw.setBitmap(mBmpDraw)
            mUndo = null

            invalidate()
        }
    }

    fun setMaskColor(color: Int) {
        mPaint.color = color
        mCanvasDraw.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)

        invalidate()
    }

    fun setMode(mode: Mode) {
        this.mMode = mode

        mPath.reset()
        mPathErase.reset()
    }

    private fun setBrushWidth(px: Float) {
        this.mPaint.strokeWidth = px
        this.mPaintErase.strokeWidth = px
    }

    fun setBrushWidthInPercent(progress: Int) {
        val strokeWidth = ((progress/100f) * 80f) + 10f
        setBrushWidth(strokeWidth)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean
    {
        return when(event?.action) {
            MotionEvent.ACTION_DOWN -> {
                touchStart(event.x, event.y)
                true
            }
            MotionEvent.ACTION_MOVE -> {
                touchMove(event.x, event.y)
                true
            }
            MotionEvent.ACTION_UP -> {
                touchEnd(PointF(event.x, event.y))
                true
            }
            else -> super.onTouchEvent(event)
        }
    }

    //

    private fun touchStart(x: Float, y: Float) {
        val tx = mTransform?.translateX(x) ?: x
        val ty = mTransform?.translateY(y) ?: y

        this.mX = tx
        this.mY = ty

        setBrushWidth(ter(mIsZoomed, 18f, 35f))

        mPath.moveTo(tx, ty)
        mPathErase.moveTo(tx, ty)

        mUndo = null
    }

    private fun touchMove(x: Float, y: Float)
    {
        val tx = mTransform?.translateX(x) ?: x
        val ty = mTransform?.translateY(y) ?: y

        val dx = abs(tx - mX)
        val dy = abs(ty - mY)

        val scale = mTransform?.scale ?: 1f

        if (dx >= (TOUCH_TOLERANCE * scale) || dy >= (TOUCH_TOLERANCE * scale)) {

            // zooming?
            if (mMode == Mode.Zoom) return

            cPath.quadTo(mX, mY, (tx + mX) / 2, (ty + mY) / 2)
            mX = tx
            mY = ty

            // setting undo
            if (mUndo == null && cPath == mPath) {
                mUndo = Bitmap.createBitmap(mBmpDraw)
            }

            mCanvasDraw.drawPath(cPath, cPaint)

            invalidate()
        }

    }

    private fun touchEnd(pt: PointF)
    {
        val tx = mTransform?.translateX(x) ?: pt.x
        val ty = mTransform?.translateY(y) ?: pt.y

        // zoom
        if (mMode == Mode.Zoom) {
            toggleZoom(PointF(tx, ty))
            return
        }

        // closing line
        cPath.lineTo(mX, mY)
        mCanvasDraw.drawPath(cPath, cPaint)

        // process?
        if (cPaint == mPaint) {
            // RemoverAlphaTask().execute()
        } else {
            mUndo = null

            invalidate()
            cPath.reset()
        }
    }
    //

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.save()

        val scale = mTransform?.scale ?: 1f
        val left = mTransform?.translatedLeft() ?: 0f
        val top = mTransform?.translatedTop() ?: 0f

        canvas?.translate(left, top)
        canvas?.scale(scale, scale)

        canvas?.drawBitmap(mBmp, 0f, 0f, null)
        canvas?.drawBitmap(mBmpDraw, 0f, 0f, mPaintSemiTransparent)

        canvas?.restore()
    }

    fun requestMaskedBmp(cbOnDone: (Bitmap)->Unit) {
        val target = Bitmap.createBitmap(mBmp.width, mBmp.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(target)

        // draw bmp
        canvas.drawBitmap(mBmp, 0f, 0f, null)

        // mask it
        val paint = Paint()
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_OUT)
        canvas.drawBitmap(mBmpDraw, 0f, 0f, paint)

        Task.main(200) {
            cbOnDone(Bmp.trim(target))
        }
    }

    //
    enum class Mode {
        Wand, Erase, Zoom
    }

    interface Listener {
        fun draViewOnLoading(option: Boolean)
        fun drawViewOnRedoCount(count: Int)
    }

    private inner class Transformation(val scale: Float, val ox: Float, val oy: Float) {
        fun translatedLeft(): Float {
            return Num.between(
                (width/2) - (ox * scale),
                width * (1 - scale), 0f
            )
        }
        fun translatedTop(): Float {
            return Num.between(
                (height/2) - (oy*scale),
                height * (1 - scale), 0f
            )
        }

        fun translateX(x: Float): Float {
            return (x - translatedLeft()) / scale
        }

        fun translateY(y: Float): Float {
            return (y - translatedTop()) / scale
        }
    }

}
