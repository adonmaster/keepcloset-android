package br.com.adonio.keepcloset.repos

import android.graphics.BitmapFactory
import androidx.sqlite.db.SimpleSQLiteQuery
import br.com.adonio.keepcloset.db.DB
import br.com.adonio.keepcloset.helpers.*
import br.com.adonio.keepcloset.models.Item
import br.com.adonio.keepcloset.models.ItemDao
import br.com.adonio.keepcloset.models.User
import br.com.adonio.keepcloset.rmodels.ItemGroupColor
import br.com.adonio.keepcloset.rmodels.ItemGroupInt
import br.com.adonio.keepcloset.rmodels.ItemGroupStr
import br.com.adonio.keepcloset.rmodels.ItemGroupTherm
import br.com.adonio.keepcloset.structs.Palette
import br.com.adonio.keepcloset.structs.Therm
import java.io.File

class ItemRepo: BaseRepo<Item, ItemDao>(DB.i.item) {

    fun save(model: Item) = Promise<Item> { resolve, reject ->
        if (model.isNew()) {
            val absolutePath = File(model.path).absolutePath
            val bmp = BitmapFactory.decodeFile(absolutePath)!!
            val thumb = Img.thumbFrom(bmp)
            val filename = Str.random(24)

            F.saveInternal("items", filename, bmp)
                .then { fileOriginal ->
                    F.saveInternal("items/thumbs", filename, thumb)
                        .then { fileThumb ->

                            model.path = fileOriginal.path
                            model.path_thumb = fileThumb.path
                            resolve(save(model, true))

                        }.catch(reject)
                }.catch(reject)
        } else {
            resolve(save(model, true))
        }
    }

    fun getAll(user: User?) = Promise<List<Item>> { resolve, _ ->
        val userId = guard(user?.id) {
            resolve(listOf())
            return@Promise
        }
        resolve(dao.getAll(userId))
    }

    fun queryAll(user: User?, query: String?) = Promise<List<Item>> { resolve, _ ->
        val userId = guard(user?.id) {
            resolve(listOf())
            return@Promise
        }

        Task.main {
            if (Str.isEmpty(query)) {
                resolve(dao.getAll(userId))
            } else {
                resolve(dao.queryAll(userId, "%$query%"))
            }
        }
    }

    fun getForCategories(userId: Long?, categories: List<String>): List<Item> {
        return dao.getForCategories(userId ?: 0, categories)
    }

    fun getAllSynced(user: User?): List<Item> {
        return dao.getAll(user?.id ?: 0)
    }

    fun getCategories(user: User?): List<String> {
        val userId = guard(user?.id) { return listOf() }

        return dao.getCategories(userId)
    }

    fun getCategoriesWithNull(user: User?): List<String?> {
        val userId = guard(user?.id) { return listOf() }
        return dao.getCategoriesWithNull(userId)
    }

    fun getStatuses(user: User?): List<String> {
        val userId = guard(user?.id) { return listOf() }

        return dao.getStatuses(userId)
            .union(listOf("Closet", "Desapego", "Emprestado"))
            .toList()
    }

    fun getBrands(user: User?): List<String> {
        val userId = guard(user?.id) { return listOf() }

        return dao.getBrands(userId)
    }

    fun destroy(id: Long) {
        dao.findSuPaths(id)?.let { model ->
            File(model.path).delete()
            File(model.path_thumb).delete()
        }
        dao.deleteBy(id)
    }

    fun count(userId: Long): Int {
        return dao.count(userId)
    }

    fun getGroupColor(userId: Long): List<ItemGroupColor> {
        val list = dao.getGroupColor(userId)
        return list.map { ItemGroupColor(it.key, it.count, Palette.shared.translate(it.key)) }
    }

    fun getGroupStatus(userId: Long): List<ItemGroupStr> {
        return dao.getGroupStatus(userId)
    }

    fun getGroupTherm(userId: Long): List<ItemGroupTherm> {
        return dao.getGroupTherm(userId).map {
            val therm = Therm.f(it.key) ?: Therm.FINE
            ItemGroupTherm(therm.toString(), it.count, therm.colorLightTheme())
        }
    }

    fun getGroupBrand(userId: Long): List<ItemGroupStr> {
        return dao.getGroupBrand(userId)
    }

}