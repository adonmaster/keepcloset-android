package br.com.adonio.keepcloset.frags.main.inspiration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.helpers.Task
import kotlinx.android.synthetic.main.frag_inspiration_slide_item.*
import java.io.File

class FragInspirationSlideItem: Fragment() {

    // static
    companion object {
        fun f(position: Int): FragInspirationSlideItem {
            val bundle = Bundle()
            bundle.putInt("position", position)
            return FragInspirationSlideItem().also {
                it.arguments = bundle
            }
        }
    }

    // state
    private lateinit var vm: FragMainInspirationVM
    private lateinit var vmSlide: FragInspirationSlideVM

    // static
    private val cPosition get() = arguments!!.getInt("position")

    // methods
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_inspiration_slide_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(activity!!).get(FragMainInspirationVM::class.java)
        vmSlide = ViewModelProviders.of(activity!!).get(FragInspirationSlideVM::class.java)

        vmSlide.selectedIndex.observe(this, Observer { position ->
            position?.let { updateDesktop(it) }
        })

    }

    private fun updateDesktop(position: Int) {
        if (position != cPosition) return

        Task.debounce("FragInspirationSlideItem", 300) {
            vm.selectedRawList.getOrNull(cPosition)?.also { item ->
                Img.load(File(item.path), ui_inspiration_item_img)
            }
        }
    }

}