package br.com.adonio.keepcloset.structs

import android.annotation.SuppressLint
import androidx.annotation.ColorRes
import br.com.adonio.keepcloset.R

class Palette {

    // static
    companion object {
        val shared = Palette()
    }

    // const

    val full = listOf(
        Pair("PRETO", listOf(
            0xFF000000, 0xFF001A34, 0xFF362D29
        )),
        Pair("BRANCO", listOf(
            0xFFF5FAFB, 0xFFF0F1EC, 0xFFF4F3EA, 0xFFFAFBF6
        )),
        Pair("CINZA", listOf(
            0xFF40414A, 0xFF515A60, 0xFF6E767F, 0xFF666666, 0xFF878789, 0xFFA49EA2, 0xFFB7B7B7, 0xFFD4D4D4
        )),
        Pair("BEGE", listOf(
            0xFF947F78, 0xFFA07E6E, 0xFFC78D6C, 0xFFD6B68A, 0xFFB79B84, 0xFFE5CBB7, 0xFFF8DEC7, 0xFFF3F1E4
        )),
        Pair("VERMELHO", listOf(
            0xFF7F182E, 0xFFD1232A, 0xFFF01836, 0xFFEC332E, 0xFFDF4516, 0xFF9D5053, 0xFFE15A52, 0xFFFE756D
        )),
        Pair("LARANJA", listOf(
            0xFFFF5A00, 0xFFC65821, 0xFFDF6F30, 0xFFEE854D, 0xFFFFA306, 0xFFD29611, 0xFFFBB875, 0xFFF6D18B
        )),
        Pair("AMARELO", listOf(
            0xFFD4C13F, 0xFFFCC938, 0xFFF4D056, 0xFFFEE05E, 0xFFEEF637, 0xFFFFED2E, 0xFFF9E380, 0xFFFDF4B8
        )),
        Pair("VERDE", listOf(
            0xFF035535, 0xFF4E6D38, 0xFF6B6436, 0xFF7E803E, 0xFF1B9683, 0xFF1F9D4B, 0xFF519E4B, 0xFFB8CC94,
            0xFFBDCC37, 0xFFB7F92F, 0xFFB5DFCD, 0xFFE0E9E2
        )),
        Pair("AZUL", listOf(
            0xFF182046, 0xFF0C316A, 0xFF384A65, 0xFF3145C0, 0xFF0E6195,
            0xFF028FA9, 0xFF00B2CA, 0xFF4E7A8E, 0xFF8BA3D4, 0xFF9DD3E6, 0xFFCDECEB, 0xFFD5E9F8
        )),
        Pair("ROXO", listOf(
            0xFF400A41, 0xFF731F5B, 0xFF5E3B4B, 0xFF432E66, 0xFF462F90, 0xFF964688, 0xFF966DAC,
            0xFFB793B4, 0xFFA091CB, 0xFFADB0D6, 0xFFDCD0DA, 0xFFE2D4E8
        )),
        Pair("ROSA", listOf(
            0xFFB70558, 0xFFCE0056, 0xFFFD12A2, 0xFFEA4EA0, 0xFFE57999, 0xFFE4ABAC, 0xFFD6B5B6,
            0xFFF0C6DC, 0xFFFCD0BF, 0xFFFCCAD6, 0xFFFADCDA, 0xFFF7E5DB
        )),
        Pair("MARROM", listOf(
            0xFF423635, 0xFF522F24, 0xFF5F4627, 0xFF795349, 0xFF7B4A22, 0xFF9A5837, 0xFFB78240,
            0xFFB87139, 0xFF9C8369, 0xFFB18259, 0xFFCFA77C
        ))
    )

    val list by lazy<List<Pair<Long, String>>> {
        val r = ArrayList<Pair<Long, String>>()
        for (pair in full) {
            for (cl in pair.second) {
                r.add(Pair(cl, pair.first))
            }
        }
        r
    }

    @SuppressLint("DefaultLocale")
    fun translate(colorDesc: String): Int {
        val list = hashMapOf(
            "PRETO" to R.color.md_black_1000,
            "BRANCO" to R.color.white,
            "CINZA" to R.color.md_grey_500,
            "BEGE" to R.color.md_red_100,
            "VERMELHO" to R.color.md_red_500,
            "LARANJA" to R.color.md_orange_500,
            "AMARELO" to R.color.md_yellow_500,
            "VERDE" to R.color.md_green_500,
            "AZUL" to R.color.md_blue_500,
            "ROXO" to R.color.md_purple_500,
            "ROSA" to R.color.md_pink_500,
            "MARROM" to R.color.md_brown_500
        )
        return list[colorDesc.trim().toUpperCase()] ?: R.color.transparent
    }

    fun randomResolved(size: Int): List<Int> {
        if (size <= 0) return listOf()
        return list.map { it.first.toInt() }.shuffled().take(size)
    }

}