package br.com.adonio.keepcloset.frags.main.closet

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class FragMainClosetEditVM: ViewModel() {

    val loading = MutableLiveData<Boolean>()
    var category = MutableLiveData<String?>()
    var brand = MutableLiveData<String?>()
    var status = MutableLiveData<String?>()
    var therm = MutableLiveData<Int>()

    var color1 = MutableLiveData<Pair<Long, String>>()
    var color2 = MutableLiveData<Pair<Long, String>>()

}