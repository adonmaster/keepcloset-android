package br.com.adonio.keepcloset.repos

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import br.com.adonio.keepcloset.db.DB
import br.com.adonio.keepcloset.helpers.F
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.helpers.Str
import br.com.adonio.keepcloset.helpers.guard
import br.com.adonio.keepcloset.models.Inspiration
import br.com.adonio.keepcloset.models.InspirationDao
import br.com.adonio.keepcloset.models.User
import java.io.File

class InspirationRepo: BaseRepo<Inspiration, InspirationDao>(DB.i.inspiration) {

    fun getAll(userId: Long) = dao.getAll(userId)

    fun getForCategories(userId: Long, categories: List<String>): List<Inspiration> {
        return dao.getForCategories(userId, categories)
    }

    fun createFor(userId: Long, category: String?, file: File): Inspiration {
        val model = Inspiration()
        model.user_id = userId
        model.category = category

        // image
        val full = F.copyInternal(file, "inspirations", Str.random(24))
        model.path = full.path

        // thumb
        val bmp = BitmapFactory.decodeFile(file.path)
        val bmpThumb = Img.thumbFrom(bmp)
        val thumb = F.saveInternalSynced("inspirations/thumb", Str.random(24), bmpThumb)
        model.path_thumb = thumb.path

        return save(model)
    }

    fun getCategories(user: User?): List<String>? {
        val userId = guard(user?.id) { return listOf() }
        return dao.getCategories(userId)
    }

    fun getCategoriesWithNull(user: User?): List<String?> {
        val userId = guard(user?.id) { return listOf() }
        return dao.getCategoriesWithNull(userId)
    }

    fun destroy(model: Inspiration) {
        File(model.path).delete()
        File(model.path_thumb).delete()

        dao.delete(model)
    }

}