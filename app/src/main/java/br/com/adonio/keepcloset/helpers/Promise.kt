package br.com.adonio.keepcloset.helpers

class Promise<V>(cb: ((V) -> Unit, (String) -> Unit) -> Unit) {

    private var resolver: (V)->Unit = {}
    private var rejector: (String)->Unit = {}
    private var doner: (()->Unit)? = null

    init {
        Task.main {
            try {
                cb(resolver, rejector)
            } catch(e: Exception) {
                rejector(e.localizedMessage)
            }
        }
    }

    fun then(cb: (V)->Unit): Promise<V> {
        resolver = {
            cb(it)
            doner?.invoke()
        }
        return this
    }

    fun catch(cb: (String)->Unit): Promise<V> {
        rejector = {
            cb(it)
            doner?.invoke()
        }
        return this
    }

    fun finally(cb: ()->Unit) {
        doner = cb
    }

}

fun <T> promiseIt(obj: T?) = Promise<T> { resolve, reject ->
    try {
        if (obj!=null) {
            resolve(obj)
        } else {
            reject("objeto vazio")
        }
    } catch(e: Exception) {
        reject(e.localizedMessage)
    }
}