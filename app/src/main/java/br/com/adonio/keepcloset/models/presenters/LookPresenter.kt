package br.com.adonio.keepcloset.models.presenters

import android.widget.ImageView
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.models.Look
import java.io.File

class LookPresenter(model: Look) : BasePresenter<Look>(model) {

    fun loadFull(imageView: ImageView?) {
        Img.load(File(model.path), imageView)
    }

}