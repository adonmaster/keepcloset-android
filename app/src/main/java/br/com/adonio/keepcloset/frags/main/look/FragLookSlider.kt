package br.com.adonio.keepcloset.frags.main.look

import android.os.Bundle
import android.view.*
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.DialogOk
import br.com.adonio.keepcloset.helpers.Act
import br.com.adonio.keepcloset.helpers.F
import br.com.adonio.keepcloset.helpers.Task
import br.com.adonio.keepcloset.models.Look
import br.com.adonio.keepcloset.repos.Repo
import kotlinx.android.synthetic.main.frag_look_slider.*

class FragLookSlider: Fragment() {

    // static
    companion object {
        fun navigate(frag: Fragment, @IdRes id: Int, position: Int) {
            val bundle = Bundle()
            bundle.putInt("position", position)
            frag.findNavController().navigate(id, bundle)
        }
    }

    //
    private lateinit var vmParent: FragMainLookVM
    private var mPosition = 0

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.frag_look_slider, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        vmParent = ViewModelProviders.of(activity!!).get(FragMainLookVM::class.java)

        //
        mPosition = arguments?.getInt("position") ?: 0

        //
        ui_pager.adapter = object: FragmentStatePagerAdapter(childFragmentManager) {
            override fun getCount(): Int {
                return vmParent.items.count()
            }
            override fun getItem(position: Int): Fragment {
                return FragLookSliderItem.f(position)
            }
        }
        ui_pager.currentItem = mPosition
        ui_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                mPosition = position
            }
        })
    }

    private fun destroy(item: Look?) {
        val lookId = item?.id ?: return
        DialogOk.show(
            context,
            getString(R.string.quer_mesmo_apagar_esse_registro),
            getString(R.string.atencao),
            getString(R.string.cancelar)
        ) {
            this.reallyDestroy(lookId)
        }
    }

    private fun reallyDestroy(modelId: Long) {
        Repo.look.destroy(modelId)
        findNavController().popBackStack()
    }

    //
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater?.inflate(R.menu.menu_frag_look_slider, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item?.itemId) {
            R.id.menu_action_delete -> {
                destroy(vmParent.items.getOrNull(mPosition))
                return true
            }
            R.id.menu_action_edit -> {
                vmParent.modelEditing = vmParent.items[mPosition]
                findNavController().navigate(R.id.action_to_look_edit)
                return true
            }
            R.id.menu_action_share -> {
                val model = vmParent.items[mPosition]
                val path = model.path ?: return true

                Task.main(10) {
                    val f = F.copyExternal(path, "Keep_Closet_Look_1.png")
                    val act = activity
                    if (f!=null && act!=null) {
                        Act.shareImage(act, f)
                    }
                }

                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}