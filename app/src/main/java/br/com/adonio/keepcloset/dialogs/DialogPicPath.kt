package br.com.adonio.keepcloset.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.adapters.DialogPicPathAdapter
import br.com.adonio.keepcloset.repos.Repo
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.dialog_pic_path.*

class DialogPicPath: DialogBase() {

    // static
    companion object {
        fun show(fragmentManager: FragmentManager?) {
            if (fragmentManager==null) return
            DialogPicPath().show(fragmentManager, "DialogPicPath")
        }

        private var listener: Listener? = null

        fun attach(listener: Listener) {
            this.listener = listener
        }
        fun detach() {
            this.listener = null
        }
    }

    // state
    private lateinit var mAdapter: DialogPicPathAdapter

    // methods

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.setTitle(getString(R.string.escolher_midia))
        return inflater.inflate(R.layout.dialog_pic_path, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ui_tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) { }
            override fun onTabUnselected(p0: TabLayout.Tab?) { }
            override fun onTabSelected(p0: TabLayout.Tab?) {
                updateList()
            }
        })

        mAdapter = DialogPicPathAdapter(object : DialogPicPathAdapter.Listener {
            override fun onSelect(item: DialogPicPathAdapter.Item) {
                listener?.dialogPicPathItem(item)
            }
        })
        ui_rv_pics.adapter = mAdapter
        ui_rv_pics.layoutManager = GridLayoutManager(context, 4)
        ui_rv_pics.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_slide_right)
        updateList()

        ui_btn_cancel.setOnClickListener { dismissAllowingStateLoss() }
        ui_btn_submit.setOnClickListener {
            dismissAllowingStateLoss()
        }
    }

    private fun updateList() {
        when (ui_tab_layout.selectedTabPosition) {
            0 -> Repo.item.getAll(App.modelUser).then { mAdapter.updateFromItems(it) }
            1 -> mAdapter.updateFromLooks(Repo.look.getAll(App.modelUser?.id ?: 0))
            2 -> mAdapter.updateFromInspirations(Repo.inspiration.getAll(App.modelUser?.id ?: 0))
            3 -> {
                dismissAllowingStateLoss()
                listener?.dialogPicPathText()
            }
            4 -> {
                dismissAllowingStateLoss()
                listener?.dialogPicPathPickAlbum()
            }
            5 -> {
                dismissAllowingStateLoss()
                listener?.dialogPicPathPickCamera()
            }
        }
    }

    // listener
    interface Listener {
        fun dialogPicPathItem(item: DialogPicPathAdapter.Item)
        fun dialogPicPathPickAlbum()
        fun dialogPicPathPickCamera()
        fun dialogPicPathText()
    }

}