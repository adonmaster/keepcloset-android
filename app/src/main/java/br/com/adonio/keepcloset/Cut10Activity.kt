package br.com.adonio.keepcloset

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import br.com.adonio.keepcloset.helpers.Task
import kotlinx.android.synthetic.main.activity_cut10.*
import org.opencv.android.OpenCVLoader
import java.lang.RuntimeException

class Cut10Activity: AppCompatActivity() {

    //
    companion object {
        @Suppress("PropertyName")
        val REQUEST_CODE = 7798

        fun start(frag: Fragment?, path: String) {
            if (frag==null) return

            val intent = Intent(frag.context, Cut10Activity::class.java)
            intent.putExtra("path", path)
            frag.startActivityForResult(intent, REQUEST_CODE)
        }
        inline fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?, cb: (String)->Unit) {
            if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                data?.getStringExtra("path")?.let(cb)
            }
        }
    }

    //
    private val cPath get() = intent.getStringExtra("path")!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cut10)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        // async calling openCvLoader
        Task.main {
            if ( ! OpenCVLoader.initDebug()) throw RuntimeException("OpenCVLoader.initDebug()")
        }

        val vm = ViewModelProviders.of(this).get(Cut10VM::class.java)
        vm.path = cPath
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

}