package br.com.adonio.keepcloset.dialogs

import android.content.Context
import androidx.appcompat.app.AlertDialog


class DialogList {

    companion object {

        private var listener: Listener? = null

        fun show(context: Context, title: String, list: List<String>) {
            val dialog = AlertDialog.Builder(context)
            dialog.setTitle(title)
            dialog.setItems(list.toTypedArray()) { d, which ->
                d.dismiss()
                listener?.dialogListOnChoose(list[which], which)
            }
            dialog.setPositiveButton("OK") { d, _ ->
                d.dismiss()
            }
            val alert = dialog.create()
            alert.show()
        }

        fun attach(listener: Listener) {
            this.listener = listener
        }
        fun detach() {
            this.listener = null
        }
    }

    // listener
    interface Listener {
        fun dialogListOnChoose(s: String, position: Int)
    }

}