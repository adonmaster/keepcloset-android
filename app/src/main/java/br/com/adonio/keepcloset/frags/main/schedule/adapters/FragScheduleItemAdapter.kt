package br.com.adonio.keepcloset.frags.main.schedule.adapters

import android.content.Context
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.extensions.setVisible
import br.com.adonio.keepcloset.helpers.Dt
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.helpers.Promise
import br.com.adonio.keepcloset.models.ScheduleItem
import br.com.adonio.keepcloset.repos.Repo
import kotlinx.android.synthetic.main.gv_frag_schedule_item.view.*
import org.threeten.bp.LocalDate
import java.io.File

class FragScheduleItemAdapter(context: Context, private val dt: LocalDate, private val listener: Listener):
    MyBaseAdapter<FragScheduleItemAdapter.VH>(context) {

    private var mData = listOf<LocalDate>()
    private val mPhotos = ArrayList<ScheduleItem>()


    init {
        retrieveDays().then { list ->
            mData = list
            notifyDataSetChanged()

            updatePhotoData(list.first(), list.last())
        }
    }

    private fun retrieveDays() = Promise<List<LocalDate>> { resolve, _ ->

        var cur = dt.withDayOfMonth(1)
        var diff = cur.dayOfWeek.value
        if (diff < 7) cur = cur.minusDays(diff.toLong())

        var end = dt.withDayOfMonth(1).plusMonths(1).minusDays(1)
        diff = 6 - end.dayOfWeek.value
        end = end.plusDays(diff.toLong())

        val r = arrayListOf<LocalDate>()
        while (cur <= end) {
            r.add(cur)
            cur = cur.plusDays(1)
        }

        resolve(r)
    }

    // methods

    private fun updatePhotoData(ini: LocalDate, end: LocalDate) {
        Repo.schedule.getBetweenPromise(App.modelUser?.id ?: 0, ini, end)
            .then { list ->
                mPhotos.clear()
                mPhotos.addAll(list)

                notifyDataSetChanged()
            }
    }

    override fun getCount(): Int {
        return mData.count()
    }

    override fun getCellLayoutId(): Int {
        return R.layout.gv_frag_schedule_item
    }

    override fun createViewHolder(itemView: View): VH {
        return VH(itemView)
    }

    override fun bindViewHolder(vh: VH, position: Int) {
        val m = mData[position]
        val isThisMonth = m.year == dt.year && m.month == dt.month

        //
        vh.itemView.setOnClickListener {
            if (isThisMonth) {
                listener.onSelect(m)
            }
        }

        // desc
        vh.uiLbl.text = Dt.format(m, "d")
        vh.uiLbl.alpha = if (isThisMonth) 1.0f else 0.2f

        // image
        val urlThumb = mPhotos.findLast{ it.dt == m }?.path_thumb
        vh.uiImg.alpha = if (isThisMonth) 1.0f else 0.2f
        if (urlThumb != null) {
            Img.load(File(urlThumb), vh.uiImg)
        } else {
            vh.uiImg.alpha = 0f
        }
    }

    // view holder
    inner class VH(itemView: View) : MyBaseAdapter.ViewHolder(itemView) {
        val uiLbl: TextView = itemView.ui_lbl
        val uiImg: ImageView = itemView.ui_img
    }

    // listener
    interface Listener {
        fun onSelect(dt: LocalDate)
    }

}