package br.com.adonio.keepcloset.widgets

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.PointF
import android.view.MotionEvent
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.extensions.getLocationInScreen
import br.com.adonio.keepcloset.helpers.Cl
import br.com.adonio.keepcloset.helpers.Task
import br.com.adonio.keepcloset.helpers.guard
import java.lang.Math.pow
import java.util.*
import kotlin.math.atan2
import kotlin.math.sqrt
import kotlin.properties.Delegates

class AnchorImageView(context: Context?) : androidx.appcompat.widget.AppCompatImageView(context) {

    init {
        isFocusable = true
        isClickable = true
    }

    // state
    private var mAnchor: Bitmap = BitmapFactory.decodeResource(context?.resources, R.drawable.img_anchor_24)
    @Suppress("DEPRECATION")
    private var mSelected by Delegates.observable(false) { _, _, v ->
//        alpha = if (v) 0.5f else 1.0f
        if (v) {
            background = resources.getDrawable(R.drawable.shape_transparent_border_accent)
        } else {
            setBackgroundColor(Cl.res(context!!, R.color.transparent))
        }

        invalidate()
        anchorOnSelect?.invoke(this, v)
    }

    // public
    var anchorOnSelect: ((AnchorImageView, Boolean)->Unit)? = null
    var anchorOnLongClick: ((AnchorImageView)->Unit)?=null
    var anchorSelected: Boolean
        get() = mSelected
        set(value) { mSelected = value }

    // methods

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (mSelected) {
            canvas?.drawBitmap(mAnchor, width.toFloat() / 2, height.toFloat() / 2, null)
        }
    }

    @Suppress("ReplaceJavaStaticMethodWithKotlinAnalog", "unused")
    private fun extractOriginalDistanceFromMidPoint(): Float {
        return sqrt(
            pow(width.toDouble() / 2.0, 2.0).toFloat() +
            pow(height.toDouble() / 2.0, 2.0).toFloat()
        )
    }

    private data class PointStamp(
        val raw: PointF, val deltaPt: PointF, var timestamp: Long, var radians: Float
    )
    private val mPtDown = PointStamp(PointF(), PointF(), 0, 0f)
    private val mRawMidPt = PointF(0f, 0f)
    private var mLastLongClickUid = ""

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val e = guard(event) { return true }
        val v = this

        val location = v.getLocationInScreen()
        val scaledWidth = v.width * v.scaleX
        val scaledHeight = v.height * v.scaleY

        when (e.action) {
            MotionEvent.ACTION_DOWN -> {
                // long click
                mLastLongClickUid = Task.mainIndexed(1100) {
                    anchorOnLongClick?.invoke(this)
                }

                mPtDown.timestamp = Calendar.getInstance().time.time
                mPtDown.deltaPt.set(v.x - e.rawX, v.y - e.rawY)
                mPtDown.raw.set(e.rawX, e.rawY)
                mPtDown.radians = atan2(e.rawX - mRawMidPt.x, e.rawY - mRawMidPt.y)

                mRawMidPt.set(location.x + scaledWidth/2, location.y + scaledHeight/2)
            }
            MotionEvent.ACTION_MOVE -> {
                Task.removeMainIndex(mLastLongClickUid)

                // rotate & scale
                if (mSelected) {
                    /*
                    val midPt = PointF(v.width.toFloat()/2, v.height.toFloat()/2)
                    // scale
                    val distance = sqrt(
                        pow((e.x - v.x - midPt.x).toDouble(), 2.0) +
                        pow((e.y - v.y - midPt.y).toDouble(), 2.0)
                    )
                    val dScale = distance.toFloat() / extractOriginalDistanceFromMidPoint() * v.scaleX
                    v.scaleX = dScale
                    v.scaleY = v.scaleX

                    // rotate
                    val radians = atan2(e.rawX - mRawMidPt.x, e.rawY - mRawMidPt.y)
                    val degrees = toDegrees(mPtDown.radians - radians.toDouble()).toFloat()
                    v.rotation = degrees
                    Log.d("Adon", "location: $location, midPoint: $mRawMidPt")
                    */
                }

                // pan
                else
                {
                    v.animate()
                        .x(e.rawX + mPtDown.deltaPt.x)
                        .y(e.rawY + mPtDown.deltaPt.y)
                        .setDuration(0)
                        .start()
                }
            }
            MotionEvent.ACTION_UP -> {
                Task.removeMainIndex(mLastLongClickUid)

                val now = Calendar.getInstance().time.time
                val diff = now - mPtDown.timestamp
                if (diff < 250) {
                    mSelected = !mSelected
                } else {
                    if (mSelected) mSelected = false
                }
            }
            else -> { }
        }
        return true
    }

}