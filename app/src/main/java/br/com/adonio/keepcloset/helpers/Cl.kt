package br.com.adonio.keepcloset.helpers

import android.content.Context
import android.os.Build
import androidx.annotation.ColorRes

@Suppress("DEPRECATION")
object Cl {

    fun res(context: Context, @ColorRes id: Int): Int {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            context.resources.getColor(id, context.theme)
        } else {
            context.resources.getColor(id)
        }
    }

}