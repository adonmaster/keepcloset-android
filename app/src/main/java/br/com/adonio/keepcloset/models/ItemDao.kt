package br.com.adonio.keepcloset.models

import androidx.room.Dao
import androidx.room.Query
import androidx.room.RawQuery
import androidx.sqlite.db.SimpleSQLiteQuery
import br.com.adonio.keepcloset.helpers.Str
import br.com.adonio.keepcloset.helpers.ter
import br.com.adonio.keepcloset.rmodels.ItemGroupInt
import br.com.adonio.keepcloset.rmodels.ItemGroupStr

@Dao
abstract class ItemDao: BaseDao<Item>() {

    @Query("select * from items where id = :id LIMIT 1")
    abstract override fun find(id: Long): Item?

    @Query("select distinct category from items where user_id = :userId AND category is not null order by category")
    abstract fun getCategories(userId: Long): List<String>

    @Query("select distinct category from items where user_id = :userId order by category")
    abstract fun getCategoriesWithNull(userId: Long): List<String?>

    @Query("select * from items where user_id = :userId order by category, id desc")
    abstract fun getAll(userId: Long): List<Item>

    @Query("select * from items where user_id = :userId and (brand like :queryWithWildcards or category like :queryWithWildcards or color1_desc like :queryWithWildcards or color2_desc like :queryWithWildcards or status like :queryWithWildcards) order by category, id desc")
    abstract fun queryAll(userId: Long, queryWithWildcards: String): List<Item>

    @Query("select * from items where user_id = :userId and coalesce(category, 'null') in (:categories) order by category, id desc")
    abstract fun getForCategories(userId: Long, categories: List<String>): List<Item>

    @Query("delete from items where id = :id")
    abstract fun deleteBy(id: Long)

    @Query("select id, path, path_thumb from items where id = :id LIMIT 1")
    abstract fun findSuPaths(id: Long): ItemSuPaths?

    @Query("select DISTINCT status from items where user_id = :userId AND status is not null order by status")
    abstract fun getStatuses(userId: Long): List<String>

    @Query("select DISTINCT brand from items where user_id = :userId AND brand is not null order by brand")
    abstract fun getBrands(userId: Long): List<String>

    @Query("select count(*) as count from items where user_id = :userId")
    abstract fun count(userId: Long): Int

    //

    @Query("select color1_desc as `key`, count(*) as count from items where user_id = :userId and color1_desc is not null group by color1_desc")
    abstract fun getGroupColor(userId: Long): List<ItemGroupStr>

    @Query("select status as `key`, count(*) as count from items where user_id = :userId and status is not null group by status")
    abstract fun getGroupStatus(userId: Long): List<ItemGroupStr>

    @Query("select therm_id as `key`, count(*) as count from items where user_id = :userId group by therm_id")
    abstract fun getGroupTherm(userId: Long): List<ItemGroupInt>

    @Query("select brand as `key`, count(*) as count from items where user_id = :userId and brand is not null group by brand")
    abstract fun getGroupBrand(userId: Long): List<ItemGroupStr>

}