package br.com.adonio.keepcloset.helpers

object Synced {

    val cache = arrayListOf<String>()

    inline fun invoke(key: String, cb: SyncedDsl.()->Unit) {
        if (cache.contains(key)) return

        cache.add(key)

        cb(SyncedDsl(key))
    }

}

class SyncedDsl(private val key: String) {
    fun syncRelease() {
        Synced.cache.remove(key)
    }
}

fun synced(key: String, cb: SyncedDsl.()->Unit) {
    Synced.invoke(key, cb)
}
