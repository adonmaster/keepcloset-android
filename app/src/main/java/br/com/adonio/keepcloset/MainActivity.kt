package br.com.adonio.keepcloset

import android.Manifest
import android.annotation.TargetApi
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.navigateUp
import br.com.adonio.keepcloset.dialogs.DialogOk
import br.com.adonio.keepcloset.helpers.Act
import br.com.adonio.keepcloset.helpers.T
import br.com.adonio.keepcloset.models.User
import br.com.adonio.keepcloset.services.AdService
import com.android.billingclient.api.*
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.nav_header_main.*
import java.lang.ref.WeakReference


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, PurchasesUpdatedListener {

    // static
    companion object {
        var shared: MainActivity? = null
    }

    // state
    private lateinit var uiDrawerLayout: DrawerLayout
    private lateinit var billingClient: BillingClient
    private var billingSkuDetails: SkuDetails? = null
    private val billingSku = "no_ads"

    private val cUiNavController
        get() = Navigation.findNavController(this, R.id.ui_nav_content)
    private val uiMainAppBarConfiguration by lazy {
        AppBarConfiguration(
            setOf(
                R.id.nav_content_frag_main,
                R.id.nav_content_frag_about,
                R.id.nav_content_frag_profile,
                R.id.nav_content_frag_stats,
                R.id.nav_content_frag_support
            ),
            uiDrawerLayout
        )
    }

    private var uiChildNavController = WeakReference<NavController?>(null)
    private var uiChildAppBarConfiguration: AppBarConfiguration? = null


    // methods
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        // drawer
        this.uiDrawerLayout = findViewById(R.id.drawer_layout)
        uiDrawerLayout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerStateChanged(newState: Int) {}
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}
            override fun onDrawerClosed(drawerView: View) {}
            override fun onDrawerOpened(drawerView: View) {
                updateDesktopDrawer(App.modelUser)
            }
        })

        // nav view
        val navView: NavigationView = findViewById(R.id.nav_view)
        navView.setNavigationItemSelectedListener(this)

        // setup billing
        setupBilling()
    }

    fun setupChildNavController(
        navController: NavController?,
        topLevelDestinationIds: Set<Int>? = null
    ) {
        uiChildNavController = WeakReference(navController)
        if (navController==null) {
            NavigationUI.setupActionBarWithNavController(
                this,
                cUiNavController,
                uiMainAppBarConfiguration
            )
        } else {
            uiChildAppBarConfiguration = AppBarConfiguration(
                topLevelDestinationIds ?: setOf(),
                uiDrawerLayout
            )
            NavigationUI.setupActionBarWithNavController(
                this,
                navController,
                uiChildAppBarConfiguration!!
            )
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val child = uiChildNavController.get()

        val r = child?.navigateUp(uiChildAppBarConfiguration!!)
            ?: cUiNavController.navigateUp(uiMainAppBarConfiguration)

        return r || super.onSupportNavigateUp()
    }

    override fun onBackPressed()
    {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.menu_drawer_main -> {
                cUiNavController.navigate(R.id.nav_content_frag_main)
            }
            R.id.menu_drawer_statistics -> {
                cUiNavController.navigate(R.id.nav_content_frag_stats)
            }
            R.id.menu_drawer_about -> {
                cUiNavController.navigate(R.id.nav_content_frag_about)
            }
            R.id.menu_drawer_support -> {
                cUiNavController.navigate(R.id.nav_content_frag_support)
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onStart() {
        super.onStart()

        shared = this
        updateDesktop()
    }

    override fun onStop() {
        super.onStop()
        shared = null
    }

    private fun updateDesktop() {
        val user = App.modelUser
        if (user == null) {
            LoginActivity.start(this)
        } else {
            updateDesktopDrawer(user)
        }
    }

    private fun updateDesktopDrawer(user: User?) {
        if (user==null) return

        ui_drawer_title?.text = user.p().name
        ui_drawer_desc?.text = user.email
        user.p().loadAvatar(ui_drawer_avatar)
        ui_drawer_version?.text = "Versão: ${Act.versionName()}"

        ui_btn_profile?.setOnClickListener {
            cUiNavController.navigate(R.id.nav_content_frag_profile)
            uiDrawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    // billing
    private fun setupBilling()
    {
        billingClient = BillingClient.newBuilder(this)
            .enablePendingPurchases()
            .setListener(this)
            .build()

        billingClient.startConnection(object : BillingClientStateListener {

            override fun onBillingSetupFinished(p0: BillingResult) {
                if (p0.responseCode == BillingClient.BillingResponseCode.OK && billingClient.isReady) {

                    // config sku details
                    billingRetrieveSkullDetails()

                    // list purchases
                    val list = billingClient.queryPurchases(BillingClient.SkuType.INAPP).purchasesList
                    if (
                        list != null
                        && list.firstOrNull { it.sku == this@MainActivity.billingSku } != null
                    )
                    {
                        AdService.savePreferences(false)
                    } else {
                        AdService.savePreferences(true)
                    }

                } else {
                    T.error(this@MainActivity, "Erro ao conectar compra e423.")
                }
            }

            override fun onBillingServiceDisconnected() {
            }
        })
    }

    private fun billingRetrieveSkullDetails() {
        val params = SkuDetailsParams.newBuilder()
            .setSkusList(arrayListOf(billingSku))
            .setType(BillingClient.SkuType.INAPP)
            .build()

        billingClient.querySkuDetailsAsync(params) { billingResult, mutableList ->
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                billingSkuDetails = mutableList?.firstOrNull { it.sku == billingSku }
                if (billingSkuDetails == null) {
                    T.error(
                        this@MainActivity,
                        "O produto ainda não foi desbloqueado para compra T3342"
                    )
                }
            }
        }
    }

    fun purchaseBilling() {
        val details = billingSkuDetails ?: return

        val params = BillingFlowParams.newBuilder()
            .setSkuDetails(details)
            .build()

        billingClient.launchBillingFlow(this, params)
    }

    override fun onPurchasesUpdated(p0: BillingResult, p1: MutableList<Purchase>?) {
        if (p0.responseCode == BillingClient.BillingResponseCode.OK)
        {
            val purchase = p1?.firstOrNull { it.sku == billingSku }
            if (purchase != null) {
                if (purchase.purchaseState == Purchase.PurchaseState.PURCHASED) {
                    // entitlement
                    AdService.savePreferences(false)

                    //
                    if ( ! purchase.isAcknowledged) {
                        val aparams = AcknowledgePurchaseParams.newBuilder()
                            .setPurchaseToken(purchase.purchaseToken)
                            .build();

                        billingClient.acknowledgePurchase(aparams) { r ->
                            if (r.responseCode != BillingClient.BillingResponseCode.OK) {
                                AdService.savePreferences(true)
                            }
                        }
                    }
                }
            }
        }
        else if (p0.responseCode == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
            AdService.savePreferences(false)
        }
    }

}
