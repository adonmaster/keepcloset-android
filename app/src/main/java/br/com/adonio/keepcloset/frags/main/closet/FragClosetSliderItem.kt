package br.com.adonio.keepcloset.frags.main.closet

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.structs.Therm
import kotlinx.android.synthetic.main.frag_closet_slider_item.*

class FragClosetSliderItem: Fragment() {

    // static
    companion object {
        fun f(position: Int): FragClosetSliderItem {
            val bundle = Bundle()
            bundle.putInt("position", position)
            val frag = FragClosetSliderItem()
            frag.arguments = bundle
            return frag
        }
    }

    // state
    private lateinit var vmParent: FragMainClosetVM

    // computed
    private val cPosition
        get() = arguments!!.getInt("position")

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_closet_slider_item, container, false)
    }

    @SuppressLint("DefaultLocale")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        vmParent = ViewModelProviders.of(activity!!).get(FragMainClosetVM::class.java)

        //
        val m = vmParent.items[cPosition]

        //
        m.p().loadFull(ui_img)

        //
        val thermDesc = Therm.f(m.therm_id)?.toString()
        ui_desc.text = listOfNotNull(m.category, m.brand, thermDesc, m.color1_desc, m.color2_desc, m.status)
            .takeIf { it.count() > 0 }
            ?.joinToString(", ")
            ?.toLowerCase() ?: getString(R.string.b_sem_descricao_b)
    }

}