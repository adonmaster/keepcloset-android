package br.com.adonio.keepcloset.helpers

import android.content.Context
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import java.util.*
import kotlin.random.Random

object Str {

    private const val mCollectionAlphaNumeric = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_-"
    private var mRandom = Random(23451)

    fun random(len: Int): String {
        val sb = StringBuilder(len)
        for (i in 0 until len) {
            sb.append(mCollectionAlphaNumeric[mRandom.nextInt(mCollectionAlphaNumeric.length)])
        }
        sb.append(Calendar.getInstance().time.time)
        return sb.toString()
    }

    fun coalesce(vararg args: String?): String {
        for (arg in args) {
            if (arg!=null) return arg
        }
        return ""
    }

    fun colorEmpty(context: Context, s: String?, emptyText: String): CharSequence? {
        return if (s?.trim()?.isEmpty() == false) s else Span(emptyText)
            .color(context, R.color.md_grey_500)
            .build()
    }

    fun plural(i: Int, zero: String="", single: String="", multiple: (Int)->String={ _ ->""}): String {
        return when {
            i <= 0 -> zero
            i == 1 -> single
            else -> multiple(i)
        }
    }

    fun isEmpty(s: String?): Boolean {
        return s?.trim()?.isEmpty() != false
    }

}