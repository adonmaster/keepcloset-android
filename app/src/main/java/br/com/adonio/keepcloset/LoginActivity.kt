package br.com.adonio.keepcloset

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation

class LoginActivity: AppCompatActivity() {

    // static
    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, LoginActivity::class.java)
            activity.startActivityForResult(intent, 551)
        }
    }

    // ui
    private lateinit var uiNav: NavController

    // state
    lateinit var vm: LoginActivityVM

    // methods

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        vm = ViewModelProviders.of(this).get(LoginActivityVM::class.java)

        uiNav = Navigation.findNavController(this, R.id.ui_nav)
    }

}