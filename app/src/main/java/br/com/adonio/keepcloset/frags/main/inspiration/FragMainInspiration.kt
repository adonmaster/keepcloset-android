package br.com.adonio.keepcloset.frags.main.inspiration

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.DialogAd
import br.com.adonio.keepcloset.dialogs.DialogStrPicker
import br.com.adonio.keepcloset.extensions.setVisible
import br.com.adonio.keepcloset.frags.main.inspiration.adapter.FragMainInspirationAdapter
import br.com.adonio.keepcloset.helpers.*
import br.com.adonio.keepcloset.models.Inspiration
import br.com.adonio.keepcloset.repos.Repo
import br.com.adonio.keepcloset.services.AdService
import kotlinx.android.synthetic.main.frag_main_inspiration.*
import java.io.File

class FragMainInspiration: Fragment(), DialogStrPicker.Listener, DialogAd.Listener {

    private lateinit var vm: FragMainInspirationVM
    private lateinit var mAdapter: FragMainInspirationAdapter
    private var mListSelected: List<File>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_main_inspiration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(activity!!).get(FragMainInspirationVM::class.java)

        mAdapter = FragMainInspirationAdapter(object : FragMainInspirationAdapter.Listener {
            override fun onEmpty(option: Boolean) {
                ui_empty.setVisible(option)
            }
            override fun onSelect(rawList: List<Inspiration>, rawPosition: Int) {
                select(rawList, rawPosition)
            }
        })
        ui_rv.adapter = mAdapter
        ui_rv.layoutManager = GridLayoutManager(context, 3).also {
            it.spanSizeLookup = object: GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (mAdapter.isItemCategory(position)) 3 else 1
                }
            }
        }
        ui_rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_slide_right)

        updateData()

        ui_btn_add.setOnClickListener {
            if (AdService.shouldShowAd()) {
                AdService.showAdDialog(childFragmentManager)
            } else {
                add()
            }
        }
    }

    private fun updateData() {
        mAdapter.updateData(Repo.inspiration.getAll(App.modelUser?.id ?: 0))
    }

    private fun add() {
        ImgProvider.pick(this, 100)
    }

    private fun select(rawList: List<Inspiration>, rawPosition: Int) {
        vm.selectedRawList = rawList
        vm.selectedRawPosition = rawPosition

        if (vm.selectedRawList.count() > 0) {
            findNavController().navigate(R.id.sg_to_frag_inspiration_slide)
        }
    }

    private fun save(category: String?, list: List<File>) {
        for (f in list) {
            Repo.inspiration.createFor(App.modelUser?.id ?: 0, category, f)
        }
        updateData()
    }

    override fun onStart() {
        super.onStart()
        DialogStrPicker.attach(this)
        DialogAd.attach(this)
    }

    override fun onStop() {
        super.onStop()
        DialogStrPicker.detach()
        DialogAd.detach(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // image picker
        ImgProvider.onActivityResultAll(requestCode, resultCode, data)
            ?.takeIf { it.count() > 0 }
            ?.map { File(it) }
            ?.let {
                mListSelected = it
                Task.main {
                    var title = getString(R.string.categoria_para_ss)
                    title += Str.plural(
                        it.count(),
                        single = getString(R.string.image),
                        multiple = { i -> "$i "+getString(R.string.images) }
                    )
                    DialogStrPicker.show(fragmentManager, title, "category")
                }
            }
    }

    override fun dialogStrPickerGetData(identifier: String): Promise<List<String>>? {
        return promiseIt(Repo.inspiration.getCategories(App.modelUser))
    }

    override fun dialogStrPickerOnSelected(identifier: String, s: String) {
        if (identifier=="category") {
            mListSelected?.let { list ->
                save(s, list)
            }
        }
    }

    //
    override fun dialogAdOnCancel() {
        add()
    }
}