package br.com.adonio.keepcloset.models

import androidx.room.Dao
import androidx.room.Query

@Dao
abstract class InspirationDao: BaseDao<Inspiration>() {

    @Query("select * from inspirations where id = :id")
    abstract override fun find(id: Long): Inspiration?

    @Query("select * from inspirations where user_id = :userId order by category, id desc")
    abstract fun getAll(userId: Long): List<Inspiration>

    @Query("select * from inspirations where user_id = :userId and coalesce(category, 'null') in (:categories) order by category, id desc")
    abstract fun getForCategories(userId: Long, categories: List<String>): List<Inspiration>

    @Query("select DISTINCT category from inspirations where user_id = :userId AND category is not null order by category")
    abstract fun getCategories(userId: Long): List<String>

    @Query("select distinct category from inspirations where user_id = :userId order by category")
    abstract fun getCategoriesWithNull(userId: Long): List<String?>

}