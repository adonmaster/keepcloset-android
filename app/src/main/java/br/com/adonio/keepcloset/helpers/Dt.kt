package br.com.adonio.keepcloset.helpers

import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.temporal.ChronoUnit
import kotlin.math.abs

object Dt {

    fun fromPattern(s: String?, pattern: String="yyyy-MM-dd HH:mm", containsOnlyDate: Boolean=false): LocalDateTime? {
        val ss = guard(s) { return null }
        val f = DateTimeFormatter.ofPattern(pattern)
        return if (containsOnlyDate)
            LocalDate.parse(ss, f).atStartOfDay()
        else
            LocalDateTime.parse(ss, f)
    }

    fun fromJson(s: String?) = fromPattern(s, "yyyy-MM-dd HH:mm:ss") // 2019-08-26 16:37:15

    fun format(date: LocalDate?, pattern: String = "dd/MM/yyyy"): String? {
        val d = guard(date) { return null }
        return d.format(DateTimeFormatter.ofPattern(pattern))
    }


    fun format(dateTime: LocalDateTime?, pattern: String = "dd/MM/yyyy HH:mm"): String? {
        val d = guard(dateTime) { return null }
        return d.format(DateTimeFormatter.ofPattern(pattern))
    }

    fun formatToJson(dt: LocalDateTime?): String? {
        return format(dt, "yyyy-MM-dd HH:mm:ss")
    }

    fun fromSql(s: String?): LocalDate? {
        return fromPattern(s, "yyyy-MM-dd", true)?.toLocalDate()
    }

    fun toSql(dt: LocalDate?): String? {
        return format(dt, "yyyy-MM-dd")
    }

    fun fromNow(d: LocalDate?): String? {
        if (d==null) return null

        val now = LocalDate.now()
        val diffInDays = ChronoUnit.DAYS.between(now, d).toInt()
        val diffInMonthsPos = abs(ChronoUnit.MONTHS.between(now, d).toInt())
        val diffInYearsPos = abs(ChronoUnit.YEARS.between(now, d).toInt())

        return if (abs(diffInDays) <= 2) {
            when(diffInDays) {
                -2 -> "anteontem"
                -1 -> "ontem"
                1 -> "amanhã"
                2 -> "depois de amanhã"
                else -> "hoje"
            }
        }
        else
        {
            val suffix = when {
                diffInYearsPos > 0 -> notZeroPlural(diffInYearsPos, "um ano") { "$it anos" }
                diffInMonthsPos > 0 -> notZeroPlural(diffInMonthsPos, "um mês") { "$it meses" }
                else -> "${abs(diffInDays)} dias"
            }

            if (diffInDays > 0) "em $suffix" else "há $suffix"
        }
    }

    private fun notZeroPlural(i: Int, oneOrZero: String, more: (Int)->String): String {
        if (i <= 1) return oneOrZero
        return more(i)
    }

}