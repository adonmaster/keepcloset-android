package br.com.adonio.keepcloset.services

import androidx.fragment.app.FragmentManager
import br.com.adonio.keepcloset.dialogs.DialogAd
import br.com.adonio.keepcloset.helpers.Http
import br.com.adonio.keepcloset.helpers.ter
import br.com.adonio.keepcloset.repos.Repo
import br.com.adonio.keepcloset.rmodels.AdRModel
import org.threeten.bp.LocalDateTime
import org.threeten.bp.temporal.ChronoUnit

object AdService {

    private var shouldAd = true;
    private var ads = listOf<AdRModel>()
    private var lastRequested = LocalDateTime.now()

    fun loadAds() {
        if (!shouldAd) return
        val minsPassed = lastRequested.until(LocalDateTime.now(), ChronoUnit.MINUTES)
        if (ads.count() > 0 && minsPassed < 30) return

        Http.getArray("https://keepcloset.com.br/mobile/generics?parent=ads")
            .then {
                ads = AdRModel.parse(it)
                lastRequested = LocalDateTime.now()
            }
            .catch {
                // T.error(activity, it)
            }
    }

    fun loadPreferences() {
        shouldAd = Repo.pref.get("app@should_ad") != "false"
    }

    fun savePreferences(shouldAdPurchase: Boolean) {
        Repo.pref.put("app@should_ad", ter(shouldAdPurchase, "true", "false"))
        this.shouldAd = shouldAdPurchase
    }

    fun shouldAd(): Boolean {
        return shouldAd
    }

    fun shouldShowAd(): Boolean {
        return shouldAd && ads.count() > 0 && Math.random() < .7
    }

    fun showAdDialog(fm: FragmentManager) {
        if (ads.count() > 0) {
            DialogAd.show(fm, ads.random())
        }
    }

}