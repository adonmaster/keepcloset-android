package br.com.adonio.keepcloset.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.adapters.DialogItemColorsAdapter
import kotlinx.android.synthetic.main.dialog_item_colors.*
import kotlin.properties.Delegates

class DialogItemColors: DialogBase() {

    // static
    companion object {
        fun show(fragmentManager: FragmentManager?) {
            if (fragmentManager==null) return
            DialogItemColors().show(fragmentManager, "DialogItemColors")
        }
        private var listener: Listener? = null
        fun attach(listener: Listener) {
            this.listener = listener
        }
        fun detach() {
            this.listener = null
        }
    }

    // state
    private var color1 by Delegates.observable<Pair<Long,String>?>(null) { _, _, v ->
        ui_view_primary?.setBackgroundColor(v?.first?.toInt() ?: 0)
    }

    private var color2 by Delegates.observable<Pair<Long,String>?>(null) { _, _, v ->
        ui_view_secondary?.setBackgroundColor(v?.first?.toInt() ?: 0)
    }

    // methods
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.setTitle("Cores")
        return inflater.inflate(R.layout.dialog_item_colors, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // rv
        ui_rv.adapter = DialogItemColorsAdapter(object : DialogItemColorsAdapter.Listener {
            override fun onSelect(color: Pair<Long, String>) {
                onSelectColor(color)
            }
        })
        ui_rv.layoutManager = GridLayoutManager(context, 7)

        // actions
        ui_btn_cancel.setOnClickListener { dismissAllowingStateLoss() }
        ui_btn_submit.setOnClickListener { submit() }
    }

    private fun submit() {
        if (listOfNotNull(color1, color2).count() == 0) return

        listener?.dialogItemColorsOnSelect(color1, color2)
        dismissAllowingStateLoss()
    }

    private fun onSelectColor(color: Pair<Long, String>) {
        if (ui_rb_primary.isChecked) {
            ui_rb_secondary.isChecked = true
            color1 = color
        } else if (ui_rb_secondary.isChecked) {
            color2 = color
            ui_rb_primary.isChecked = true
        }
    }

    // listener
    interface Listener {
        fun dialogItemColorsOnSelect(color1: Pair<Long,String>?, color2: Pair<Long,String>?)
    }

}