package br.com.adonio.keepcloset.dialogs.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Act
import br.com.adonio.keepcloset.structs.Therm
import kotlinx.android.synthetic.main.rv_dialog_item_therm.view.*

class DialogItemThermAdapter(private val listener: Listener): RecyclerView.Adapter<DialogItemThermAdapter.VH>() {

    private val mData by lazy { Therm.list() }

    override fun getItemCount(): Int {
        return mData.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(R.layout.rv_dialog_item_therm, parent, false)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = mData[position]
        holder.uiLbl?.text = m.toString()
        holder.uiLblLeftIcon?.text = m.faIconColored(holder.itemView.context)
    }

    // view holder
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiLbl: TextView? = itemView.ui_lbl
        val uiLblLeftIcon: TextView? = itemView.ui_lbl_left_icon

        init {
            itemView.setOnClickListener {
                listener.onSelect(mData[adapterPosition])
            }
        }
    }

    // listener
    interface Listener {
        fun onSelect(therm: Therm)
    }

}