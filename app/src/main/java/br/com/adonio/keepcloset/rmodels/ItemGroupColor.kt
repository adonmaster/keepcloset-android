package br.com.adonio.keepcloset.rmodels

import androidx.annotation.ColorRes
import androidx.room.Ignore

data class ItemGroupColor(
    val desc: String,
    val count: Int,
    @ColorRes val colorId: Int
)