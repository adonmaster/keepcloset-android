package br.com.adonio.keepcloset.models.presenters

import android.widget.ImageView
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.models.User
import java.io.File

class UserPresenter(model: User) : BasePresenter<User>(model) {

    val name: String
        get() = model.name ?: "[Desconhecido]"

    fun loadAvatar(imageView: ImageView?) {
        val s = model.avatar
        if (s==null) {
            Img.load(R.drawable.img_guy, imageView)
        } else {

            val f = File(s)
            if (f.exists()) {
                Img.load(f, imageView)
            }
            else {
                Img.load(s, imageView)
            }
        }
    }

}