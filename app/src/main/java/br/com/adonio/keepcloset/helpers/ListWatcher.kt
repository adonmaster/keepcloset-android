package br.com.adonio.keepcloset.helpers

class ListWatcher<T>(val data: List<T>?=null) {

    private var cache = data?.toMutableList() ?: mutableListOf()
    private var observer: ((ListWatcher<T>, Int?, T?)->Unit)? = null

    fun observe(o: (ListWatcher<T>, Int?, T?)->Unit): ListWatcher<T> {
        observer = o
        return this
    }

    operator fun get(i: Int) = cache[i]

    fun add(el: T) {
        cache.add(el)
        this.observer?.invoke(this, cache.size-1, el)
    }

    fun removeAt(index: Int): T {
        return cache.removeAt(index)
            .also {
                this.observer?.invoke(this, index, it)
            }
    }

    fun count() = cache.size
    fun isEmpty() = cache.size == 0
    fun contains(t: T) = cache.contains(t)
    fun indexOf(t: T) = cache.indexOf(t)
    fun firstIndex(t: T) = cache.indexOf(t).takeIf { it >= 0 }

    fun put(list: List<T>): ListWatcher<T> {
        cache = list.toMutableList()
        this.observer?.invoke(this, null, null)
        return this
    }

    fun clear() {
        cache.clear()
        this.observer?.invoke(this, null, null)
    }

}