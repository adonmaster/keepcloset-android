package br.com.adonio.keepcloset.repos

import android.graphics.Bitmap
import android.view.View
import br.com.adonio.keepcloset.db.DB
import br.com.adonio.keepcloset.helpers.F
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.helpers.Promise
import br.com.adonio.keepcloset.helpers.Str
import br.com.adonio.keepcloset.models.*
import java.io.File
import java.util.ArrayList

@Suppress("unused")
class LookRepo {

    // state
    private val dao by lazy { DB.i.look }

    // methods
    fun getAll(userId: Long) = dao.getAll(userId)

    fun getCategories(user: User?): List<String> {
        return dao.getCategories(user?.id ?: 0)
    }

    fun getForCategories(userId: Long, list: List<String>): List<Look> {
        return dao.getForCategories(userId, list)
    }

    fun getCategoriesWithNull(user: User?): List<String?> {
        return dao.getCategoriesWithNull(user?.id ?: 0)
    }

    data class SuItem(val id: Long, val sourceType: String, val sourceId: Long, val path: String)
    fun save(model: Look, bitmap: Bitmap, items: ArrayList<Pair<SuItem, View>>, category: String?, bknd: Int) = Promise<Look> { resolve, _ ->
        model.category = category
        model.bknd_index = bknd

        // image
        val full = F.saveInternalSynced("looks", Str.random(24), bitmap)
        model.path = full.path

        // thumb
        val bmpThumb = Img.thumbFrom(bitmap)
        val thumb = F.saveInternalSynced("looks/thumb", Str.random(24), bmpThumb)
        model.path_thumb = thumb.path

        // save look
        val lookModel = saveModel(model)

        saveItems(lookModel, items)

        // done
        resolve(lookModel)
    }

    private fun saveItems(look: Look, items: ArrayList<Pair<SuItem, View>>)
    {
        // getting all paths to delete
        val dbItemsFiles = dao.getItemsFor(look.id).map { it.path }.toMutableList()

        // sync: remove only db record, maintaining file, I'll cleanup later
        removeAllItemsFor(look.id, false)

        // sync save
        for ((index, pair) in items.withIndex()) {
            val suItem = pair.first
            val model = LookItem()

            // try to reuse the file
            if (suItem.id <= 0) {
                val f = F.copyInternal(File(pair.first.path), "looks/items", Str.random(24))
                model.path = f.path
            } else {
                model.path = suItem.path
                dbItemsFiles.remove(suItem.path)
            }

            model.look_id = look.id
            model.source_type = pair.first.sourceType
            model.source_id = pair.first.sourceId
            model.pos_x = pair.second.x
            model.pos_y = pair.second.y
            model.scale = pair.second.scaleX
            model.rotation = pair.second.rotation
            model.position = index

            // save db
            dao.insertItem(model)
        }

        // cleanup
        dbItemsFiles.forEach { File(it).delete() }

    }

    fun destroy(lookId: Long) {
        dao.delete(lookId)
        removeAllItemsFor(lookId, true)
    }

    private fun removeAllItemsFor(lookId: Long, deleteFile: Boolean) {
        val items = dao.getItemsFor(lookId)
        if (deleteFile) {
            for (item in items) {
                File(item.path).delete()
            }
        }
        dao.removeItemsFor(lookId)
    }

    private fun removeItem(item: LookItem, deleteFile: Boolean) {
        if (deleteFile) File(item.path).delete()
        dao.removeItem(item.id)
    }

    private fun saveModel(model: Look): Look {
        return if (model.id > 0) {
            dao.update(model)
            model
        } else {
            val id = dao.insert(model)
            dao.find(id)!!
        }
    }

    fun getItemsFor(lookId: Long): List<LookItem> {
        return dao.getItemsFor(lookId)
    }

    fun count(userId: Long): Int {
        return dao.count(userId)
    }


}