package br.com.adonio.keepcloset.repos

object Repo {

    val user by lazy { UserRepo() }
    val item by lazy { ItemRepo() }
    val look by lazy { LookRepo() }
    val inspiration by lazy { InspirationRepo() }
    val schedule by lazy { ScheduleRepo() }
    val luggage by lazy { LuggageRepo() }
    val pref by lazy { PrefRepo() }

}