package br.com.adonio.keepcloset

import android.app.Application
import br.com.adonio.keepcloset.db.DB
import br.com.adonio.keepcloset.models.User
import br.com.adonio.keepcloset.repos.Repo
import br.com.adonio.keepcloset.services.AdService
import com.androidnetworking.AndroidNetworking
import com.jakewharton.threetenabp.AndroidThreeTen
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

class App: Application() {

    companion object {

        fun modelUserReload() {
            shared.loadActiveUser()
        }

        lateinit var shared: App

        val modelUser: User?
            get() = shared.model.user
    }

    lateinit var model: AppViewModel

    override fun onCreate() {
        super.onCreate()

        shared = this
        model = AppViewModel()

        configDateTime()
        configHttp()
        configDb(false)
        loadActiveUser()
        AdService.loadPreferences()
    }

    private fun configDateTime() {
        AndroidThreeTen.init(this)
    }

    private fun configHttp() {
        val config = OkHttpClient.Builder()
            .addInterceptor { chain ->
                chain.proceed(
                    chain.request()!!.newBuilder()
                        .addHeader("Accept", "application/json")
                        .also {
                            if (model.token.isEmpty()) return@also
                            it.addHeader("Authorization","Bearer ${model.token}")
                        }
                        .build()
                )
            }
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()

        AndroidNetworking.initialize(applicationContext, config)
        AndroidNetworking.enableLogging()
    }

    private fun configDb(deleteDatabase: Boolean)
    {
        deleteDatabase && deleteDatabase("main.room")

        DB.config(this)
    }

    private fun loadActiveUser() {
        model.user = Repo.user.findActive()
    }

}