package br.com.adonio.keepcloset.frags.main.closet

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.Cut10Activity
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.DialogItemColors
import br.com.adonio.keepcloset.dialogs.DialogItemTherm
import br.com.adonio.keepcloset.dialogs.DialogOk
import br.com.adonio.keepcloset.dialogs.DialogStrPicker
import br.com.adonio.keepcloset.extensions.setVisible
import br.com.adonio.keepcloset.helpers.Promise
import br.com.adonio.keepcloset.helpers.Str
import br.com.adonio.keepcloset.helpers.guard
import br.com.adonio.keepcloset.helpers.promiseIt
import br.com.adonio.keepcloset.models.Item
import br.com.adonio.keepcloset.repos.Repo
import br.com.adonio.keepcloset.structs.Therm
import kotlinx.android.synthetic.main.frag_main_closet_edit.*

class FragMainClosetEdit: Fragment(), DialogStrPicker.Listener, DialogItemTherm.Listener, DialogItemColors.Listener {

    //
    private lateinit var vm: FragMainClosetEditVM
    private lateinit var vmParent: FragMainClosetVM

    //
    private val cModel: Item
        get() = vmParent.modelEditing.value!!

    //
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.frag_main_closet_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.vmParent = ViewModelProviders.of(activity!!).get(FragMainClosetVM::class.java)
        this.vm = ViewModelProviders.of(this).get(FragMainClosetEditVM::class.java)

        // img
        cModel.p().loadThumb(ui_img, true)

        //
        ui_btn_cut10.setOnClickListener {
            Cut10Activity.start(this, cModel.path)
        }

        // progress
        vm.loading.observe(viewLifecycleOwner, Observer {
            ui_progress.setVisible(it)
        })

        // category
        this.vm.category.observe(viewLifecycleOwner, Observer {
            ui_lbl_category.text = Str.colorEmpty(context!!, it, getString(R.string.escolha_dots))
            cModel.category = it
        })
        this.vm.category.value = cModel.category
        ui_wrapper_category.setOnClickListener {
            DialogStrPicker.show(fragmentManager, getString(R.string.ccategoria), "category")
        }

        // colors
        val colorObserver = Observer<Pair<Long, String>> {
            cModel.color1 = vm.color1.value?.first ?: 0
            cModel.color1_desc = vm.color1.value?.second
            cModel.color2 = vm.color2.value?.first ?: 0
            cModel.color2_desc = vm.color2.value?.second

            ui_lbl_colors.text = cModel.p().fullColors(context!!, getString(R.string.escolha_dots))
        }
        vm.color1.observe(viewLifecycleOwner, colorObserver)
        vm.color2.observe(viewLifecycleOwner, colorObserver)
        vm.color1.value = cModel.color1_desc?.let { Pair(cModel.color1, it) }
        vm.color2.value = cModel.color2_desc?.let { Pair(cModel.color2, it) }
        ui_wrapper_colors.setOnClickListener {
            DialogItemColors.show(fragmentManager)
        }

        // brand
        ui_lbl_brand.text = Str.colorEmpty(context!!, null, getString(R.string.escolha_dots))
        this.vm.brand.observe(viewLifecycleOwner, Observer {
            cModel.brand = it
            ui_lbl_brand.text = cModel.p().fullBrand(context!!, getString(R.string.escolha_dots))
        })
        this.vm.brand.value = cModel.brand
        ui_wrapper_brand.setOnClickListener {
            DialogStrPicker.show(fragmentManager, getString(R.string.cmarca), "brand")
        }

        // therm
        this.vm.therm.observe(viewLifecycleOwner, Observer {
            cModel.therm_id = it
            ui_lbl_therm.text = cModel.p().fullTherm(context!!, getString(R.string.escolha_dots))
        })
        vm.therm.value = cModel.therm_id
        ui_wrapper_therm.setOnClickListener {
            DialogItemTherm.show(fragmentManager)
        }

        // status
        this.vm.status.observe(viewLifecycleOwner, Observer {
            cModel.status = it
            ui_lbl_status.text = cModel.p().fullStatus(context!!, getString(R.string.escolha_dots))
        })
        this.vm.status.value = cModel.status
        ui_wrapper_status.setOnClickListener {
            DialogStrPicker.show(fragmentManager, getString(R.string.cstatus), "status")
        }

    }

    override fun dialogItemThermOnSelect(therm: Therm) {
        vm.therm.value = therm.v()
    }

    override fun dialogItemColorsOnSelect(color1: Pair<Long, String>?, color2: Pair<Long, String>?) {
        vm.color1.value = color1
        vm.color2.value = color2
    }

    override fun dialogStrPickerGetData(identifier: String): Promise<List<String>>? {
        when (identifier) {
            "category" -> return promiseIt(Repo.item.getCategories(App.modelUser))
            "status" -> return promiseIt(Repo.item.getStatuses(App.modelUser))
            "brand" -> return promiseIt(Repo.item.getBrands(App.modelUser))
        }
        return null
    }

    override fun dialogStrPickerOnSelected(identifier: String, s: String) {
        when (identifier) {
            "category" -> vm.category.value = s
            "brand" -> vm.brand.value = s
            "status" -> vm.status.value = s
        }
    }

    private fun submit() {
        vm.loading.value = true
        Repo.item.save(cModel)
            .then {
                finish()
            }
            .catch { DialogOk.show(context, it, getString(R.string.atencao)) }
            .finally { vm.loading.value = false }
    }

    private fun finish() {
        findNavController().popBackStack()
    }

    private fun destroy() {
        val modelId = guard(cModel.takeIf { !it.isNew() }?.id) { return }

        DialogOk.show(context,
            getString(R.string.quer_mesmo_apagar_esse_registro),
            getString(R.string.atencao),
            getString(R.string.cancelar)
        ) {
            this.reallyDestroy(modelId)
        }
    }

    private fun reallyDestroy(modelId: Long) {
        Repo.item.destroy(modelId)
        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(if (cModel.isNew()) R.menu.menu_frag_new else R.menu.menu_frag_edit, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        item?.itemId?.let { id ->
            when (id) {
                R.id.menu_action_done -> {
                    submit()
                }
                R.id.menu_action_delete -> {
                    destroy()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        DialogStrPicker.attach(this)
        DialogItemTherm.attach(this)
        DialogItemColors.attach(this)
    }

    override fun onStop() {
        super.onStop()
        DialogStrPicker.detach()
        DialogItemTherm.detach()
        DialogItemColors.detach()
    }

    //

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // cut10
        Cut10Activity.onActivityResult(requestCode, resultCode, data) { path ->
            cModel.path = path
            cModel.path_thumb = path

            cModel.p().loadThumb(ui_img, true)
        }
    }
}