package br.com.adonio.keepcloset.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import br.com.adonio.keepcloset.R
import kotlinx.android.synthetic.main.dialog_query.*

class DialogQuery: DialogBase() {

    // static
    companion object {
        fun show(fragmentManager: FragmentManager?, title: String, ss: String, identifier: String="") {
            if (fragmentManager==null) return

            val dialog = DialogQuery()
            val bundle = Bundle()
            bundle.putString("identifier", identifier)
            bundle.putString("ss", ss)
            bundle.putString("title", title)
            dialog.arguments = bundle

            dialog.show(fragmentManager, "DialogQuery")
        }

        private var listener: Listener? = null

        fun attach(listener: Listener) {
            this.listener = listener
        }
        fun detach() {
            this.listener = null
        }
    }

    // computed
    private val cSs
        get() = arguments!!.getString("ss")!!

    private val cIdentifier
        get() = arguments!!.getString("identifier")!!

    private val cTitle
        get() = arguments!!.getString("title")!!

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.setTitle(cTitle)
        return inflater.inflate(R.layout.dialog_query, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        ui_ed.setText(cSs)

        //
        ui_btn_cancel.setOnClickListener { dismissAllowingStateLoss() }
        ui_btn_submit.setOnClickListener {
            dismissAllowingStateLoss()
            val s = ui_ed.text.toString()
            if (s.isNotBlank()) {
                listener?.dialogQueryOnSubmit(cIdentifier, s.trim())
            }
        }

    }


    // listener
    interface Listener {
        fun dialogQueryOnSubmit(identifier: String, v: String)
    }
}