package br.com.adonio.keepcloset.frags.main.schedule

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.DialogItemColors
import br.com.adonio.keepcloset.dialogs.DialogList
import br.com.adonio.keepcloset.dialogs.DialogPicPath
import br.com.adonio.keepcloset.dialogs.adapters.DialogPicPathAdapter
import br.com.adonio.keepcloset.extensions.setVisible
import br.com.adonio.keepcloset.frags.main.schedule.adapters.FragScheduleDayAdapter
import br.com.adonio.keepcloset.helpers.*
import br.com.adonio.keepcloset.models.ScheduleItem
import br.com.adonio.keepcloset.repos.Repo
import kotlinx.android.synthetic.main.frag_schedule_day.*
import org.threeten.bp.LocalDate

class FragScheduleDay: Fragment(), DialogPicPath.Listener, DialogList.Listener,
    DialogItemColors.Listener {

    // static
    companion object {
        fun navigate(from: Fragment, @IdRes to: Int, dt: LocalDate) {
            val bundle = Bundle()
            bundle.putString("dt", Dt.toSql(dt))
            from.findNavController().navigate(to, bundle)
        }
    }

    // state
    private lateinit var mAdapter: FragScheduleDayAdapter

    // computed
    val cDt get() = Dt.fromSql(arguments!!.getString("dt"))!!

    // methods
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_schedule_day, container, false)
    }

    private var mLastSelected: ScheduleItem? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        updateDesktopHeader()

        mAdapter = FragScheduleDayAdapter(cDt, object : FragScheduleDayAdapter.Listener {
            override fun onSelect(item: ScheduleItem) {
                mLastSelected = item
                DialogList.show(context!!,
                    getString(R.string.o_quer_fazer_q),
                    listOf(getString(R.string.escolher_cor), getString(R.string.apagar)))
            }
            override fun onEmpty(option: Boolean) {
                ui_empty.setVisible(option)
            }
        })
        ui_rv.adapter = mAdapter
        ui_rv.layoutManager = GridLayoutManager(context, 3)
        ui_rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_slide_right)

        ui_btn_add.setOnClickListener { add() }
    }

    @SuppressLint("DefaultLocale")
    private fun updateDesktopHeader()
    {
        val ctx = context!!
        val month = Dt.format(cDt, "MMMM")!!.toUpperCase()
        val year = Dt.format(cDt, "yyyy")!!
        val date = Dt.format(cDt, "dd")!!
        val dayOfWeek = Dt.format(cDt, "EEEE")!!.toUpperCase()

        ui_lbl_header.text = Span(month)
            .concat(" $year").color(ctx, R.color.md_grey_500)
            .concat("\n$date ")
            .concat(dayOfWeek).color(ctx, R.color.md_pink_700)
            .build()
    }

    private fun add() {
        DialogPicPath.show(fragmentManager)
    }

    override fun dialogPicPathItem(item: DialogPicPathAdapter.Item) {
        val userId = guard(App.modelUser?.id) { return }
        val model = Repo.schedule.create(userId, cDt, item.thumb, item.full, item.item_id, item.look_id)

        mAdapter.insertModelAtBeginning(model)
    }

    override fun dialogPicPathPickAlbum() {
        ImgProvider.pick(this, 1)
    }

    override fun dialogPicPathPickCamera() {
        ImgProvider.pick(this, 1)
    }

    override fun dialogPicPathText() {}

    override fun dialogListOnChoose(s: String, position: Int) {
        when (position) {
            0 -> {
                DialogItemColors.show(fragmentManager)
            }
            1 -> {
                Repo.schedule.destroy(mLastSelected?.id)
                mAdapter.removeWithId(mLastSelected?.id)
            }
        }
    }

    override fun dialogItemColorsOnSelect(color1: Pair<Long, String>?, color2: Pair<Long, String>?)
    {
        mLastSelected?.color1 = color1?.first ?: 0
        mLastSelected?.color1_desc = color1?.second

        mLastSelected?.color2 = color2?.first ?: 0
        mLastSelected?.color2_desc = color2?.second

        mAdapter.updateByModel(mLastSelected)

        // update repo
        val scheduleItemId = guard(mLastSelected?.id) { return }
        Repo.schedule.updateColor(
            scheduleItemId,
            color1?.first ?: 0,
            color1?.second,
            color2?.first ?: 0,
            color2?.second
        )
    }

    // ui
    override fun onStart() {
        super.onStart()
        DialogPicPath.attach(this)
        DialogList.attach(this)
        DialogItemColors.attach(this)
    }

    override fun onStop() {
        super.onStop()
        DialogPicPath.detach()
        DialogList.detach()
        DialogItemColors.detach()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val path = ImgProvider.onActivityResultFirst(requestCode, resultCode, data)?.path
            ?: return

        val userId = guard(App.modelUser?.id) { return }
        Task.main {

            val bmp = BitmapFactory.decodeFile(path)
            val thumbFile = Img.thumbFrom(bmp)
            val f = F.saveExternalSynced("", "keepCloset_schedule_item_thumb.png", thumbFile)

            val model = Repo.schedule.create(userId, cDt, f.path, path, 0, 0)

            mAdapter.insertModelAtBeginning(model)

        }

    }

}