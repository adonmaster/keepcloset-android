package br.com.adonio.keepcloset.helpers

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import br.com.adonio.keepcloset.App
import java.io.File

object Act {

    fun replace(manager: FragmentManager, @IdRes frag_container: Int, fragment: Fragment, backStack: String?=null): Int
    {
        val transaction = manager.beginTransaction()
            .replace(frag_container, fragment)
        if (backStack != null) transaction.addToBackStack(backStack)
        return transaction.commitAllowingStateLoss()
    }

    fun finish(activity: Activity, withResult: Int) {
        activity.setResult(withResult)
        activity.finish()
    }

    fun inflate(@LayoutRes layoutId: Int, parent: ViewGroup, attachToRoot: Boolean=false): View? {
        return LayoutInflater.from(parent.context).inflate(layoutId, parent, attachToRoot)
    }

    fun openLink(context: Context?, url: String) {
        context?.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    fun openInstagram(context: Context, username: String)
    {
        val uri = "http://instagram.com/_u/$username"
        val instaIntent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        instaIntent.`package` = "com.instagram.android"

        if (isIntentAvailable(context, instaIntent)) {
            context.startActivity(instaIntent)
        } else {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(uri)))
        }
    }

    private fun isIntentAvailable(context: Context, intent: Intent): Boolean {
        val list = context.packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
        return list.size > 0
    }

    fun shareImage(activity: Activity, imageFile: File) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.type = "image/*"
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imageFile))
        activity.startActivity(Intent.createChooser(intent, "Compartilhar usando..."))
    }

    fun setTitle(activity: Activity?, title: String?) {
        val act = activity as? AppCompatActivity
        val sab = act?.supportActionBar
        sab?.title = title
    }

    fun hideKeyboard(frag: Fragment?) {
        hideKeyboard(frag?.context, frag?.view)
    }

    fun hideKeyboard(activity: Activity?) {
        hideKeyboard(activity, activity?.currentFocus)
    }

    fun hideKeyboard(context: Context?, view: View?) {
        val ctx = context ?: return
        val v = view ?: return
        val inputMethodManager = ctx.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(v.windowToken, 0)
    }

    fun versionName(): String {
        val info = App.shared.packageManager.getPackageInfo(App.shared.packageName, 0)
        return info.versionName
    }

}