package br.com.adonio.keepcloset.helpers

class ResultEx<V> private constructor(
    private var wasSuccessful: Boolean,
    private var payload: V?,
    private var error: String?
) {

    companion object {
        fun <V> success(payload: V): ResultEx<V> {
            return ResultEx(true, payload, null)
        }
        fun <V> failure(reason: String): ResultEx<V> {
            return ResultEx(false, null, reason)
        }
    }

    fun onError(cb: (String)->Unit): ResultEx<V> {
        if (!wasSuccessful) cb(error!!)
        return this
    }

    fun onSuccess(cb: (V)->Unit): ResultEx<V> {
        if (wasSuccessful) cb(payload!!)
        return this
    }

    fun value(): V? = payload

}