package br.com.adonio.keepcloset.frags.main.schedule.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.annotation.LayoutRes

abstract class MyBaseAdapter<V: MyBaseAdapter.ViewHolder>(private val context: Context): BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val v: View
        val vh: V
        if (convertView == null) {
            val inflater = LayoutInflater.from(context)
            v = inflater.inflate(getCellLayoutId(), parent, false)
            vh = createViewHolder(v)
            v.tag = vh
        } else {
            v = convertView
            @Suppress("UNCHECKED_CAST")
            vh = v.tag as V
        }

        bindViewHolder(vh, position)
        return v
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @LayoutRes
    abstract fun getCellLayoutId(): Int

    abstract fun createViewHolder(itemView: View): V
    abstract fun bindViewHolder(vh: V, position: Int)

    abstract class ViewHolder(@Suppress("unused") val itemView: View)
}