package br.com.adonio.keepcloset.models

import androidx.room.Entity
import org.threeten.bp.LocalDate

@Entity(tableName = "schedule_item")
class ScheduleItem: BaseModel() {

    companion object {
        fun f(dt: LocalDate, path: String, path_thumb: String): ScheduleItem {
            return ScheduleItem().also {
                it.dt = dt
                it.path = path
                it.path_thumb = path_thumb
            }
        }
    }

    lateinit var dt: LocalDate

    var user_id: Long = 0

    var item_id: Long = 0
    var look_id: Long = 0

    var color1: Long = 0
    var color1_desc: String? = null

    var color2: Long = 0
    var color2_desc: String? = null

    lateinit var path: String
    lateinit var path_thumb: String

}