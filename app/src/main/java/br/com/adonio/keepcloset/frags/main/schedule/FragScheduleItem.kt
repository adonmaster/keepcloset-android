package br.com.adonio.keepcloset.frags.main.schedule

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.frags.main.schedule.adapters.FragScheduleItemAdapter
import kotlinx.android.synthetic.main.frag_schedule_item.*
import org.threeten.bp.LocalDate

class FragScheduleItem: Fragment() {

    // static
    companion object {
        fun f(position: Int): FragScheduleItem {
            val bundle = Bundle()
            bundle.putInt("position", position)
            val v = FragScheduleItem()
            v.arguments = bundle
            return v
        }
    }

    // state
    private lateinit var vm: FragMainScheduleVM

    // computed
    private val cDt by lazy {
        val p = arguments!!.getInt("position")
        vm.positionToDt(p)
    }

    // methods
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_schedule_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(activity!!).get(FragMainScheduleVM::class.java)

        ui_grid_month.adapter = FragScheduleItemAdapter(context!!, cDt, object: FragScheduleItemAdapter.Listener {
            override fun onSelect(dt: LocalDate) {
                FragScheduleDay.navigate(this@FragScheduleItem, R.id.sg_to_frag_schedule_day, dt)
            }
        })
    }

}