package br.com.adonio.keepcloset.frags.support

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import br.com.adonio.keepcloset.MainActivity
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Http
import br.com.adonio.keepcloset.helpers.Rv
import br.com.adonio.keepcloset.helpers.T
import br.com.adonio.keepcloset.rmodels.SupportRModel
import kotlinx.android.synthetic.main.frag_support.*
import kotlinx.android.synthetic.main.rv_frag_support.view.*

class FragSupport: Fragment(), Rv.Owner {

    //
    private val mLoading = MutableLiveData<Boolean>().also { it.value = false }
    private lateinit var mAdapter: Rv

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_support, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        mLoading.observe(this, Observer {
            ui_progress.isVisible = it
        })

        //
        mAdapter = Rv(this, R.layout.rv_frag_support)
        ui_rv.adapter = mAdapter
        Rv.setupForLinear(ui_rv, context)

        //
        request()
    }

    private fun request() {
        if (mLoading.value != false) return

        mLoading.value = true
        Http.get("http://keepcloset.com.br/mobile/support")
            .then {

                val list = SupportRModel.parse(it)
                mAdapter.updateData(list)

            }
            .catch {
                T.error(activity, it)
            }
            .finally {
                mLoading.value = false
            }
    }

    //

    override fun rvCreate(
        identifier: String,
        itemView: View,
        r: () -> Rv.Value,
        position: () -> Int
    ) {
        itemView.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("url", r().getValue<SupportRModel>().url)
            findNavController().navigate(R.id.sg_to_support_detail, bundle)
        }
    }

    override fun rvBind(identifier: String, v: View, h: Rv.VH, r: Rv.Value, position: Int) {
        r.whenValue<SupportRModel> {

            v.ui_title.text = it.title ?: getString(R.string.b_desconhecido_b)
            v.ui_desc.text = it.desc ?: getString(R.string.d_desconhecido_d)

        }
    }

    override fun rvId(identifier: String, r: Rv.Value, position: Int): Int? {
        return null
    }

}