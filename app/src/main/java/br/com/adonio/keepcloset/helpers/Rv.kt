package br.com.adonio.keepcloset.helpers

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.R
import java.lang.ref.WeakReference

class Rv(
    _owner: Owner,
    @LayoutRes private val itemId: Int,
    private val identifier: String = ""
):
    RecyclerView.Adapter<Rv.VH>() {

    //
    @Suppress("unused")
    companion object {
        fun setupForLinearHorizontal(rv: RecyclerView, context: Context?) {
            rv.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_slide_right)
        }
        fun setupForLinear(rv: RecyclerView, context: Context?) {
            rv.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
        }
    }

    //
    private var cache = listOf<Any>()
    private val ownerInstance = WeakReference<Owner>(_owner)

    //
    private val owner
        get() = ownerInstance.get()!!


    //
    override fun getItemCount(): Int {
        return cache.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(itemId, parent, false)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = cache.getOrNull(position) ?: return
        owner.rvBind(identifier, holder.itemView, holder, Value(m), position)
    }

    override fun getItemId(position: Int): Long {
        val m = cache.getOrNull(position) ?: super.getItemId(position)
        return owner.rvId(identifier, Value(m), position)?.toLong() ?: super.getItemId(position)
    }

    //
    fun updateData(list: List<Any>) {
        cache = list
        notifyDataSetChanged()
    }

    //
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            owner.rvCreate(identifier, itemView, { Value(cache[adapterPosition]) }) { adapterPosition }
        }
    }

    // owner
    @Suppress("RemoveRedundantQualifierName")
    interface Owner {
        fun rvCreate(identifier: String, itemView: View, r: ()->Value, position: ()->Int)
        fun rvBind(identifier: String, v: View, h: Rv.VH, r: Value, position: Int)
        fun rvId(identifier: String, r: Value, position: Int): Int?
    }

    @Suppress("UNCHECKED_CAST")
    class Value(val v: Any) {
        inline fun <reified V> whenValue(cb: (V)->Unit): Value {
            if (v is V) cb(v)
            return this
        }
        fun <V> getValue(): V {
            return value()!!
        }
        fun <V> value(): V? {
            return v as? V
        }
    }

}