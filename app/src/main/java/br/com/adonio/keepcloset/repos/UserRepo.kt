package br.com.adonio.keepcloset.repos

import br.com.adonio.keepcloset.db.DB
import br.com.adonio.keepcloset.models.User
import br.com.adonio.keepcloset.models.UserDao

class UserRepo: BaseRepo<User, UserDao>(DB.i.user) {

    fun findActive(): User? {
        return dao.findActive()
    }

    fun registerAndActivate(email: String): User {
        val model = dao.findBy(email) ?: User()
        model.email = email
        model.active = true

        return save(model, true)
    }

    fun saveAvatar(userId: Long, path: String?) {
        val model = dao.find(userId) ?: return
        model.avatar = path
        model.avatar_thumb = path

        save(model)
    }

    fun saveName(userId: Long, text: String) {
        val model = dao.find(userId) ?: return
        model.name = text

        save(model)
    }
}