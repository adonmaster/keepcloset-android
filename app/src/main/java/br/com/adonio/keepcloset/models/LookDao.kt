package br.com.adonio.keepcloset.models

import androidx.room.*

@Dao
abstract class LookDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(model: Look): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract fun update(model: Look)

    @Query("delete from looks where id = :id")
    abstract fun delete(id: Long)

    @Query("select * from looks where id = :id LIMIT 1")
    abstract fun find(id: Long): Look?

    @Query("select * from looks where user_id = :userId")
    abstract fun getAll(userId: Long): List<Look>

    @Query("select * from looks where user_id = :userId and coalesce(category, 'null') in (:categories)")
    abstract fun getForCategories(userId: Long, categories: List<String>): List<Look>

    @Query("select DISTINCT category from looks where user_id = :userId AND category is not null")
    abstract fun getCategories(userId: Long): List<String>

    @Query("select distinct category from looks where user_id = :userId")
    abstract fun getCategoriesWithNull(userId: Long): List<String?>

    @Query("select count(*) as count from looks where user_id = :userId")
    abstract fun count(userId: Long): Int

    // items

    @Update(onConflict = OnConflictStrategy.REPLACE)
    abstract fun updateItem(model: LookItem)

    @Query("select * from look_item where look_id = :lookId order by position")
    abstract fun getItemsFor(lookId: Long): List<LookItem>

    @Query("delete from look_item where look_id = :lookId")
    abstract fun removeItemsFor(lookId: Long)

    @Insert
    abstract fun insertItem(lookItem: LookItem): Long

    @Query("delete from look_item where id = :lookItemId")
    abstract fun removeItem(lookItemId: Long)

}