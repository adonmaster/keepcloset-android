package br.com.adonio.keepcloset.extensions

import org.json.JSONObject

fun JSONObject.isNotNull(name: String): Boolean {
    return !isNull(name)
}