package br.com.adonio.keepcloset.frags.stats

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.helpers.Rv
import br.com.adonio.keepcloset.helpers.Task
import br.com.adonio.keepcloset.helpers.coalesce
import br.com.adonio.keepcloset.repos.Repo
import br.com.adonio.keepcloset.rmodels.ScheduleGroupPathRModel
import br.com.adonio.keepcloset.structs.ChartIntValueFormatter
import br.com.adonio.keepcloset.structs.Palette
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import kotlinx.android.synthetic.main.frag_stats.*
import kotlinx.android.synthetic.main.rv_frag_stats_calendar.view.*
import java.io.File

class FragStats: Fragment(), Rv.Owner {

    //
    private lateinit var mCalendarAdapter: Rv
    private val mLoading = MutableLiveData<Boolean>().also { it.value = false }

    //
    private val cUserId get() = App.modelUser?.id ?: 0

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_stats, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        mLoading.observe(this, Observer {
            ui_progress.isVisible = it
        })

        //
        mCalendarAdapter = Rv(this, R.layout.rv_frag_stats_calendar)
        ui_rv_calendar.adapter = mCalendarAdapter
        Rv.setupForLinearHorizontal(ui_rv_calendar, context)

        //
        requestData()
    }

    //
    @SuppressLint("DefaultLocale")
    private fun requestData()
    {
        if (mLoading.value != false) return
        mLoading.value = true
        Task.main(200) {

            // closet
            ui_closet_total.text = "${Repo.item.count(cUserId)}"

            // look
            ui_look_total.text = "${Repo.look.count(cUserId)}"

            // calendar
            val list = Repo.schedule.getGroupedPaths(cUserId)
            mCalendarAdapter.updateData(list)
            ui_empty.isVisible = list.count() == 0

            //
            val colorData = Repo.item.getGroupColor(cUserId)
            setupPieChart(
                ui_chart_color,
                colorData.map { it.desc to it.count },
                colorData.map { it.colorId }
            )

            //
            val statusData = Repo.item.getGroupStatus(cUserId)
            setupPieChart(
                ui_chart_status,
                statusData.map { it.key to it.count }, null
            )

            //
            val thermData = Repo.item.getGroupTherm(cUserId)
            setupPieChart(
                ui_chart_therm,
                thermData.map { it.desc to it.count },
                thermData.map { it.color }
            )

            //
            val brandData = Repo.item.getGroupBrand(cUserId)
            setupPieChart(
                ui_chart_brand,
                brandData.map { it.key to it.count }, null
            )

            //
            mLoading.value = false
        }
    }

    @SuppressLint("DefaultLocale")
    private fun setupPieChart(chart: PieChart, data: List<Pair<String,Int>>, colors: List<Int>?=null) {
        val entries = data.map {
            PieEntry(it.second.toFloat(), it.first.toLowerCase())
        }
        val dataSet = PieDataSet(entries, "")

        if (colors!=null) {
            dataSet.setColors(colors.toIntArray(), context)
        } else {
            dataSet.colors = Palette.shared.randomResolved(data.count())
        }

        val pieData = PieData(dataSet)
        pieData.setValueFormatter(ChartIntValueFormatter())

        chart.data = pieData
        chart.description.isEnabled = false
        chart.legend.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        chart.legend.orientation = Legend.LegendOrientation.VERTICAL
        chart.setDrawEntryLabels(false)

        chart.invalidate()
    }

    //
    override fun rvCreate(
        identifier: String,
        itemView: View,
        r: () -> Rv.Value,
        position: () -> Int
    ) {}

    override fun rvBind(identifier: String, v: View, h: Rv.VH, r: Rv.Value, position: Int) {
        r.whenValue<ScheduleGroupPathRModel> {
            val path = coalesce(it.item?.path_thumb, it.look?.path_thumb, "")
            Img.load(File(path), v.ui_img)
            v.ui_badge.text = "${it.count}"
        }
    }

    override fun rvId(identifier: String, r: Rv.Value, position: Int): Int? {
        return null
    }

}