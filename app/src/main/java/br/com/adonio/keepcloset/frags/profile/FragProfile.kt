package br.com.adonio.keepcloset.frags.profile

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.DialogOk
import br.com.adonio.keepcloset.helpers.Act
import br.com.adonio.keepcloset.helpers.F
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.helpers.ImgProviderCrop
import br.com.adonio.keepcloset.repos.Repo
import kotlinx.android.synthetic.main.frag_profile.*

class FragProfile: Fragment() {

    //
    private val cUser
        get() = App.modelUser!!

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.frag_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val user = App.modelUser ?: return

        //
        ui_avatar.setOnClickListener {
            ImgProviderCrop.pick(context!!, this)
        }
        user.p().loadAvatar(ui_avatar)

        //
        ui_email.text = user.email

        //
        ui_ed_name.setText(user.name)
    }

    private fun saveName() {
        Act.hideKeyboard(this)

        Repo.user.saveName(App.modelUser?.id ?: 0L, ui_ed_name.text.toString())
        App.modelUserReload()
    }

    //
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val f = ImgProviderCrop.onActivityResult(requestCode, resultCode, data) ?: return

        val copiedFile = F.copyInternal(f, "${cUser.id}", "avatar.dat")

        Img.load(copiedFile, ui_avatar)

        Repo.user.saveAvatar(cUser.id, copiedFile.path)

        App.modelUserReload()
    }

    override fun onStop() {
        super.onStop()

        saveName()
    }

    //

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_frag_profile, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId) {
            R.id.menu_action_done -> {
                saveName()
                DialogOk.show(context, getString(R.string.dados_salvos_com_sucesso), getString(R.string.perfil))
            }
        }
        return super.onOptionsItemSelected(item)
    }

}