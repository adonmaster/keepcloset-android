package br.com.adonio.keepcloset.helpers

import androidx.annotation.StringRes
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.repos.Repo

object Res {

    fun locations(): List<String> {
        val originals = App.shared.resources.getStringArray(R.array.locations).toMutableList()
        val preflist = Repo.pref.getList("Res@locations") ?: listOf()
        originals.addAll(preflist)
        return originals
    }

    fun locationAdd(s: String) {
        Repo.pref.add("Res@locations", s)
    }

    fun str(@StringRes id: Int): String {
        return App.shared.getString(id)
    }

}