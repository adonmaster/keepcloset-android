package br.com.adonio.keepcloset.frags.main.schedule

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.threeten.bp.LocalDate

class FragMainScheduleVM: ViewModel() {

    val position = MutableLiveData<Int>().also { it.value = 100 }
    val positionCount = 200

    val cDate get() = positionToDt(position.value!!)

    fun positionToDt(position: Int): LocalDate {
        val diff = position - (positionCount/2)
        return LocalDate.now().plusMonths(diff.toLong())
    }
}