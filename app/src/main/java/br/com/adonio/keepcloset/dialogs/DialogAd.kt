package br.com.adonio.keepcloset.dialogs

import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import br.com.adonio.keepcloset.MainActivity
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.extensions.setEnabledEx
import br.com.adonio.keepcloset.helpers.Act
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.helpers.Task
import br.com.adonio.keepcloset.rmodels.AdRModel
import kotlinx.android.synthetic.main.dialog_ad.view.*
import kotlin.math.roundToInt
import kotlin.math.roundToLong

class DialogAd: DialogBase() {

    // static
    companion object {
        fun show(fragmentManager: FragmentManager?, ad: AdRModel) {
            if (fragmentManager==null) return
            val dialog = DialogAd()
            val bundle = Bundle()
            bundle.putParcelable("ad", ad)
            dialog.arguments = bundle
            dialog.isCancelable = false

            dialog.show(fragmentManager, "DialogAd")
        }
        private var listeners = mutableListOf<Listener>()
        fun attach(listener: Listener) {
            listeners.add(listener)
        }
        fun detach(listener: Listener) {
            listeners.remove(listener)
        }
    }

    //
    private val cAd
        get() = arguments!!.getParcelable<AdRModel>("ad")!!

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.setTitle("Publicidade")
        return inflater.inflate(R.layout.dialog_ad, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (view.ui_img == null) return
        if (view.ui_desc == null) return

        //
        if (cAd.attachments.count() > 0) {
            Img.load(cAd.attachments.random().url, view.ui_img)
        }

        //
        view.ui_img.setOnClickListener {
            openLink()
        }

        //
        view.ui_desc.isVisible = cAd.body.desc?.isNotBlank() == true
        view.ui_desc.text = cAd.body.desc
        view.ui_desc.setOnClickListener {
            openLink()
        }

        //
        view.ui_title.isVisible = cAd.body.title?.isNotBlank() == true
        view.ui_title.text = cAd.body.title
        view.ui_title.setOnClickListener {
            openLink()
        }

        //
        view.ui_cancel.setEnabledEx(false)

        var randomIntervalInSecs = ((5000 + (7000 * Math.random()))/1000).roundToLong()
        val timer = object: CountDownTimer(randomIntervalInSecs * 1000, 1000) {
            override fun onTick(p0: Long) {
                view.ui_cancel.text = "${--randomIntervalInSecs} segundos para fechar"
            }
            override fun onFinish() {
                view.ui_cancel.setEnabledEx(true)
                view.ui_cancel.text = "Fechar"
            }
        }
        timer.start()

        view.ui_cancel.setOnClickListener {
            dialog?.dismiss()
            listeners.forEach { it.dialogAdOnCancel() }
        }

        //
        view.ui_no_more.setOnClickListener {
            dialog?.dismiss()
            Task.main(200) {
                MainActivity.shared?.purchaseBilling()
            }
        }
    }

    private fun openLink()
    {
        Act.openLink(context, cAd.body.link)

        dialog?.dismiss()
        listeners.forEach { it.dialogAdOnCancel() }
    }


    //
    interface Listener {
        fun dialogAdOnCancel()
    }
}