package br.com.adonio.keepcloset.frags.main.look

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.dialogs.DialogAd
import br.com.adonio.keepcloset.extensions.setVisible
import br.com.adonio.keepcloset.frags.main.look.adapter.FragMainLookAdapter
import br.com.adonio.keepcloset.helpers.synced
import br.com.adonio.keepcloset.models.Look
import br.com.adonio.keepcloset.repos.Repo
import br.com.adonio.keepcloset.services.AdService
import kotlinx.android.synthetic.main.frag_main_look.*

class FragMainLook: Fragment(), DialogAd.Listener {

    // state
    private lateinit var vm: FragMainLookVM
    private lateinit var mAdapter: FragMainLookAdapter

    // methods
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.frag_main_look, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // view model
        vm = ViewModelProviders.of(activity!!).get(FragMainLookVM::class.java)

        // rv
        mAdapter = FragMainLookAdapter(object : FragMainLookAdapter.Listener {
            override fun onSelect(item: Look) {
                onSelectLook(item)
            }
            override fun onEmpty(option: Boolean) {
                ui_empty.setVisible(option)
            }
        })
        ui_rv.adapter = mAdapter
        ui_rv.layoutManager = GridLayoutManager(context, 3).also {
            it.spanSizeLookup = object: GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (mAdapter.getModel(position)?.isTypeCategory() == true) 3 else 1
                }
            }
        }
        ui_rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_slide_right)

        // btn add
        ui_btn_add.setOnClickListener {
            if (AdService.shouldShowAd()) {
                AdService.showAdDialog(childFragmentManager)
            } else {
                lookNew()
            }
        }

        //
        updateData()
    }

    private fun updateData() {
        synced("FragMainLook@updateData") {
            vm.items = Repo.look.getAll(App.modelUser?.id ?: 0)
            mAdapter.updateData(vm.items)

            syncRelease()
        }
    }

    private fun lookNew() {
        vm.modelEditing = Look().also {
            it.user_id = App.modelUser?.id ?: 0
            it.bknd_index = 0
        }

        findNavController().navigate(R.id.action_to_frag_main_look_edit)
    }

    private fun onSelectLook(item: Look) {
        val dataPosition = vm.items.indexOf(item)
        FragLookSlider.navigate(this, R.id.action_to_look_slider, dataPosition)
    }

    override fun onStart() {
        super.onStart()

        DialogAd.attach(this)
    }

    override fun onStop() {
        super.onStop()

        DialogAd.detach(this)
    }

    //
    override fun dialogAdOnCancel() {
        lookNew()
    }
}