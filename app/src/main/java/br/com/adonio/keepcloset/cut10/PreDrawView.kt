package br.com.adonio.keepcloset.cut10

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.os.AsyncTask
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import br.com.adonio.keepcloset.helpers.Bmp
import org.opencv.android.Utils
import org.opencv.core.*
import org.opencv.core.Rect
import org.opencv.imgproc.Imgproc
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min


@Suppress("PrivatePropertyName")
class PreDrawView(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    //
    private var mBmp = Bmp.createEmpty()

    private var mBmpDraw = Bmp.createEmpty()
    private var mCanvasDraw = Canvas()

    private var mPaintSemiTransparent = Paint()

    private var mPath = Path()
    private var mPaint = Paint()

    //
    init {
        isClickable = true
        setLayerType(LAYER_TYPE_SOFTWARE, null)

        mPaintSemiTransparent.alpha = (255 * .7).toInt()

        mPaint = Paint(mPaint)
        mPaint.isDither = true
        mPaint.color = Color.BLUE
        mPaint.style = Paint.Style.STROKE
        mPaint.strokeJoin = Paint.Join.ROUND
        mPaint.strokeCap = Paint.Cap.ROUND
        mPaint.strokeWidth = 180f
    }

    //
    fun setBitmap(bmp: Bitmap?)
    {
        if (bmp==null || bmp.width == 0 || width == 0) return

        mBmp = Bmp.getResizedBitmap(bmp, width, height)!!
        mBmp.setHasAlpha(true)

        mBmpDraw = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        mBmpDraw.setHasAlpha(true)

        mCanvasDraw = Canvas(this.mBmpDraw)

        mPath = Path()

        invalidate()
    }

    fun clear() {
        setBitmap(mBmp)
    }

    fun requestMaskedBmp(cbOnDone: (Bitmap, Bitmap)->Unit) {
        RemoverTask().execute({ mask ->
            cbOnDone(mBmp, mask)
        })
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean
    {
        return when(event?.action) {
            MotionEvent.ACTION_DOWN -> {
                touchStart(event.x, event.y)
                true
            }
            MotionEvent.ACTION_MOVE -> { touchMove(event.x, event.y); true }
            MotionEvent.ACTION_UP -> {
                touchEnd()
                true
            }
            else -> super.onTouchEvent(event)
        }
    }

    private var mx = 0f
    private var my = 0f
    private fun touchStart(x: Float, y: Float) {
        mx = x
        my = y

        mPath.moveTo(x, y)
    }

    private fun touchMove(x: Float, y: Float)
    {
        val dx: Float = abs(x - mx)
        val dy: Float = abs(y - my)

        if (dx >= 20 || dy >= 20) {

            mPath.quadTo(mx, my, (x + mx) / 2, (y + my) / 2)
            mCanvasDraw.drawPath(mPath, mPaint)

            mx = x
            my = y

            invalidate()
        }

    }

    private fun touchEnd()
    {
        // closing line
        mPath.lineTo(mx, my)
        mCanvasDraw.drawPath(mPath, mPaint)

        invalidate()
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.save()

        canvas?.drawBitmap(mBmp, 0f, 0f, null)
        canvas?.drawBitmap(mBmpDraw, 0f, 0f, mPaintSemiTransparent)

        canvas?.restore()
    }

    //
    @SuppressLint("StaticFieldLeak")
    private inner class RemoverTask: AsyncTask<(Bitmap)->Unit, Void, Bitmap>() {
        private var cb: ((Bitmap)->Unit)?=null

        override fun doInBackground(vararg params: ((Bitmap) -> Unit)?): Bitmap {
            this.cb = params[0]
            var bitmap = mBmp

            val img = Mat()
            Utils.bitmapToMat(bitmap, img)

            val maskPack = calculateMaskPack()
            if (maskPack.first.width < 2 || maskPack.first.height < 2) {
                // empty mask? evade!
                return Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
            }

            val rect = maskPack.first
            val mask = maskPack.second

            val fgdModel = Mat()
            val bgdModel = Mat()

            val imgC3 = Mat()
            Imgproc.cvtColor(img, imgC3, Imgproc.COLOR_RGBA2RGB)

            Imgproc.grabCut(
                imgC3, mask, rect, bgdModel, fgdModel, 8,
                Imgproc.GC_INIT_WITH_MASK or Imgproc.GC_INIT_WITH_RECT
            )

            val source = Mat(1, 1, CvType.CV_8U, Scalar(Imgproc.GC_PR_FGD.toDouble()))
            Core.compare(mask, source, mask, Core.CMP_EQ)

            //This is important. You must use Scalar(255,255, 255,255), not Scalar(255,255,255)
            val foreground = Mat(
                img.size(), CvType.CV_8UC3, Scalar(
                    255.0, 255.0, 255.0, 255.0
                )
            )
            img.copyTo(foreground, mask)

            // convert matrix to output bitmap
            bitmap = Bitmap.createBitmap(
                foreground.size().width.toInt(),
                foreground.size().height.toInt(),
                Bitmap.Config.ARGB_8888
            )
            Utils.matToBitmap(foreground, bitmap)
            return bitmap
        }

        private fun indexer(x: Int, y: Int): Int {
            return y * mBmpDraw.width + x
        }

        private fun calculateMaskPack(): Pair<Rect, Mat> {
            val w = mBmpDraw.width
            val h = mBmpDraw.height

            var minx = 9999999
            var miny = 9999999
            var maxx = 0
            var maxy = 0

            val pixels = IntArray(w * h)
            mBmpDraw.getPixels(pixels, 0, w, 0, 0, w, h)

            val mask = Mat(h, w, CvType.CV_8U, Scalar(Imgproc.GC_BGD.toDouble()))

            var hasPixel = false
            for (x in 0 until w) {
                for (y in 0 until h) {
                    val index = indexer(x, y)
                    val px = pixels[index]

                    // rect
                    if (px != 0) {
                        hasPixel = true
                        mask.put(y, x, Imgproc.GC_PR_FGD.toDouble())

                        minx = min(minx, x)
                        miny = min(miny, y)
                        maxx = max(maxx, x)
                        maxy = max(maxy, y)
                    }
                }
            }

            // has pixel
            if (hasPixel) return Rect(minx, miny, maxx - minx, maxy - miny) to mask

            // empty mask
            return Rect(0, 0, 0, 0) to mask
        }

        override fun onPostExecute(result: Bitmap) {
            cb?.invoke(result)
        }
    }

}

