package br.com.adonio.keepcloset.models

import androidx.room.Entity
import androidx.room.Ignore
import br.com.adonio.keepcloset.models.presenters.ItemPresenter

@Entity(tableName = "items")
class Item: BaseModel() {

    companion object {
        fun f(userId: Long, path: String) = Item().apply {
            this.user_id = userId
            this.path = path
            this.path_thumb = path
        }
    }

    var user_id: Long = 0
    var brand: String? = null
    var category: String? = null
    var color1: Long = 0
    var color1_desc: String? = null
    var color2: Long = 0
    var color2_desc: String? = null
    var therm_id: Int = 0
    var status: String? = null
    lateinit var path: String
    lateinit var path_thumb: String

    @Ignore
    fun p() = cache.getGranted("presenter", { ItemPresenter(this) })

}