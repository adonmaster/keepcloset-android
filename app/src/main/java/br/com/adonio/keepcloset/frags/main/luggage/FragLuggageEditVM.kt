package br.com.adonio.keepcloset.frags.main.luggage

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.adonio.keepcloset.models.LuggageItem
import org.threeten.bp.LocalDate

class FragLuggageEditVM: ViewModel() {

    var luggageId = 0L
    val location = MutableLiveData<String>()
    val date = MutableLiveData<LocalDate>()
    val duration = MutableLiveData<Int>()

    val items = arrayListOf<LuggageItem>()

}