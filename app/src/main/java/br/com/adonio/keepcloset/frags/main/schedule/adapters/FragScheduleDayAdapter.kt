package br.com.adonio.keepcloset.frags.main.schedule.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.App
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.helpers.Act
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.helpers.guard
import br.com.adonio.keepcloset.models.ScheduleItem
import br.com.adonio.keepcloset.repos.Repo
import kotlinx.android.synthetic.main.rv_frag_schedule_day.view.*
import org.threeten.bp.LocalDate
import java.io.File

class FragScheduleDayAdapter(val dt: LocalDate, private val listener: Listener): RecyclerView.Adapter<FragScheduleDayAdapter.VH>() {

    private val mData = arrayListOf<ScheduleItem>()

    init {
        Repo.schedule.getBetweenPromise(App.modelUser?.id ?: 0, dt, dt)
            .then {  list ->
                mData.clear()
                mData.addAll(list)

                notifyDataSetChanged()
            }
    }

    fun insertModelAtBeginning(item: ScheduleItem) {
        mData.add(0, item)
        notifyItemInserted(0)
    }

    fun removeWithId(scheduleItemId: Long?) {
        val id = guard(scheduleItemId) { return }
        val index = guard(mData.indexOfFirst { it.id == id }) { return }

        mData.removeAt(index)
        notifyItemRemoved(index)
    }

    fun updateByModel(item: ScheduleItem?) {
        val i = guard(item) { return }
        val position = guard(mData.indexOfFirst { it.id == i.id }) { return }

        mData[position] = i
        notifyItemChanged(position)
    }

    // --

    override fun getItemCount(): Int {
        return mData.count().also { listener.onEmpty(it==0) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(R.layout.rv_frag_schedule_day, parent, false)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = mData[position]
        Img.load(File(m.path_thumb), holder.uiImg)

        holder.uiColor1.setBackgroundColor(m.color1.toInt())
        holder.uiColor2.setBackgroundColor(m.color2.toInt())
    }


    // vh
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiImg: ImageView = itemView.ui_img
        val uiColor1: View = itemView.ui_color_1
        val uiColor2: View = itemView.ui_color_2

        init {
            itemView.setOnClickListener { listener.onSelect(mData[adapterPosition]) }
        }
    }

    // listener
    interface Listener {
        fun onSelect(item: ScheduleItem)
        fun onEmpty(option: Boolean)
    }

}