package br.com.adonio.keepcloset.frags.main.luggage.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.keepcloset.R
import br.com.adonio.keepcloset.extensions.setVisible
import br.com.adonio.keepcloset.helpers.Act
import br.com.adonio.keepcloset.helpers.Img
import br.com.adonio.keepcloset.models.LuggageItem
import kotlinx.android.synthetic.main.rv_frag_luggage_edit.view.*
import java.io.File

class FragLuggageEditAdapter(private val mData: List<LuggageItem>, private val listener: Listener):
    RecyclerView.Adapter<FragLuggageEditAdapter.VH>() {

    override fun getItemCount(): Int {
        return mData.count()
            .also { listener.onEmpty(it==0) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(R.layout.rv_frag_luggage_edit, parent, false)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = mData[position]

        //
        if (m.desc == "Text") {

            holder.uiImg.setImageResource(android.R.color.transparent)
            holder.uiTxt.text = m.path

        } else {

            Img.load(File(m.path), holder.uiImg)
            holder.uiTxt.text = ""

        }

        //
        holder.uiCover.setVisible(m.isPacked)

        //
        holder.uiIconNew.setVisible(m.isNew())
    }


    // vh
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiCover = itemView.ui_cover
        val uiImg = itemView.ui_img
        val uiTxt = itemView.ui_txt
        val uiIconNew = itemView.ui_icon_new
        init {
            itemView.setOnClickListener {
                val p = adapterPosition
                listener.onSelect(mData[p], p)
            }
            itemView.setOnLongClickListener {
                val p = adapterPosition
                listener.onLongClick(mData[p], p)
                true
            }
        }
    }

    // listener
    interface Listener {
        fun onEmpty(option: Boolean)
        fun onSelect(model: LuggageItem, position: Int)
        fun onLongClick(model: LuggageItem, position: Int)
    }
}