package br.com.adonio.keepcloset.helpers

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class SyncTest {

    @Test
    fun testBase() {
        var count = 2

        //
        synced("testBase") {
            count = 161
            syncRelease()
        }
        assertEquals(161, count)

        //
        synced("test2") {
            count = 343
            // i wont release this
        }
        synced("test2") {
            // must be blocked!!
            count = 747
        }
        assertEquals(343, count)
    }

}