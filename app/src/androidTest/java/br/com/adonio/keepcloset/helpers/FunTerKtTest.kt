package br.com.adonio.keepcloset.helpers

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class FunTerKtTest {

    @Test
    fun testCoalesce() {
        assertEquals(coalesce("dino"), "dino")
        assertEquals(coalesce("dino", null, "coma"), "dino")
        assertEquals(coalesce(null, null, "coma"), "coma")
    }

}