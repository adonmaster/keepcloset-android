package br.com.adonio.keepcloset.helpers

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.threeten.bp.LocalDate

@RunWith(JUnit4::class)
class DtTest {

    @Test
    fun fromNow() {
        val now = LocalDate.now()
        assertEquals(Dt.fromNow(now), "hoje")
        assertEquals(Dt.fromNow(now.plusDays(1)), "amanhã")
        assertEquals(Dt.fromNow(now.plusDays(-1)), "ontem")

        assertEquals(Dt.fromNow(now.plusDays(32)), "em um mês")
        assertEquals(Dt.fromNow(now.plusDays(-32)), "há um mês")

        assertEquals(Dt.fromNow(now.plusMonths(3)), "em 3 meses")
        assertEquals(Dt.fromNow(now.plusMonths(-13)), "há um ano")
    }

}